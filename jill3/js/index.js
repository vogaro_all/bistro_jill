;// Global Object
var Page = Page || {};

(function($){

	var $win = $(window);
	var $html = $('html');
	var $body = $('body');
	var $htmlBody = $('html,body');
	var $doc = $(document);

	var manifestPC = [
		{ src: 'images/bg_company.png' },
		{ src: 'images/bg_company_repeat.png' },
		{ src: 'images/bg_info_ttl_repeat.png' },
		{ src: 'images/bg_shop_jb_on.jpg' },
		{ src: 'images/bg_shop_jill.png' },
		{ src: 'images/bg_shop_jill_on.jpg' },
		{ src: 'images/bg_shop_nijill.png' },
		{ src: 'images/bg_shop_nijill_on.jpg' },
		{ src: 'images/bg_staff.jpg' },
		{ src: 'images/ico_arrow_mv_banner.png' },
		{ src: 'images/ico_arrow01.png' },
		{ src: 'images/ico_arrow01_on.png' },
		{ src: 'images/img_shop_jb.jpg' },
		{ src: 'images/img_shop_jill.jpg' },
		{ src: 'images/img_shop_nijill.jpg' },
		{ src: 'images/ttl_blog.png' },
		{ src: 'images/ttl_company.png' },
		{ src: 'images/ttl_mv_banner.png' },
		{ src: 'images/ttl_mv_banner_on.png' },
		{ src: 'images/ttl_news.png' },
		{ src: 'images/ttl_shop_jb.png' },
		{ src: 'images/ttl_shop_jb_on.png' },
		{ src: 'images/ttl_shop_jill.png' },
		{ src: 'images/ttl_shop_jill_on.png' },
		{ src: 'images/ttl_shop_nijill.png' },
		{ src: 'images/ttl_shop_nijill_on.png' },
		{ src: 'images/ttl_staff.png' },
		{ src: 'images/txt_mv_banner.png' },
		{ src: 'images/txt_mv_banner_on.png' }
	];

	var manifestSP = [
		{ src: 'images/img_mv_sp.jpg' },
		{ src: 'images/bg_company_sp.png' },
		{ src: 'images/bg_shop_jill_sp.png' },
		{ src: 'images/bg_shop_nijill_sp.png' },
		{ src: 'images/bg_staff_sp.jpg' },
		{ src: 'images/btn_mv_banner_sp.png' },
		{ src: 'images/img_shop_jb_sp.jpg' },
		{ src: 'images/img_shop_jill_sp.jpg' },
		{ src: 'images/img_shop_nijill_sp.jpg' },
		{ src: 'images/ttl_blog_sp.png' },
		{ src: 'images/ttl_company_sp.png' },
		{ src: 'images/ttl_news_sp.png' },
		{ src: 'images/ttl_staff_sp.png' }
	];

	/*--------------------------------------------------------------------------
		 youtube
	---------------------------------------------------------------------------*/
	/**
	 * MVの背景動画 youtube
	 */
	(function(){
		// スマートデバイスの時は処理しない
		if($.isDevice('sd')){
			return false;
		}
		var tag = document.createElement('script');
		tag.src = 'https://www.youtube.com/iframe_api';
		var firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

		var player;
		var timer;

		/**
		 * youtube JavaScript APIを使用するための関数
		 */
		window.onYouTubeIframeAPIReady = function(){
			player = new YT.Player('YTVideo', {
				width: window.innerWidth,
				height: window.innerHeight,
				videoId: 'KLTcOanXx1g',
				playerVars: {
					'controls': 0,
					'disablekb': 1, //キーボード操作を無効化
					'modestbranding': 1, //ロゴ非表示
					'rel': 0, //再生終了後の関連動画の非表示
					'showinfo': 0, //動画タイトルを非表示
					'origin': 'http://www.bistro-jill.com/'
				},
				events: {
					'onReady': onPlayerReady,
					'onStateChange': onPlayerStateChange
				}
			});
		};

		/**
		 * プレーヤーが準備できたらこの関数が呼ばれる
		 */
		function onPlayerReady(event){
			event.target.playVideo();
			event.target.mute();

			// youtube動画の再生準備が出来たことを知らせる
			Common.controller.trigger('youtubeready');
			loop();
		}

		/**
		 * プレーヤーの状態が変わる度に呼ばれる
		 */
		function onPlayerStateChange(event){
			if(event.data === 0){
				event.target.playVideo();
			}
		}

		/**
		 * youtube動画が再生されるまでループ
		 */
		function loop(){

			var duration = player.getCurrentTime();

			timer = setTimeout(loop,500);

			if(duration > 0){
				// youtube動画の再生が始まったことを知らせる
				Common.controller.trigger('youtubestarted');
				clearTimeout(timer);
			}
		}
	}());

	/*--------------------------------------------------------------------------
		 bgMovie
	---------------------------------------------------------------------------*/
	Page.bgMovie = (function(){
		var p = BgMovie.prototype;

		/**
		 * 背景動画（yotuube）の上下の黒地の部分が見えないように画角に合わせ、
		 * 全画面に設定
		 */
		function BgMovie(){
			this._selector = {
				video: '#YTVideo',
				cover: '.bgVideo-cover'
			};
			this._videoW = 1280;
			this._videoH = 720;
			this._videoRatio = Math.round(this._videoW / this._videoH * 100) / 100;
			this._windowRatio = Math.round(window.innerWidth / window.innerHeight * 100) / 100;
			this._scale = 0;

			this._init();
		}

		/**
		 * 初期設定
		 */
		p._init = function(){
			Common.controller.on('youtubeready', this._onReady.bind(this));
			Common.controller.on('youtubestarted', this._onStarted.bind(this));
		};

		/**
		 * 動画の準備が出来たら実行
		 */
		p._onReady = function(){
			this._onResize();
			Common.controller.on('resize', this._onResize.bind(this));
		};

		/**
		 * 動画が再生されたら実行
		 */
		p._onStarted = function(){
			TweenMax.fromTo($(this._selector.cover), 1, {opacity: 1}, {opacity: 0, display: 'none'});
		};

		/**
		 * リサイズ時の処理
		 */
		p._onResize = function(){
			var ww = window.innerWidth;
			var wh = window.innerHeight;
			var $video = $(this._selector.video);

			// ムービーの比率16:9
			// ウィンドウの比率
			this._windowRatio = Math.round(window.innerWidth / window.innerHeight * 100) / 100;

			if(this._windowRatio > this._videoRatio){
				// 横の黒が見える状態
				this._scale = this._windowRatio / this._videoRatio;
			} else {
				// 縦の黒が見える状態
				this._scale = this._videoRatio / this._windowRatio;
			}

			var scaleW = ww * this._scale;
			var scaleH = wh * this._scale;

			$video.css({
				top: -(scaleH - wh) / 2,
				left: -(scaleW - ww) / 2,
				width: scaleW,
				height: scaleH
			});
		};

		return BgMovie;
	}());

	/*--------------------------------------------------------------------------
		 movieOverlay
	---------------------------------------------------------------------------*/
	Page.movieOverlay = (function(){
		var p = MovieOverlay.prototype;

		/**
		 * スクロールダウンで動画にオーバーレイをかける処理
		 */
		function MovieOverlay(){
			this._$target = $('.bgVideo-overlay');
			this._defaultAlpha = 0;
			this._max = 1;

			this._init();
		}

		/**
		 * 初期設定
		 */
		p._init = function(){
			this._onEvent();
		};

		/**
		 * イベント登録
		 */
		p._onEvent = function(){
			$win.on('scroll.movieOverlay', this._onScroll.bind(this));
		};

		/**
		 * スクロール時の処理
		 */
		p._onScroll = function(){
			var endY = parseInt($win.height(), 10);
			var scrollVal = $win.scrollTop();
			var alpha = this._defaultAlpha + ((scrollVal / endY * 100) / 100);

			if(alpha > this._defaultAlpha && alpha <= this._max){
				this._$target.css({ 'background-color': 'rgba(0,0,0,' + alpha + ')'});
			}
		};

		return MovieOverlay;
	}());

	/*--------------------------------------------------------------------------
		 letterAnimation
	---------------------------------------------------------------------------*/
	Page.letterAnimation = (function(){
		var p = LetterAnimation.prototype;

		/**
		 * オープニングアニメーション01
		 * MV内のメッセージ
		 * ローディング後のMVアニメーションで最初に実行される
		 */
		function LetterAnimation(){
			this._$mv = $('#MV');
			this._$mvTtl = this._$mv.find('.mvTtl');
			this._$letter = this._$mv.find('.mvTtl-letter');
			this._$default = this._$mv.find('.mvTtl-default');
			this._$cover = this._$mv.find('.mvTtl-cover');
			this._$bg = this._$mv.find('.mvTtl-bg');

			this._init();
		}

		/**
		 * 初期設定
		 * ローディングアニメーション終了後にendLoadedイベントによって処理開始
		 */
		p._init = function(){
			TweenMax.set(this._$bg, { width: 0 });
			TweenMax.set(this._$cover, { width: 0 });

			Common.controller.on('endLoaded', this._tween.bind(this));
		};

		/**
		 * アニメーション設定
		 */
		p._tween = function(){
			var _this = this;

			// tween
			TweenMax.to(this._$bg, 0.6, { width: '100%', ease: Expo.easeOut });
			TweenMax.to(this._$cover, 0.6, { width: '100%', delay: 0.5, ease: Expo.easeInOut, onComplete: function(){
				TweenMax.to(_this._$bg, 0.6, { x: '101%', delay: 0.1, ease: Expo.easeOut, display: 'none'});
				Common.controller.trigger('endLetterAnimation');
			}});

		};

		/**
		 * 初期化処理
		 */
		p._reset = function(){
			this._$cover.attr('style', '');
		};

		return LetterAnimation;

	}());

	/*--------------------------------------------------------------------------
		 bannerAnimation
	---------------------------------------------------------------------------*/
	Page.bannerAnimation = (function(){
		var p = BannerAnimation.prototype;

		/**
		 * オープニングアニメーション02
		 * MV内のバナー
		 * メッセージのアニメーション終了後に処理
		 */
		function BannerAnimation(){
			this._$bg = $('#MV .mvBanner-bg');
			this._$circle = $('#MV .mvBanner-circle');
			this._$ttl = $('#MV .mvBanner-ttl');
			this._$txt = $('#MV .mvBanner-txt');
			this._$arrow = $('#MV .mvBanner-arrow');

			this._init();
		}

		/**
		 * 初期設定
		 */
		p._init = function(){
			TweenMax.set(this._$ttl, { opacity: 0, x: 80, ease: Expo.easeOut });
			TweenMax.set(this._$txt, { opacity: 0, x: 80, ease: Expo.easeOut });
			TweenMax.set(this._$arrow, { opacity: 0, x: 20, ease: Expo.easeOut });
			TweenMax.set(this._$circle, { opacity: 0, ease: Expo.easeOut });
			TweenMax.set(this._$bg, { opacity: 0 });

			Common.controller.on('endLetterAnimation', this._animate.bind(this));
		};

		/**
		 * アニメーション設定
		 */
		p._animate = function(){
			TweenMax.to(this._$ttl, 1, { opacity: 1, x: 0, delay: 0.5, ease: Expo.easeOut });
			TweenMax.to(this._$txt, 1, { opacity: 1, x: 0, delay: 0.6, ease: Expo.easeOut });
			TweenMax.to(this._$arrow, 1.4, { opacity: 1, x: 0, delay: 0.8, ease: Expo.easeOut, onComplete: this._reset.bind(this) });
			TweenMax.to(this._$circle, 1, { opacity: 1, delay: 0.5, ease: Sine.easeOut });
			TweenMax.to(this._$bg, 1, { opacity: 1, delay: 1, ease: Sine.easeOut, onComplete: function(){
				Common.controller.trigger('endBannerAnimation');
			}});
		};

		/**
		 * 初期化
		 */
		p._reset = function(){
			this._$ttl.attr('style', '');
			this._$txt.attr('style', '');
			this._$arrow.attr('style', '');
			this._$circle.attr('style', '');
			this._$bg.attr('style', '');
		};

		return BannerAnimation;
	}());

	/*--------------------------------------------------------------------------
		 scrollDownAnimation
	---------------------------------------------------------------------------*/
	Page.scrollDownAnimation = (function(){
		var p = ScrollDownAnimation.prototype;

		/**
		 * オープニングアニメーション03
		 * MV内のスクロールインジケータ
		 * バナーアニメーションが終わり次第実行
		 */
		function ScrollDownAnimation(){
			this._$scrollDown = $('#MV .mvScroll');

			this._init();
		}

		/**
		 * 初期設定
		 */
		p._init = function(){
			TweenMax.set(this._$scrollDown, { opacity: 0, y: -10, ease: Expo.easeOut });
			Common.controller.on('endLetterAnimation', this._animate.bind(this));
		};

		/**
		 * アニメーション設定
		 */
		p._animate = function(){
			TweenMax.to(this._$scrollDown, 1.5, { opacity: 1, y: 0, delay: 1.5, ease: Expo.easeOut });
		};

		return ScrollDownAnimation;
	}());

/*----------------------------------------------------------------------
	DOM READY
----------------------------------------------------------------------*/
	jQuery(function($){
		new Common.fullScreen($('#MV'));
		new Page.movieOverlay();

		// スマートデバイスの時は処理しない
		if(!$.isDevice('sd')){
			new Page.bgMovie();
		}

		if(!$html.hasClass('lt-ie9')){
			new Page.letterAnimation();
			new Page.bannerAnimation();
			new Page.scrollDownAnimation();
		}

		$('.js-btnHover').each(function(){
			new Common.btnHover($(this));
		});

		// PC
		if($html.hasClass('modePC')){
			if(!$html.hasClass('lt-ie9')){
				new Common.loading(manifestPC, function(){
					Common.controller.trigger('endLoaded');
				});
			}
		} else {
			// SP
			new Common.loading(manifestSP, function(){
				Common.controller.trigger('endLoaded');
			});
		}

	});

}(jQuery));
