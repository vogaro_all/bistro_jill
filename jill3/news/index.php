<?php
include_once(dirname(__FILE__)."/../lacne/news/output/post.php");
?>
<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie8 lt-ie9" lang="ja">
<![endif]-->
<!--[if IE 9]>
<html class="ie9 lt-ie9" lang="ja">
<![endif]-->
<!--[if !IE]><!-->
<html lang="ja">
<!--<![endif]-->
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>ジリオンニュース｜株式会社ジリオン</title>
<meta name="description" content="「ジリオンニュース」をご紹介いたします。目黒,中目黒,五反田,学芸大学で、大衆ビストロジル/大衆ビストロ煮ジルを展開しています。">
<meta name="keywords" content="株式会社ジリオン,大衆ビストロジル,煮ジル,肉料理,居酒屋,バル,ワイン,目黒,中目黒,五反田,学芸大学">

<? include_once(dirname(__FILE__).'/../shared/inc/head.inc'); ?>
<link rel="stylesheet" href="css/index.css">

<? include_once(dirname(__FILE__).'/../shared/inc/ie.inc'); ?>

</head>

<body>
<? include_once(dirname(__FILE__).'/../shared/inc/ga.inc'); ?>

<? include_once(dirname(__FILE__).'/../shared/inc/loading.inc'); ?>

<div id="Page">

<? include_once(dirname(__FILE__).'/../shared/inc/header.php'); ?>

<div id="Content">

	<header class="m-pageHeader">
		<div class="m-pageHeader-inner">
			<h1 class="m-pageHeader-ttl">
				<img src="images/ttl_page.png" alt="JILLION NEWS" width="434" height="50" class="u-disp-pcImg">
				<img src="images/ttl_page_sp.png" alt="" width="283" height="32" class="u-disp-spImg">
			<!-- .kvTtl // --></h1>
		<!-- .kv-inner // --></div>
	<!-- .kv // --></header>

	<div class="m-container">
		<div class="m-container-inner l-container">

			<div class="m-frame c-shadow">
				<div class="m-frame-inner">

					<div class="m-frame-cornerTop">
						<div class="m-frame-cornerBottom">
							<div class="m-frame-body">

								<div class="newsLists">


<?php

$params = array(
		"num"				 => '15',
		"postmeta"			=> true
);

echo printListNews($params , 'list');
?>



								<!-- .newsLists // --></div>

							<!-- .m-frame-body // --></div>
						<!-- .m-frame-cornerBottom // --></div>
					<!-- .m-frame-cornerTop // --></div>

				<!-- .m-frame-inner // --></div>
			<!-- .m-frame // --></div>

<!--===============================================================
ページャーのdisabled設定
m-pager-prev, m-pager-nextにis-disabledクラスを追加し、aタグを削除してください
===============================================================-->

<?php
echo renderPager(15 , "pager_list" , "" , $url_param);
?>

		<!-- .m-container-inner // --></div>
	<!-- .m-container // --></div>

<!-- #Content // --></div>

<? include_once(dirname(__FILE__).'/../shared/inc/footer.inc'); ?>

<!-- #Page // --></div>

<? include_once(dirname(__FILE__).'/../shared/inc/js.inc'); ?>
<script src="js/index.js"></script>
</body>
</html>
