$(document).ready(function(){
	$('ul#main-nav').superfish({
	  speed:       'fast',
	  delay:       0,
	  animation:   {opacity:'show'}
	});
  $('#contents ul li:nth-child(4n)').addClass('clip-r');
  $('.box-bg dl:nth-child(odd)').addClass('clip-r');
  $('#blog-feed li:nth-child(4n), .box-bg li:nth-child(4n)').addClass('clip-r');

});