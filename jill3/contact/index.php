<?php
require_once(dirname(__FILE__).'/system/config.php');

error_reporting(0);

//完了画面へ遷移
if(!empty($_POST['comp'])){
	$data_list = fn_get_form_param($_POST);
	$data_list = fn_sanitize($data_list);

	$subject = "お問い合わせがありました。";
	
	$body .= "お問い合わせ種別：".$type_select[$data_list['type']]."\r\n";
	$body .= "会社名：".$data_list['company']."\r\n";
	$body .= "部署名：".$data_list['department']."\r\n";
	$body .= "お名前：".$data_list['name']."\r\n";
	$body .= "メールアドレス：".$data_list['email']."\r\n";
	$body .= "電話番号：".$data_list['tel']."\r\n";
	$body .= "お問い合わせ内容：".$data_list['ask']."\r\n";
	$body .= "_______________________________________________________\r\n\r\n";


	//管理者宛メール
	$MAIL->send(ADMIN_MAIL_ADDRESS, $subject, $body, ADMIN_MAIL_ADDRESS);
	
	$subject = "お問い合わせありがとうございました。";
	
	$body = "お問い合わせありがとうございました。\r\n\r\n";
	$body .= "送信いただきました内容を確認後、担当者よりご連絡差し上げます。\r\n";
	$body .= "\r\n";
	$body .= "\r\n";
	$body .= "株式会社ジリオン\r\n";
	
	//関係者宛メール
	$MAIL->send($data_list['email'], $subject, $body, ADMIN_MAIL_ADDRESS);

	header("Location: ./thanks.html");
	exit;
}

//確認画面へ遷移
else if(!empty($_POST['input'])){

	$data_list = fn_get_form_param($_POST);
	$data_list = fn_sanitize($data_list);

	//入力チェック
	$err_check_arr = array(
		"type"=>array("name"=>"お問い合わせ種別","type"=>array("null","alpha_numeric")),
		"company"=>array("name"=>"会社名","type"=>array("null","len"),"length"=>255),
		"name"=>array("name"=>"お名前","type"=>array("null","len"),"length"=>255),
		"email"=>array("name"=>"メールアドレス","type"=>array("null","email","len"),"length"=>255),
		"email_confirm"=>array("name"=>"メールアドレス（確認）","type"=>array("null","email","len"),"length"=>255),
		"tel"=>array("name"=>"電話番号","type"=>array("null","alpha_numeric")),
		"ask"=>array("name"=>"お問い合わせ内容","type"=>array("null")),
	);

	//バリデーション実行
	$err = $VALIDATION->check($data_list,$err_check_arr);

	//メールアドレスのバリデーション追加
	if(empty($err['email']) && empty($err['email_confirm'])){
		if($data_list['email'] != $data_list['email_confirm']){
			$err['email'] = $err['email_confirm'] = 'メールアドレスが一致しません。';
		}
	}
	
	if(empty($err)){
		require_once(FORM_CONFIRM);
	}else{
		require_once(FORM_INPUT);
	}
}

//入力画面へ戻る
else if(!empty($_POST['back'])){
	$data_list = fn_get_form_param($_POST);
	$data_list = fn_sanitize($data_list);
	require_once(FORM_INPUT);
}

//入力画面
else{
	require_once(FORM_INPUT);
}