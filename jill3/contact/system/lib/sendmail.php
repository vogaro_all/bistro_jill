<?php

/**
// ------------------------------------------------------------------------
 * Lib_sendmail.php
 * 
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

require_once(dirname(__FILE__).'/../vendors/qdmail.php');

class sendmail
{
	
    /**
    * @var string ERR_NOT_FOUND_TEMPLATE　テンプレート未検文言
    */
    const ERR_NOT_FOUND_TEMPLATE = '指定のテンプレートファイルが見つかりませんでした。';

	/**
	 *  コンストラクタ
	 * 
	 *  @param  $session Class_Sessionオブジェクト
	 *  @return void
	 */
	function sendmail() {
	}
	
    /**
    *  メール送信
    *
    *  @access public
    *  @param  string $to  送信先アドレス
    *  @param  string $subject  題名
    *  @param  array  $body_data  本文データ
    *  @param  string $from  送信元
     * @param  array  $cc
    *  @return boolean
    */
    function send($to, $subject, $body_data, $from ,$cc = array()) {

        $mail = new Qdmail();

        $mail -> to( $to );
        $mail -> subject( $subject );
        $mail -> text( $body_data);
        $mail -> from( $from );
        
        if(count($cc)) $mail -> cc( $cc );
        
        return $mail ->send();
    }
}
?>