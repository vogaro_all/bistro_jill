<?php

/**
// ------------------------------------------------------------------------
 * Lib_validation.php
 * 
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class validation 
{

        /** @var array  $data  チェック対象データ*/
	var $data;
	
	/** @var array  $err  発見したエラーを入れる配列*/
	var $err = array();
	
	/** @var array  $arr  チェック項目*/
	var $arr;
    
        
	/**
	 *  コンストラクタ
	 * 
	 *  @param  $session Class_Sessionオブジェクト
	 *  @return void
	 */
	function validation() {
        
    }
	
        
    /**
     * チェック対象データをセットしバリデーション実行
     * @param string $data
     * @param array $err_arr 
     */
    function check($data,$err_arr) {

        $this->data = $data;
        $this->arr = $err_arr;
        
        foreach($this->arr as $key=>$value){

            foreach($value["type"] as $value2){

                if(!isset($this->err[$key])){

                    switch($value2){

                        case "null":                        
                            $this->check_null($value["name"],$key);                        
                            break;

                        case "numeric":
                            $this->check_numeric($value["name"],$key);
                            break;

                        case "len":
                            $this->check_length($value["name"],$key,$value["length"]);
                            break;                        

                        case "email":                        
                            $this->check_email($value["name"],$key);                        
                            break;

                        case "alpha" :
                            $this->check_alpha($value["name"],$key);
                            break;

                        case "alpha_numeric" :
                            $this->check_alpha_numeric($value["name"],$key);
                            break;

                        case "null_checkbox":
                            $this->check_null_checkbox($value["name"],$key);
                            break;

                        case "num_checkbox":
                            $this->check_num_checkbox($value["name"],$key,$value["length"]);
                            break;
                        
                        case "output_date":
                            $this->check_output_date($value["name"] , $key);
                            break;

                        case "hold_date":
                            $this->check_hold_date($value["name"] , $key);
                            break;
                        case "kana":
                            $this->check_kana($value["name"] , $key);
                            break;
                    }
                }
            }
        }

        return $this->err;
    }
    
    /**
     *  チェック関数(NULLをチェック)
     *
     *  @access public
     *  @param  string $name  チェックデータ名称
     *  @param  string $key  チェックデータのキー
     *  @return void
     */
    function check_null($name,$key) {
       	
        $val = $this->data[$key];

        if(empty( $val )){
            
            $this->err[$key] = $name."を入力してください。";

        }
        
        return;
    }

    /**
     *  チェック関数(数値型をチェック)
     *
     *  @access public
     *  @param  string $name  チェックデータ名称
     *  @param  string $key  チェックデータのキー
     *  @return void
     */
    function check_numeric($name,$key) {

       	$val = $this->data[$key];
       	
        if(!is_numeric($val)){
            $this->err[$key] = $name."は数値を入力してください。";
        }
        return;
    }
    
    /**
     *  チェック関数(データの長さをチェック)
     *
     *  @access public
     *  @param  string $name  チェックデータ名称
     *  @param  string $key  チェックデータのキー
     *  @param  int $len  チェックする長さ
     *  @return void
     */
    function check_length($name,$key,$len) {
        
       	$val = $this->data[$key];
        $data_len = mb_strlen( $val , "UTF-8");
        if($data_len > $len){
            $this->err[$key] = $name."の文字数がオーバーしています。";
        }
        return;
    }

    /**
     *  チェック関数(メールアドレスをチェック)
     *
     *  @access public
     *  @param  string $name  チェックデータ名称
     *  @param  string $key  チェックデータのキー
     *  @return void
     */
    function check_email($name,$key) {
        
       	$val = $this->data[$key];
       	
        $pat = "/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/";
        if(!preg_match($pat,$val)){
            $this->err[$key] = "メールアドレスの書式が正しくありません";  
        }
        
        return;
    }
    
    /**
     *  チェック関数(半角英字のみかをチェック)
     *
     *  @access public
     *  @param  string $name  チェックデータ名称
     *  @param  string $key  チェックデータのキー
     *  @return void
     */
    function check_alpha($name , $key) {
        
        $val = $this->data[$key];
        if(!empty($val)){
            if(! preg_match("/^([ア-ン゛゜ァ-ォャ-ョｦ-ﾟa-zA-Z0-9 　\-])+$/u", $val))
            {
                $this->err[$key] = $name."は半角英数ハイフンおよびカタカナのみで入力してください。";
            }
        }
        return;
        
    }
    
    /**
     *  チェック関数(半角英数字のみかをチェック)
     *
     *  @access public
     *  @param  string $name  チェックデータ名称
     *  @param  string $key  チェックデータのキー
     *  @return void
     */
    function check_alpha_numeric($name , $key) {
        
        $val = $this->data[$key];
        if(!empty($val)){
            if(! preg_match("/^([0-9])+$/i", $val))
            {
                $this->err[$key] = $name."は半角数字のみで入力してください。";
            }
        }
        return;
    }
    
    
    /**
     *  チェック関数(チェックボックスで選択されているかチェック)
     *
     *  @access public
     *  @param  string $name  チェックデータ名称
     *  @param  string $key  チェックデータのキー
     *  @return void
     */
    function check_null_checkbox($name,$key) {
        
       	$val = $this->data[$key];
       	
        if(!is_array($val) || !count($val)){
				
            $this->err[$key] = $name."の選択をしてください。";

        }
        return;
        
    }

    /**
     *  チェック関数(チェックボックスで指定数以内の選択がされているかをチェック)
     *　※チェックなしの場合は通る
     *  @access public
     *  @param  string $name  チェックデータ名称
     *  @param  string $key  チェックデータのキー
     *  @param  int $len  許可するチェック数
     *  @return void
     */
    function check_num_checkbox($name,$key,$len) {
        
       	$val = $this->data[$key];
       	
        if(is_array($val) &&  count($val) > $len){
				
            $this->err[$key] = $name."の選択数がオーバーしています";

        }
        return;
    }
    
    /**
     *  チェック関数(日付の形式が正しいかをチェック)
     *
     *  @access public
     *  @param  string $name  チェックデータ名称
     *  @param  string $key  チェックデータのキー
     *  @return void
     */
    function check_output_date($name , $key) {
        
        $val = $this->data[$key];
        
        $err = 0;
        if(!preg_match('/^(\d\d\d\d)\/(\d\d)\/(\d\d) (\d\d):(\d\d)$/' , $val) && !preg_match('/^(\d\d\d\d)\-(\d\d)\-(\d\d) (\d\d):(\d\d)$/' , $val))
        {
            if(!preg_match('/^(\d\d\d\d)\/(\d\d)\/(\d\d)$/' , $val) && !preg_match('/^(\d\d\d\d)\-(\d\d)\-(\d\d)$/' , $val))
            {
                $err = 1;
            }
        }
        
        if($err) $this->err[$key] = $name."の書式が正しくありません。";
        
        return;
        
    }

    /**
     *  チェック関数(日付の形式が正しいかをチェック)
     *
     *  @access public
     *  @param  string $name  チェックデータ名称
     *  @param  string $key  チェックデータのキー
     *  @return void
     */
    function check_hold_date($name , $key) {
        
        $val = $this->data[$key];
        
        $err = 0;
        if(!preg_match('/^(\d\d\d\d)\/(\d\d)\/(\d\d)$/' , $val) && !preg_match('/^(\d\d\d\d)\-(\d\d)\-(\d\d)$/' , $val))
        {
            $err = 1;
        }
        
        if($err) $this->err[$key] = $name."の書式が正しくありません。";
        
        return;
        
    }

    /**
     *  チェック関数(ひらがなのみかをチェック)
     *
     *  @access public
     *  @param  string $name  チェックデータ名称
     *  @param  string $key  チェックデータのキー
     *  @return void
     */
    function check_kana($name , $key) {
        
        $val = $this->data[$key];
        if(!empty($val)){
            if(! preg_match("/^[ぁ-ゞ]+$/u",$val))
            {
                $this->err[$key] = $name."はひらがなで入力してください。";
            }
        }
        return;
    }
    

}

?>