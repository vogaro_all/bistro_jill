<?php
ini_set('error_reporting', E_ALL | ~E_STRICT);

require_once(dirname(__FILE__).'/lib/common.php');
require_once(dirname(__FILE__).'/lib/validation.php');
require_once(dirname(__FILE__).'/lib/sendmail.php');

//入力画面テンプレート
define("FORM_INPUT", dirname(__FILE__)."/tmpl/index.php");

//確認画面テンプレート
define("FORM_CONFIRM", dirname(__FILE__)."/tmpl/confirm.php");

//管理者メールアドレス
define("ADMIN_MAIL_ADDRESS", "contact@jillion.co.jp");
//define("ADMIN_MAIL_ADDRESS", "okada@invogue.co.jp,shibata@vogaro.co.jp");
//define("ADMIN_MAIL_ADDRESS", "yoshida@jillion.co.jp");

// お問い合わせ種別
$type_select = array(
		1 => '出店・教育コンサルティングのご依頼・ご相談',
		2 => 'その他',
);

// 希望職種プルダウン
$job_select = array(
		'' => '--',
		1 => 'ホールスタッフ',
		2 => 'キッチンスタッフ',
);

// 雇用形態プルダウン
$job_type_select = array(
		'' => '--',
		1 => 'アルバイト',
		2 => 'パート',
		3 => '店長、料理長',
		4 => '副店長、スーシェフ候補',
);

//バリデーションクラスインスタンス生成
$VALIDATION = new validation();

//メールクラスインスタンス生成
$MAIL = new sendmail();