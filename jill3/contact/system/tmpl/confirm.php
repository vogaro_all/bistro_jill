<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie8 lt-ie9" lang="ja">
<![endif]-->
<!--[if IE 9]>
<html class="ie9 lt-ie9" lang="ja">
<![endif]-->
<!--[if !IE]><!-->
<html lang="ja">
<!--<![endif]-->
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>お問い合わせ｜株式会社ジリオン</title>
<meta name="description" content="お問い合わせフォームです。目黒,中目黒,五反田,学芸大学で、大衆ビストロジル/大衆ビストロ煮ジルを展開しています。">
<meta name="keywords" content="株式会社ジリオン,大衆ビストロジル,煮ジル,肉料理,居酒屋,バル,ワイン,目黒,中目黒,五反田,学芸大学">

<? include_once(dirname(__FILE__).'/../../../shared/inc/head.inc'); ?>
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/confirm.css">

<? include_once(dirname(__FILE__).'/../../../shared/inc/ie.inc'); ?>

</head>

<body>
<? include_once(dirname(__FILE__).'/../../../shared/inc/ga.inc'); ?>

<? include_once(dirname(__FILE__).'/../../../shared/inc/loading.inc'); ?>

<div id="Page">

<? include_once(dirname(__FILE__).'/../../../shared/inc/header.php'); ?>

<div id="Content">

	<header class="m-pageHeader">
		<div class="m-pageHeader-inner">
			<h1 class="m-pageHeader-ttl">
				<img src="images/ttl_page.png" alt="CONTACT" width="270" height="45" class="u-disp-pcImg">
				<img src="images/ttl_page_sp.png" alt="" width="171" height="28" class="u-disp-spImg">
				<span class="m-pageHeader-txt-contact">お問い合わせ</span>
			<!-- .kvTtl // --></h1>
		<!-- .kv-inner // --></div>
	<!-- .kv // --></header>

	<div class="m-container">
		<div class="m-container-inner l-container">

			<div class="m-frame c-shadow">
				<div class="m-frame-inner">
		<form method="POST">
					<div class="m-frame-cornerTop">
						<div class="m-frame-cornerBottom">
							<div class="m-frame-body">

								<div class="m-flow">
									<p class="m-flow-fig">
										<img src="/shared/images/img_flow02.png" alt="確認" width="345" height="84" class="u-disp-pcImg">
										<img src="/shared/images/img_flow02_sp.png" alt="" width="228" height="57" class="u-disp-spImg">
									</p>
									<p class="m-flow-txt">入力内容をご確認の上、「 ENTRY 」ボタンをクリックしてください。</p>
								<!-- .m-flow // --></div>

								<div class="m-form m-form-confirm contactForm">

									<dl class="m-formList">
										<dt class="m-formList-ttl">お問い合わせ種別</dt>
										<dd class="m-formList-cont">
											<?php echo (!empty($data_list['type'])) ? $type_select[$data_list['type']]:'';?>
										</dd>
									</dl>

									<dl class="m-formList">
										<dt class="m-formList-ttl">会社名</dt>
										<dd class="m-formList-cont">
											<?php echo (!empty($data_list['company'])) ? $data_list['company']:'';?>
										</dd>
									</dl>

									<dl class="m-formList">
										<dt class="m-formList-ttl">部署名</dt>
										<dd class="m-formList-cont">
											<?php echo (!empty($data_list['department'])) ? $data_list['department']:'';?>
										</dd>
									</dl>

									<dl class="m-formList">
										<dt class="m-formList-ttl">お名前</dt>
										<dd class="m-formList-cont">
											<?php echo (!empty($data_list['name'])) ? $data_list['name']:'';?>
										</dd>
									</dl>

									<dl class="m-formList">
										<dt class="m-formList-ttl">メールアドレス</dt>
										<dd class="m-formList-cont">
											<?php echo (!empty($data_list['email'])) ? $data_list['email']:'';?>
										</dd>
									</dl>

									<dl class="m-formList">
										<dt class="m-formList-ttl">電話番号</dt>
										<dd class="m-formList-cont">
											<?php echo (!empty($data_list['tel'])) ? $data_list['tel']:'';?>
										</dd>
									</dl>

									<dl class="m-formList">
										<dt class="m-formList-ttl">お問い合わせ内容</dt>
										<dd class="m-formList-cont">
											<?php echo (!empty($data_list['ask'])) ? $data_list['ask']:'';?>
										</dd>
									</dl>

								<!-- .entryForm // --></div>

								<div class="m-formSubmit contactSubmit">
									<div class="m-formSubmit-btns">
										<p class="m-formSubmit-btn"><input name="back" type="submit" value="BACK" class="c-btn c-btn-red"></p>
										<p class="m-formSubmit-btn"><input name="comp" type="submit" value="ENTRY" class="c-btn c-btn-black"></p>
									<!-- .entrySubmit-btns // --></div>
								<!-- .entrySubmit // --></div>

							<!-- .m-frame-body // --></div>
						<!-- .m-frame-cornerBottom // --></div>
						<?php foreach ($data_list as $key => $value) : ?>
			<input type="hidden" name="<?php echo $key; ?>" value="<?php echo $value; ?>">
			<?php endforeach; ?>

					<!-- .m-frame-cornerTop // --></div>
				</form>
				<!-- .m-frame-inner // --></div>
			<!-- .m-frame // --></div>

		<!-- .m-container-inner // --></div>
	<!-- .m-container // --></div>

<!-- #Content // --></div>

<? include_once(dirname(__FILE__).'/../../../shared/inc/footer.inc'); ?>

<!-- #Page // --></div>

<? include_once(dirname(__FILE__).'/../../../shared/inc/js.inc'); ?>
<script src="js/common.js"></script>
</body>
</html>
