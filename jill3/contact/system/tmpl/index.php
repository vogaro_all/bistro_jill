<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie8 lt-ie9" lang="ja">
<![endif]-->
<!--[if IE 9]>
<html class="ie9 lt-ie9" lang="ja">
<![endif]-->
<!--[if !IE]><!-->
<html lang="ja">
<!--<![endif]-->
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>お問い合わせ｜株式会社ジリオン</title>
<meta name="description" content="お問い合わせフォームです。目黒,中目黒,五反田,学芸大学で、大衆ビストロジル/大衆ビストロ煮ジルを展開しています。">
<meta name="keywords" content="株式会社ジリオン,大衆ビストロジル,煮ジル,肉料理,居酒屋,バル,ワイン,目黒,中目黒,五反田,学芸大学">

<? include_once(dirname(__FILE__).'/../../../shared/inc/head.inc'); ?>
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/index.css">

<? include_once(dirname(__FILE__).'/../../../shared/inc/ie.inc'); ?>

</head>

<body>
<? include_once(dirname(__FILE__).'/../../../shared/inc/ga.inc'); ?>

<? include_once(dirname(__FILE__).'/../../../shared/inc/loading.inc'); ?>

<div id="Page">

<? include_once(dirname(__FILE__).'/../../../shared/inc/header.php'); ?>

<div id="Content">

	<header class="m-pageHeader">
		<div class="m-pageHeader-inner">
			<h1 class="m-pageHeader-ttl">
				<img src="images/ttl_page.png" alt="CONTACT" width="270" height="45" class="u-disp-pcImg">
				<img src="images/ttl_page_sp.png" alt="" width="171" height="28" class="u-disp-spImg">
				<span class="m-pageHeader-txt-contact">お問い合わせ</span>
			<!-- .kvTtl // --></h1>
		<!-- .kv-inner // --></div>
	<!-- .kv // --></header>

	<div class="m-container">
		<div class="m-container-inner l-container">

			<div class="m-frame c-shadow">
				<div class="m-frame-inner">

					<div class="m-frame-cornerTop">
						<div class="m-frame-cornerBottom">
							<div class="m-frame-body">

								<div class="m-flow">
									<p class="m-flow-fig">
										<img src="/shared/images/img_flow01.png" alt="入力" width="345" height="84" class="u-disp-pcImg">
										<img src="/shared/images/img_flow01_sp.png" alt="" width="228" height="57" class="u-disp-spImg">
									</p>
									<p class="m-flow-txt">以下の空欄に必要事項をご記入ください。内容を確認致しまして、必要な場合に限り、折り返しのご連絡をさせていただきます。<br>内容によりましては、必ずしもご返信差し上げることができない場合もございますので、予めご了承ください。</p>
								<!-- .m-flow // --></div>

								<form action="./index.php" method="post">

									<div class="m-form contactForm">

										<dl class="m-formList m-formList-required contactFormList-type">
											<dt class="m-formList-ttl">お問い合わせ種別</dt>
											<dd class="m-formList-cont">
										<?php if(!empty($err['type'])): ?>
										<p class="m-formList-error"><?php echo $err['type']; ?></p>
					<?php endif; ?>
												<span class="c-radio">
												<input type="radio" name="type" value="1" id="Type01" <?php echo (!empty($data_list['type']) && $data_list['type'] = 1) ? 'checked':'';?>>
													<label for="Type01">出店・教育コンサルティングのご依頼・ご相談</label>
												</span>
												<span class="c-radio">
												<input type="radio" name="type" value="2" id="Type02" <?php echo (!empty($data_list['type']) && $data_list['type'] = 2) ? 'checked':'';?>>
													<label for="Type02">その他</label>
												</span>
												<p class="link"><a href="/recruit/entry/">採用のエントリーはコチラ</a></p>
											</dd>
										</dl>

										<dl class="m-formList m-formList-required contactFormList-company">
											<dt class="m-formList-ttl">会社名</dt>
											<dd class="m-formList-cont">
											<?php if(!empty($err['company'])): ?>
										<p class="m-formList-error"><?php echo $err['company']; ?></p>
					<?php endif; ?>
												<input type="text" name="company" class="c-input" value="<?php echo (!empty($data_list['company'])) ? $data_list['company']:'';?>">
											</dd>
										</dl>

										<dl class="m-formList contactFormList-department">
											<dt class="m-formList-ttl">部署名</dt>
											<dd class="m-formList-cont">
												<input type="text" name="department" class="c-input" value="<?php echo (!empty($data_list['department'])) ? $data_list['department']:'';?>">
											</dd>
										</dl>

										<dl class="m-formList m-formList-required contactFormList-name">
											<dt class="m-formList-ttl">お名前</dt>
											<dd class="m-formList-cont">
											<?php if(!empty($err['name'])): ?>
										<p class="m-formList-error"><?php echo $err['name']; ?></p>
					<?php endif; ?>
												<input type="text" name="name" class="c-input" value="<?php echo (!empty($data_list['name'])) ? $data_list['name']:'';?>">
											</dd>
										</dl>

										<dl class="m-formList m-formList-required contactFormList-email">
											<dt class="m-formList-ttl">メールアドレス</dt>
											<dd class="m-formList-cont">
										<?php if(!empty($err['email'])): ?>
										<p class="m-formList-error"><?php echo $err['email']; ?></p>
					<?php endif; ?>
												<input type="text" name="email" class="c-input" value="<?php echo (!empty($data_list['email'])) ? $data_list['email']:'';?>">
											</dd>
										</dl>
										<dl class="m-formList m-formList-required contactFormList-emailConfirm">
											<dt class="m-formList-ttl">メールアドレス（確認）</dt>
											<dd class="m-formList-cont">
										<?php if(!empty($err['email_confirm'])): ?>
										<p class="m-formList-error"><?php echo $err['email_confirm']; ?></p>
					<?php endif; ?>
												<input type="text" name="email_confirm" class="c-input" value="<?php echo (!empty($data_list['email_confirm'])) ? $data_list['email_confirm']:'';?>">
											</dd>
										</dl>

										<dl class="m-formList m-formList-required contactFormList-tel">
											<dt class="m-formList-ttl">電話番号</dt>
											<dd class="m-formList-cont">
										<?php if(!empty($err['tel'])): ?>
										<p class="m-formList-error"><?php echo $err['tel']; ?></p>
					<?php endif; ?>
												<input type="text" name="tel" class="c-input" value="<?php echo (!empty($data_list['tel'])) ? $data_list['tel']:'';?>">
											</dd>
										</dl>

										<dl class="m-formList m-formList-required contactFormList-ask">
											<dt class="m-formList-ttl">お問い合わせ内容</dt>
											<dd class="m-formList-cont">
										<?php if(!empty($err['ask'])): ?>
										<p class="m-formList-error"><?php echo $err['ask']; ?></p>
					<?php endif; ?>
												<textarea name="ask" cols="30" rows="10" class="c-text" placeholder="例) 現在飲食店の新規出店を考えており、出店コンセプト立案やサービス設計に関して相談したい"><?php echo (!empty($data_list['ask'])) ? $data_list['ask']:'';?></textarea>
											</dd>
										</dl>

									<!-- .entryForm // --></div>

									<div class="m-privacy">
										<p class="m-privacyTxt">ご送信の前に、必ず「個人情報の取扱いについて」の内容をお読みのうえ、ご同意願います。</p>
										<div class="m-privacyCont">
											<div class="m-privacyCont-inner">

												<div class="m-privacyList">
													<p class="m-privacyList-ttl">（１）事業者の氏名または名称</p>
													<p class="m-privacyList-txt">株式会社ジリオン</p>
												</div>

												<div class="m-privacyList">
													<p class="m-privacyList-ttl">（２）個人情報保護管理者</p>
													<p class="m-privacyList-txt">吉田裕司</p>
												</div>

												<div class="m-privacyList">
													<p class="m-privacyList-ttl">（３）個人情報の利用目的 ・当社事業に関してお問い合わせいただいた内容に回答するため 。</p>
												</div>

												<div class="m-privacyList">
													<p class="m-privacyList-ttl">（４）個人情報の第三者提供について</p>
													<p class="m-privacyList-txt">取得した個人情報は法令等による場合を除いて第三者に提供することはありません。</p>
												</div>

												<div class="m-privacyList">
													<p class="m-privacyList-ttl">（５）個人情報の取扱いの委託について</p>
													<p class="m-privacyList-txt">取得した個人情報の取扱いの全部又は、一部を委託することがあります。</p>
												</div>

												<div class="m-privacyList">
													<p class="m-privacyList-ttl">（６）開示対象個人情報の開示等および問い合わせ窓口について</p>
													<p class="m-privacyList-txt">ご本人からの求めにより、当社が保有する開示対象個人情報の利用目的の通知・開示・内容の訂正・追加または削除・利用の停止・消去および第三者への提供の停止（「開示等」といいます。）に応じます。開示等に応ずる窓口は、お問合せいただきました当該店舗になります。</p>
												</div>

												<div class="m-privacyList">
													<p class="m-privacyList-ttl">（７）個人情報の安全管理措置について</p>
													<p class="m-privacyList-txt">取得した個人情報については、漏洩、減失またはき損の防止と是正、その他個人情報の安全管理のために必要かつ適切な措置を講じます。<br>お問合せへの回答後、取得した個人情報は当社内において削除致します。</p>
												</div>

												<div class="m-privacyList">
													<p class="m-privacyList-ttl">（８）個人情報保護方針</p>
													<p class="m-privacyList-txt">当社ホームページの個人情報保護方針をご覧下さい。</p>
												</div>

												<div class="m-privacyList">
													<p class="m-privacyList-ttl">（９）個人情報を与えなかった場合に生じる結果</p>
													<p class="m-privacyList-txt">個人情報を与えることは任意です。お問い合わせいただいた内容に回答するために必要な個人情報をご提供いただけない場合は、お問い合わせいただいた内容に回答できない場合がございますので了承願います。</p>
												</div>

												<div class="m-privacyList">
													<p class="m-privacyList-ttl">（１０）当社の個人情報の取扱いに関する苦情、相談等の問合せ先</p>
													<p class="m-privacyList-txt">
														窓口の名称 お問い合わせ窓口<br>
														連絡先 住所 ：〒141-0021<br>
														東京都品川区上大崎2-26-5　メグロードビル地下1階<br>
														電話 ：03-6417-0015</p>
												</div>

											<!-- .m-privacyCont-inner // --></div>
										<!-- .m-privacyCont // --></div>
									<!-- .m-privacy // --></div>

									<div class="m-formSubmit contactSubmit js-checkSubmit">
										<p class="m-formSubmit-agree">
											<span class="c-check">
												<input type="checkbox" id="Agree" class="js-checkSubmit-trigger">
												<label for="Agree">個人情報の取扱いに同意する</label>
											</span>
										</p>
										<div class="m-formSubmit-btns">
											<p class="m-formSubmit-btn"><input type="submit" value="NEXT" class="c-btn c-btn-black js-checkSubmit-target" disabled></p>
										<!-- .entrySubmit-btns // --></div>
									<!-- .entrySubmit // --></div>
					<input type="hidden" name="input" value="1">
								</form>

							<!-- .m-frame-body // --></div>
						<!-- .m-frame-cornerBottom // --></div>
					<!-- .m-frame-cornerTop // --></div>

				<!-- .m-frame-inner // --></div>
			<!-- .m-frame // --></div>

		<!-- .m-container-inner // --></div>
	<!-- .m-container // --></div>

<!-- #Content // --></div>

<? include_once(dirname(__FILE__).'/../../../shared/inc/footer.inc'); ?>

<!-- #Page // --></div>

<? include_once(dirname(__FILE__).'/../../../shared/inc/js.inc'); ?>
<script src="js/common.js"></script>

</body>
</html>
