<?php
require_once(dirname(__FILE__).'/system/config.php');

error_reporting(0);

//完了画面へ遷移
if(!empty($_POST['comp'])){
	$data_list = fn_get_form_param($_POST);
	$data_list = fn_sanitize($data_list);

	$subject = "お問い合わせがありました。";
	
	$body .= "お名前：".$data_list['name']."\r\n";
	$body .= "生年月日：".$data_list['birth_year']."年".$data_list['birth_month']."月".$data_list['birth_day']."日"."\r\n";
	$body .= "性別：".$gender_select[$data_list['gender']]."\r\n";
	$body .= "メールアドレス：".$data_list['email']."\r\n";
	$body .= "電話番号：".$data_list['tel']."\r\n";
	$body .= "希望職種：".$job_select[$data_list['job']]."\r\n";
	$body .= "希望雇用形態：".$job_type_select[$data_list['job_type']]."\r\n";
	$body .= "採用に関してのご希望ご質問：".$data_list['ask']."\r\n";
	$body .= "_______________________________________________________\r\n\r\n";


	//管理者宛メール
	$MAIL->send(ADMIN_MAIL_ADDRESS, $subject, $body, ADMIN_MAIL_ADDRESS);
	
	$subject = "お問い合わせありがとうございました。";
	
	$body = "お問い合わせありがとうございました。\r\n\r\n";
	$body .= "送信いただきました内容を確認後、担当者よりご連絡差し上げます。\r\n";
	$body .= "\r\n";
	$body .= "\r\n";
	$body .= "株式会社ジリオン\r\n";
	
	//関係者宛メール
	$MAIL->send($data_list['email'], $subject, $body, ADMIN_MAIL_ADDRESS);

	header("Location: ./kanda_thanks.html");
	exit;
}

//確認画面へ遷移
else if(!empty($_POST['input'])){
	$data_list = fn_get_form_param($_POST);
	$data_list = fn_sanitize($data_list);

	//入力チェック
	$err_check_arr = array(
		"name"=>array("name"=>"お名前","type"=>array("null","len"),"length"=>255),
		"birth_year"=>array("name"=>"生年月日 年","type"=>array("null","alpha_numeric")),
		"birth_month"=>array("name"=>"生年月日 月","type"=>array("null","alpha_numeric")),
		"birth_day"=>array("name"=>"生年月日 日","type"=>array("null","alpha_numeric")),
		"gender"=>array("name"=>"性別","type"=>array("null","alpha_numeric")),
		"email"=>array("name"=>"メールアドレス","type"=>array("null","email","len"),"length"=>255),
		"email_confirm"=>array("name"=>"メールアドレス（確認）","type"=>array("null","email","len"),"length"=>255),
		"tel"=>array("name"=>"電話番号","type"=>array("null","alpha_numeric")),
		"job"=>array("name"=>"希望職種","type"=>array("null","alpha_numeric")),
		"job_type"=>array("name"=>"希望雇用形態","type"=>array("null","alpha_numeric"))
	);

	//バリデーション実行
	$err = $VALIDATION->check($data_list,$err_check_arr);

	//メールアドレスのバリデーション追加
	if(empty($err['email']) && empty($err['email_confirm'])){
		if($data_list['email'] != $data_list['email_confirm']){
			$err['email'] = $err['email_confirm'] = 'メールアドレスが一致しません。';
		}
	}
	
	// 日付のバリデーション追加
	if(empty($err['birth_year']) && empty($err['birth_month']) && empty($err['birth_day'])){
		if (!(checkdate($data_list['birth_month'], $data_list['birth_day'], $data_list['birth_year']))) {
			$err['birth_year'] = '正しい日付ではありません。';
		}
	}
	
	if(empty($err)){
		require_once(FORM_CONFIRM);
	}else{
		require_once(FORM_INPUT);
	}
}

//入力画面へ戻る
else if(!empty($_POST['back'])){
	$data_list = fn_get_form_param($_POST);
	$data_list = fn_sanitize($data_list);
	require_once(FORM_INPUT);
}

//入力画面
else{
	require_once(FORM_INPUT);
}