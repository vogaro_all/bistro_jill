<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie8 lt-ie9" lang="ja">
<![endif]-->
<!--[if IE 9]>
<html class="ie9 lt-ie9" lang="ja">
<![endif]-->
<!--[if !IE]><!-->
<html lang="ja">
<!--<![endif]-->
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>目黒のビストロなら大衆ビストロ ジル</title>
<meta name="description" content="新規レストランを一緒に作り上げるオープニングメンバー大募集!!”大衆ビストロ”という斬新な飲食店モデルでヒット連発！ビストロJILLの新業態「JB」　７/1 GRAND OPEN!">
<meta name="keywords" content="神田,和ビストロ,JB,肉料理,ワイン">

<?php include_once(dirname(__FILE__)."/../../shared/inc/head.inc"); ?>
<link rel="stylesheet" href="css/confirm.css">

<?php include_once(dirname(__FILE__)."/../../shared/inc/ie.inc"); ?>

</head>

<body>
<?php include_once(dirname(__FILE__)."/../../shared/inc/ga.inc"); ?>

<div id="Page">

<?php include_once(dirname(__FILE__)."/../../shared/inc/header.inc"); ?>

<div id="Content">

  <header class="m-kv">
    <div class="m-kv-inner">
      <h1 class="m-kv-ttl">
        <img src="images/kanda/ttl_page.png" alt="JB 神田店 NEW OPEN 2016.7.1">
      <!-- .kvTtl // --></h1>
    <!-- .kv-inner // --></div>
  <!-- .kv // --></header>

  <section class="entry" id="entry">
    <div class="entry-inner">

      <h2 class="entryTtl c-borderTtl">
        <img src="shared/images/ttl_entry.png" alt="ENTRY" width="195" height="44" class="u-disp-pcImg">
        <img src="shared/images/ttl_entry_sp.png" alt="" width="112" height="25" class="u-disp-spImg">
      </h2>

      <div class="m-frame entryFrame c-shadow">
        <div class="m-frame-inner">

		<form method="POST">
          <div class="m-frame-cornerTop">
            <div class="m-frame-cornerBottom">
            <div class="m-frame-body entryFrame-body">

              <div class="m-flow">
                <p class="m-flow-fig">
                  <img src="shared/images/img_flow02.png" alt="確認" width="345" height="84" class="u-disp-pcImg">
                  <img src="shared/images/img_flow02_sp.png" alt="" width="228" height="57" class="u-disp-spImg">
                </p>
                <p class="m-flow-txt">入力内容をご確認の上、「 ENTRY 」ボタンをクリックしてください。</p>
              <!-- .m-flow // --></div>

              <div class="m-form m-form-confirm entryForm">

                <dl class="m-formList">
                  <dt class="m-formList-ttl">お名前</dt>
                  <dd class="m-formList-cont"><?php echo (!empty($data_list['name'])) ? $data_list['name']:'';?></dd>
                </dl>
                <dl class="m-formList">
                  <dt class="m-formList-ttl">生年月日</dt>
                  <dd class="m-formList-cont"><?php echo (!empty($data_list['birth_year'])) ? $data_list['birth_year']:'';?>年<?php echo (!empty($data_list['birth_month'])) ? $data_list['birth_month']:'';?>月 <?php echo (!empty($data_list['birth_day'])) ? $data_list['birth_day']:'';?>日</dd>
                </dl>
                <dl class="m-formList">
                  <dt class="m-formList-ttl">性別</dt>
                  <dd class="m-formList-cont"><?php echo (!empty($data_list['gender'])) ? $gender_select[$data_list['gender']]:'';?></dd>
                </dl>
                <dl class="m-formList">
                  <dt class="m-formList-ttl">メールアドレス</dt>
                  <dd class="m-formList-cont"><?php echo (!empty($data_list['email'])) ? $data_list['email']:'';?></dd>
                </dl>
                <dl class="m-formList">
                  <dt class="m-formList-ttl">電話番号</dt>
                  <dd class="m-formList-cont"><?php echo (!empty($data_list['tel'])) ? $data_list['tel']:'';?></dd>
                </dl>
                <dl class="m-formList">
                  <dt class="m-formList-ttl">希望職種</dt>
                  <dd class="m-formList-cont"><?php echo (!empty($data_list['job'])) ? $job_select[$data_list['job']]:'';?></dd>
                </dl>
                <dl class="m-formList">
                  <dt class="m-formList-ttl">希望雇用形態</dt>
                  <dd class="m-formList-cont"><?php echo (!empty($data_list['job_type'])) ? $job_type_select[$data_list['job_type']]:'';?></dd>
                </dl>
                <dl class="m-formList">
                  <dt class="m-formList-ttl">採用に関しての<br class="u-disp-pc">ご希望ご質問</dt>
                  <dd class="m-formList-cont"><?php echo (!empty($data_list['ask'])) ? $data_list['ask']:'';?></dd>
                </dl>
              <!-- .entryFormLists // --></div>

              <div class="m-formSubmit entrySubmit">
                <div class="m-formSubmit-btns">
                  <p class="m-formSubmit-btn"><input name="back" type="submit" value="BACK" class="c-btn c-btn-form02 c-btn-back"></p>
                  <p class="m-formSubmit-btn"><input name="comp" type="submit" value="ENTRY" class="c-btn c-btn-form01 c-btn-entry"></p>
                </div>
              <!-- .entrySubmit // --></div>

            <!-- .m-frame-body // --></div>
            <!-- .m-frame-cornerBottom // --></div>
            <?php foreach ($data_list as $key => $value) : ?>
			<input type="hidden" name="<?php echo $key; ?>" value="<?php echo $value; ?>">
			<?php endforeach; ?>
            </form>
          <!-- .m-frame-cornerTop // --></div>

        <!-- .m-frame-inner // --></div>
      <!-- .m-frame // --></div>

    <!-- .entry-inner // --></div>
  <!-- .entry // --></section>

<!-- #Content // --></div>

<?php include_once(dirname(__FILE__)."/../../shared/inc/footer.inc"); ?>

<!-- #Page // --></div>

<?php include_once(dirname(__FILE__)."/../../shared/inc/js.inc"); ?>

</body>
</html>
