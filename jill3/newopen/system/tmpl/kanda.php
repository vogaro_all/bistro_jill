<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie8 lt-ie9" lang="ja">
<![endif]-->
<!--[if IE 9]>
<html class="ie9 lt-ie9" lang="ja">
<![endif]-->
<!--[if !IE]><!-->
<html lang="ja">
<!--<![endif]-->
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>目黒のビストロなら大衆ビストロ ジル</title>
<meta name="description" content="新規レストランを一緒に作り上げるオープニングメンバー大募集!!”大衆ビストロ”という斬新な飲食店モデルでヒット連発！ビストロJILLの新業態「JB」　７/1 GRAND OPEN!">
<meta name="keywords" content="神田,和ビストロ,JB,肉料理,ワイン">

<?php include_once(dirname(__FILE__)."/../../shared/inc/head.inc"); ?>
<link rel="stylesheet" href="css/index.css">

<?php include_once(dirname(__FILE__)."/../../shared/inc/ie.inc"); ?>

</head>

<body>
<?php include_once(dirname(__FILE__)."/../../shared/inc/ga.inc"); ?>

<div id="Page">

<?php include_once(dirname(__FILE__)."/../../shared/inc/header.inc"); ?>

<div id="Content">

  <header class="m-kv">
    <div class="m-kv-inner">
      <h1 class="m-kv-ttl">
        <img src="images/kanda/ttl_page.png" alt="JB 神田店 NEW OPEN 2016.7.1">
      <!-- .kvTtl // --></h1>
    <!-- .kv-inner // --></div>
  <!-- .kv // --></header>

  <div class="tab u-disp-pc">
    <ul class="tab-lists">
      <li><a href="#ConceptScroll" class="js-scroll"><span><img src="shared/images/txt_tab01.png" alt="CONCEPT" width="82" height="13"></span></a></li>
      <li><a href="#RecruitScroll" class="js-scroll"><span><img src="shared/images/txt_tab02.png" alt="RECRUIT" width="80" height="13"></span></a></li>
      <li><a href="#EntryScroll" class="js-scroll"><span><img src="shared/images/txt_tab03.png" alt="ENTRY" width="60" height="13"></span></a></li>
    </ul>
  <!-- .tab // --></div>

  <section id="Concept" class="concept section">
    <div class="conceptCont">
      <div class="conceptCont-inner">
        <h2 id="ConceptScroll" class="conceptTtl c-borderTtl">
          <img src="shared/images/ttl_concept.png" alt="CONCEPT" width="273" height="45" class="u-disp-pcImg">
          <img src="shared/images/ttl_concept_sp.png" alt="" width="157" height="26" class="u-disp-spImg">
        </h2>
        <p class="conceptTxt">"熟成""発酵"など日本古来の伝統調理技法を駆使したフレンチのシェフが<br>手掛けるビストロ料理と、ソムリエ厳選のワイン、<br>豊富な日本酒類が楽しめる　日本の四季や旬に特化した<和ビストロ></p>
      <!-- .conceptCont-inner // --></div>
    <!-- .conceptCont // --></div>
    <div class="conceptFig"></div>
  <!-- .concept // --></section>

  <section id="Recruit" class="recruit section">
    <div class="recruit-inner">

      <div class="m-frame c-shadow recruitFrame">
        <div class="m-frame-inner">

          <div class="m-frame-cornerTop">
            <div class="m-frame-cornerBottom">
              <div class="m-frame-body recruitFrame-body">

                <h2 id="RecruitScroll" class="recruitTtl c-borderTtl">
                  <img src="shared/images/ttl_recruit.png" alt="RECRUIT" width="264" height="40" class="u-disp-pcImg">
                  <img src="shared/images/ttl_recruit_sp.png" alt="" width="149" height="23" class="u-disp-spImg">
                </h2>
                <p class="recruitLead">ジリオンへのエントリーをご希望の方は、募集概要をご確認頂き採用エントリーフォームよりご応募ください。</p>

                <div class="recruitLists">

                  <dl class="recruitList recruitList-job">
                    <dt class="recruitList-ttl">募集職種</dt>
                    <dd class="recruitList-cont">
                      <ul>
                        <li>1. ホールスタッフ、キッチンスタッフ（一般社員）</li>
                        <li>2. 副店長、スーシェフ候補</li>
                        <li>3. 店長、料理長</li>
                        <li>4. アルバイト(キッチン・ホール）/パート</li>
                      </ul>
                    </dd>
                  <!-- .recruitList // --></dl>

                  <dl class="recruitList recruitList-genre">
                    <dt class="recruitList-ttl">雇用形態</dt>
                    <dd class="recruitList-cont">正社員, アルバイト</dd>
                  <!-- .recruitList // --></dl>

                  <dl class="recruitList recruitList-qualify">
                    <dt class="recruitList-ttl">応募資格</dt>
                    <dd class="recruitList-cont">
                      <p class="recruitList-lead">オープニングスタッフとしてご活躍していただける方（既存店舗でも採用可能）飲食未経験者も大歓迎！</p>
                      <dl class="recruitList-ttlList">
                        <dt>【キッチン】</dt>
                        <dd>
                          <ul>
                            <li>・洋食店、特にビストロ調理経験をお持ちの方は優遇。</li>
                            <li>・洋食経験者で和食の技法を取り入れてみたい、和食経験者でビストロの技法を取り入れてみたい方</li>
                          </ul>
                        </dd>
                      </dl>
                      <dl class="recruitList-ttlList">
                        <dt>【ホール】</dt>
                        <dd>
                          <ul>
                            <li>・ホールスタッフは他業界の（サービス業）などでの接客経験や居酒屋での接客経験者大歓迎！平日ランチパートも大歓迎！</li>
                            <li></li>
                          </ul>
                        </dd>
                      </dl>
                    </dd>
                  <!-- .recruitList // --></dl>

                  <dl class="recruitList recruitList-time">
                    <dt class="recruitList-ttl">勤務時間</dt>
                    <dd class="recruitList-cont">
                      <p>10:00～23:30の間でシフト制</p>
                      <ul>
                        <li>※月の勤務時間は、平均220時間</li>
                        <li>※休憩あり、終電OK</li>
                        <li>※アルバイトは週2日～1日4時間以上勤務</li>
                        <li>※平日ランチのみもOK</li>
                      </ul>
                    </dd>
                  <!-- .recruitList // --></dl>

                  <dl class="recruitList recruitList-pay">
                    <dt class="recruitList-ttl">給与</dt>
                    <dd class="recruitList-cont">
                      <dl>
                        <dt>【給与イメージ】</dt>
                        <dd>
                          <p>キッチンスタッフ～料理長候補</p>
                          <p>ホールスタッフ～店長候補</p>
                          <p>月給30万円～35万円+インセンティブ+賞与/年収約450～500万円</p>
                          <ul>
                            <li>1. 月給25万円～30万円+賞与年2回+諸手当</li>
                            <li>2. 月給28万円～35万円+賞与年3回+インセンティブ+諸手当</li>
                            <li>3. 月給30万円～45万円+賞与年3回+インセンティブ+諸手当</li>
                            <li>4. 時給1000円～※週5日勤務で時給アップ</li>
                          </ul>
                          <p>※経験・能力等考慮の上決定します。</p>
                        </dd>
                      </dl>
                    </dd>
                  <!-- .recruitList // --></dl>

                  <dl class="recruitList recruitList-dayoff">
                    <dt class="recruitList-ttl">休日・休暇</dt>
                    <dd class="recruitList-cont">
                      <p>年間100日以上（週休2日制シフト）、年末年始、有給休暇、他<br>※新店舗は日曜・祝日定休</p>
                    </dd>
                  <!-- .recruitList // --></dl>

                  <dl class="recruitList recruitList-treat">
                    <dt class="recruitList-ttl">待遇</dt>
                    <dd class="recruitList-cont">
                      <ul>
                        <li>・交通費支給</li>
                        <li>・店長, 料理長, 副店長, 副料理長は賞与年3回（3半期毎）、 一般社員（管理職以外）は年2回賞与支給</li>
                        <li>・昇給有</li>
                        <li>・保険・厚生年金・雇用・労災保険</li>
                        <li>・食事付き</li>
                        <li>・有給休暇</li>
                        <li>・制服貸与</li>
                        <li>・残業手当</li>
                        <li>・深夜手当</li>
                        <li>・休日出勤手当</li>
                        <li>・独立支援制度</li>
                        <li>・研修期間有</li>
                        <li>・充実の研修システム。社内だけでなく外部期間での勉強会も開催（無料）</li>
                        <li>・現場メインの人事評価システム</li>
                        <li>・PAからの正社員登用制度あり</li>
                        <li>・アルバイトは時給UP有</li>
                      </ul>
                    </dd>
                  <!-- .recruitList // --></dl>
                <!-- .recruitLists // --></div>

                <p class="recruitCaution">※募集状況は各店舗ごとに異なります。ご希望の店舗がある場合はお電話か採用受付フォームよりお問い合わせください。</p>

              <!-- .recruitBody-cont // --></div>
            <!-- .recruitBody-inner // --></div>
          <!-- .recruitBody-outer // --></div>

        <!-- .recruitCont-inner // --></div>
      <!-- .recruitCont // --></div>

    <!-- .recruit-inner // --></div>
  <!-- .recruit // --></section>

  <section id="Entry" class="entry">
    <div class="entry-inner">

      <h2 id="EntryScroll" class="entryTtl c-borderTtl">
        <img src="shared/images/ttl_entry.png" alt="ENTRY" width="195" height="44" class="u-disp-pcImg">
        <img src="shared/images/ttl_entry_sp.png" alt="" width="112" height="25" class="u-disp-spImg">
      </h2>

      <div class="m-frame c-shadow entryFrame" id="entry">
        <div class="m-frame-inner">

          <div class="m-frame-cornerTop">
            <div class="m-frame-cornerBottom">
            <div class="m-frame-body entryFrame-body">

              <div class="m-flow">
                <p class="m-flow-fig">
                  <img src="shared/images/img_flow01.png" alt="入力" width="345" height="84" class="u-disp-pcImg">
                  <img src="shared/images/img_flow01_sp.png" alt="" width="228" height="57" class="u-disp-spImg">
                </p>
                <p class="m-flow-txt">以下の空欄に必要事項をご記入ください。内容を確認致しまして、必要な場合に限り、折り返しのご連絡をさせていただきます。<br>内容によりましては、必ずしもご返信差し上げることができない場合もございますので、予めご了承ください。</p>
              <!-- .m-flow // --></div>

              <form method="POST" action="./kanda.php#entry">

                <div class="m-form entryForm">

                  <dl class="m-formList m-formList-required entryFormList-name">
                    <dt class="m-formList-ttl">お名前</dt>
                    <dd class="m-formList-cont">
                    
                    <?php if(!empty($err['name'])): ?>
                    <p class="m-formList-error"><?php echo $err['name']; ?></p>
					<?php endif; ?>
                      <input type="text" name="name" class="c-input" value="<?php echo (!empty($data_list['name'])) ? $data_list['name']:'';?>">
                    </dd>
                  </dl>
                  <dl class="m-formList m-formList-required entryFormList-birth">
                    <dt class="m-formList-ttl">生年月日</dt>
                    <dd class="m-formList-cont">
                    <?php if(!empty($err['birth_year'])): ?>
                    <p class="m-formList-error"><?php echo $err['birth_year']; ?></p>
					<?php endif; ?>
					<?php if(!empty($err['birth_month'])): ?>
                    <p class="m-formList-error"><?php echo $err['birth_month']; ?></p>
					<?php endif; ?>
					<?php if(!empty($err['birth_day'])): ?>
                    <p class="m-formList-error"><?php echo $err['birth_day']; ?></p>
					<?php endif; ?>
                      <span class="u-disp-pc">西暦</span>
                      <input type="text" name="birth_year" class="c-input year" value="<?php echo (!empty($data_list['birth_year'])) ? $data_list['birth_year']:'';?>">年
                      <input type="text" name="birth_month" class="c-input month" value="<?php echo (!empty($data_list['birth_month'])) ? $data_list['birth_month']:'';?>">月
                      <input type="text" name="birth_day" class="c-input day" value="<?php echo (!empty($data_list['birth_day'])) ? $data_list['birth_day']:'';?>">日
                    </dd>
                  </dl>
                  <dl class="m-formList m-formList-required entryFormList-gender">
                    <dt class="m-formList-ttl">性別</dt>
                    <dd class="m-formList-cont">
                    <?php if(!empty($err['gender'])): ?>
                    <p class="m-formList-error"><?php echo $err['gender']; ?></p>
					<?php endif; ?>
                      <span class="c-radio">
                        <input type="radio" name="gender" value="1" id="GenderM" <?php echo (!empty($data_list['gender']) && $data_list['gender'] = 1) ? 'checked':'';?>>
                        <label for="GenderM">男性</label>
                      </span>
                      <span class="c-radio">
                        <input type="radio" name="gender" value="2" id="GenderL" <?php echo (!empty($data_list['gender']) && $data_list['gender'] = 2) ? 'checked':'';?>>
                        <label for="GenderL">女性</label>
                      </span>
                    </dd>
                  </dl>
                  <dl class="m-formList m-formList-required entryFormList-email">
                    <dt class="m-formList-ttl">メールアドレス</dt>
                    <dd class="m-formList-cont">
                    <?php if(!empty($err['email'])): ?>
                    <p class="m-formList-error"><?php echo $err['email']; ?></p>
					<?php endif; ?>
                      <input type="text" name="email" class="c-input" value="<?php echo (!empty($data_list['email'])) ? $data_list['email']:'';?>">
                    </dd>
                  </dl>
                  <dl class="m-formList m-formList-required entryFormList-emailConfirm">
                    <dt class="m-formList-ttl">メールアドレス（確認）</dt>
                    <dd class="m-formList-cont">
                    <?php if(!empty($err['email_confirm'])): ?>
                    <p class="m-formList-error"><?php echo $err['email_confirm']; ?></p>
					<?php endif; ?>
                      <input type="text" name="email_confirm" class="c-input" value="<?php echo (!empty($data_list['email_confirm'])) ? $data_list['email_confirm']:'';?>">
                    </dd>
                  </dl>
                  <dl class="m-formList m-formList-required entryFormList-tel">
                    <dt class="m-formList-ttl">電話番号</dt>
                    <dd class="m-formList-cont">
                    <?php if(!empty($err['tel'])): ?>
                    <p class="m-formList-error"><?php echo $err['tel']; ?></p>
					<?php endif; ?>
                      <input type="text" name="tel" class="c-input t-s330" value="<?php echo (!empty($data_list['tel'])) ? $data_list['tel']:'';?>">
                    </dd>
                  </dl>
                  <dl class="m-formList m-formList-required entryFormList-job">
                    <dt class="m-formList-ttl">希望職種</dt>
                    <dd class="m-formList-cont">
                    <?php if(!empty($err['job'])): ?>
                    <p class="m-formList-error"><?php echo $err['job']; ?></p>
					<?php endif; ?>
                      <label class="c-select">
                        <select name="job">
                        <?php foreach ($job_select as $key => $value) {
                        ?>
                        <option value="<?php echo $key; ?>" <?php echo (!empty($data_list['job']) && $data_list['job'] == $key) ? 'selected':'';?>><?php echo $value; ?></option>
                        <?php
                        }?>
                        </select>
                      </label>
                    </dd>
                  </dl>
                  <dl class="m-formList m-formList-required entryFormList-jobType">
                    <dt class="m-formList-ttl">希望雇用形態</dt>
                    <dd class="m-formList-cont">
                    <?php if(!empty($err['job_type'])): ?>
                    <p class="m-formList-error"><?php echo $err['job_type']; ?></p>
					<?php endif; ?>
                      <label class="c-select">
                        <select name="job_type">
                         <?php foreach ($job_type_select as $key => $value) {
                        ?>
                        <option value="<?php echo $key; ?>" <?php echo (!empty($data_list['job_type']) && $data_list['job_type'] == $key) ? 'selected':'';?>><?php echo $value; ?></option>
                        <?php
                        }?>
                        </select>
                      </label>
                    </dd>
                  </dl>
                  <dl class="m-formList entryFormList-ask">
                    <dt class="m-formList-ttl">採用に関しての<br class="u-disp-pc">ご希望ご質問</dt>
                    <dd class="m-formList-cont">
                    <?php if(!empty($err['ask'])): ?>
                    <p class="m-formList-error"><?php echo $err['ask']; ?></p>
					<?php endif; ?>
                      <textarea name="ask" cols="30" rows="10" class="c-text"><?php echo (!empty($data_list['ask'])) ? $data_list['ask']:'';?></textarea>
                    </dd>
                  </dl>

                <!-- .entryForm // --></div>

                <div class="m-privacy">
                  <p class="m-privacyTxt">ご送信の前に、必ず「個人情報の取扱いについて」の内容をお読みのうえ、ご同意願います。</p>
                  <div class="m-privacyCont">
                    <div class="m-privacyCont-inner">

                      <div class="m-privacyList">
                        <p class="m-privacyList-ttl">（１）事業者の氏名または名称</p>
                        <p class="m-privacyList-txt">株式会社ジリオン</p>
                      </div>

                      <div class="m-privacyList">
                        <p class="m-privacyList-ttl">（２）個人情報保護管理者</p>
                        <p class="m-privacyList-txt">吉田裕司</p>
                      </div>

                      <div class="m-privacyList">
                        <p class="m-privacyList-ttl">（３）個人情報の利用目的 ・当社事業に関してお問い合わせいただいた内容に回答するため 。</p>
                      </div>

                      <div class="m-privacyList">
                        <p class="m-privacyList-ttl">（４）個人情報の第三者提供について</p>
                        <p class="m-privacyList-txt">取得した個人情報は法令等による場合を除いて第三者に提供することはありません。</p>
                      </div>

                      <div class="m-privacyList">
                        <p class="m-privacyList-ttl">（５）個人情報の取扱いの委託について</p>
                        <p class="m-privacyList-txt">取得した個人情報の取扱いの全部又は、一部を委託することがあります。</p>
                      </div>

                      <div class="m-privacyList">
                        <p class="m-privacyList-ttl">（６）開示対象個人情報の開示等および問い合わせ窓口について</p>
                        <p class="m-privacyList-txt">ご本人からの求めにより、当社が保有する開示対象個人情報の利用目的の通知・開示・内容の訂正・追加または削除・利用の停止・消去および第三者への提供の停止（「開示等」といいます。）に応じます。開示等に応ずる窓口は、お問合せいただきました当該店舗になります。</p>
                      </div>

                      <div class="m-privacyList">
                        <p class="m-privacyList-ttl">（７）個人情報の安全管理措置について</p>
                        <p class="m-privacyList-txt">取得した個人情報については、漏洩、減失またはき損の防止と是正、その他個人情報の安全管理のために必要かつ適切な措置を講じます。<br>お問合せへの回答後、取得した個人情報は当社内において削除致します。</p>
                      </div>

                      <div class="m-privacyList">
                        <p class="m-privacyList-ttl">（８）個人情報保護方針</p>
                        <p class="m-privacyList-txt">当社ホームページの個人情報保護方針をご覧下さい。</p>
                      </div>

                      <div class="m-privacyList">
                        <p class="m-privacyList-ttl">（９）個人情報を与えなかった場合に生じる結果</p>
                        <p class="m-privacyList-txt">個人情報を与えることは任意です。お問い合わせいただいた内容に回答するために必要な個人情報をご提供いただけない場合は、お問い合わせいただいた内容に回答できない場合がございますので了承願います。</p>
                      </div>

                      <div class="m-privacyList">
                        <p class="m-privacyList-ttl">（１０）当社の個人情報の取扱いに関する苦情、相談等の問合せ先</p>
                        <p class="m-privacyList-txt">
                          窓口の名称 お問い合わせ窓口<br>
                          連絡先 住所 ：〒141-0021<br>
                          東京都品川区上大崎2-26-5　メグロードビル地下1階<br>
                          電話 ：03-6417-0015</p>
                      </div>

                    <!-- .m-privacyCont-inner // --></div>
                  <!-- .m-privacyCont // --></div>
                <!-- .m-privacy // --></div>

                <div class="m-formSubmit entrySubmit js-checkSubmit">
                  <p class="m-formSubmit-agree">
                    <span class="c-check">
                      <input type="checkbox" id="Agree" class="js-checkSubmit-trigger">
                      <label for="Agree">個人情報の取扱いに同意する</label>
                    </span>
                  </p>
                  <div class="m-formSubmit-btns">
                    <p class="m-formSubmit-btn"><input type="submit" value="ENTRY" class="c-btn c-btn-form01 c-btn-entry js-checkSubmit-target" disabled></p>
                  <!-- .entrySubmit-btns // --></div>
                <!-- .entrySubmit // --></div>
				<input type="hidden" name="input" value="1">
              </form>

            <!-- .entryBody-cont // --></div>
            <!-- .entryBody-inner // --></div>
          <!-- .entryBody-inner // --></div>

        <!-- .entryCont-inner // --></div>
      <!-- .entryCont // --></div>

    <!-- .entry-inner // --></div>
  <!-- .entry // --></section>

<!-- #Content // --></div>

<?php include_once(dirname(__FILE__)."/../../shared/inc/footer.inc"); ?>

<!-- #Page // --></div>

<?php include_once(dirname(__FILE__)."/../../shared/inc/js.inc"); ?>

</body>
</html>
