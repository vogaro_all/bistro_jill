;// Global Object
var Common = Common || {};

/*--------------------------------------------------------------------------
   Polyfill　bindイベントのポリフィル処理
---------------------------------------------------------------------------*/
if (!Function.prototype.bind) {
  Function.prototype.bind = function (oThis) {
	if (typeof this !== "function") {
		// closest thing possible to the ECMAScript 5
		// internal IsCallable function
		throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
	}

	var aArgs = Array.prototype.slice.call(arguments, 1),
		fToBind = this,
		fNOP = function () {},
		fBound = function () {
			return fToBind.apply(this instanceof fNOP && oThis
				? this
				: oThis,
				aArgs.concat(Array.prototype.slice.call(arguments)));
		};

	fNOP.prototype = this.prototype;
	fBound.prototype = new fNOP();

	return fBound;
  };
}

(function($){

	var $win = $(window);
	var $html = $('html');
  var $body = $('body');
	var $htmlBody = $('html,body');
  var $doc = $(document);

  Common.setIE = (function(){
    if($.isBrowser('ie')){
      $html.addClass('ie');
    }
  }()); //setIE

	/*--------------------------------------------------------------------------
	   controller
	---------------------------------------------------------------------------*/
	Common.controller = (function () {
		var p = Controller.prototype;

    /**
		 * メディエーター
     * インスタンス間でのイベントの登録・実行処理を担う
		 *
		 * @class Controller
		 * @return {Instance} インスタンスを実行
		 */
		function Controller() {
			this._listeners = {};
		}

    /**
		 * @method on イベント登録処理
     * @param {String} type イベント名
     * @param {Object} fn 登録関数
		 */
		p.on = function (type, fn) {
			if ( !this._listeners[type] ) {
				this._listeners[type] = [];
			}

			this._listeners[type].push(fn);
		};

    /**
		 * @method trigger イベント発火処理
     * @param {type} type イベント名
     * @param args 引数
		 */
		p.trigger = function (type, args) {
			var i;
			var len = (this._listeners[type]) ? this._listeners[type].length : 0;

			for ( i = 0; i < len; i++ ) {
				this._listeners[type][i](args);
			}
		};

		return new Controller();
	}());

	/*--------------------------------------------------------------------------
		 PageView
	---------------------------------------------------------------------------*/
	Common.pageView = (function () {
		var p = PageView.prototype;
		var s = PageView;

    /**
		 * モード html要素に付与されるクラス
		 */
		s.mode = {
			pc: 'modePC',
			sp: 'modeSD'
		};

    /**
		 * サイト共通で使用されるユーティリティ関数群
		 *
		 * @class PageView
		 * @return {Instance} インスタンスを返す
		 */
		function PageView() {
			this._init();
		}

    /**
		 * @method init 初期処理
		 */
		p._init = function () {
			this._$window = $(window);
			this._$document = $(document);
			this._$html = $('html');
			this._$head = $('head');
			this._currentMode = '';

			this._props = {};
			this._props.winH = this._$window.innerHeight();

			this._handleEvents();
			this._checkMode();
		};

    /**
		 * @method handleEvents イベント登録
		 */
		p._handleEvents = function () {
			var _this = this;
			var timer;
			var interval = 100;

			// リサイズ
			this._$window.on('resize', function () {
				clearTimeout(timer);
				timer = setTimeout(function () {
					_this._props.winH = _this._$window.innerHeight();
					_this._checkMode();
					Common.controller.trigger('resize');
				}, interval);
			});
		};

    /**
		 * @method checkMode PC/SPのレスポンシブ監視及びhtml要素へのクラスの出しわけ処理
		 */
		p._checkMode = function () {
			var mode = this._$head.css('font-family').replace(/"/gi, '');

			if ( this._currentMode !== mode ) {
				Common.isPC = (mode === 'pc') ? true : false;
				Common.isSP = (mode === 'sp') ? true : false;

        if(this._currentMode){
  				this._$html.removeClass(s.mode[this._currentMode]);
        }
				this._$html.addClass(s.mode[mode]);

				Common.controller.trigger('modeChange', mode);

				this._currentMode = mode;
			}
		};

		return PageView;
	}());

  /*--------------------------------------------------------------------------
		 scroll
	---------------------------------------------------------------------------*/
  Common.scroll = (function(){
    var p = Scroll.prototype;

    function Scroll($trigger){
      this._$trigger = $trigger;
      this._$target = $(this._$trigger.attr('href'));
      this._diff = (this._$target.attr('id') == 'page') ? 0 : -50;
      this._offset = 0;

      this._init();
    }

    p._init = function(){
      this._onEvent();
    };

    p._setPos = function(){
      this._offset = this._$target.offset().top;
    };

    p._onEvent = function(){
      var _this = this;
      this._$trigger.on('click', function(){
        _this._move();
        return false;
      });
    };

    p._move = function(){
      var duration = 1000,
        easing = 'easeOutQuint';
      this._setPos();

      $htmlBody.animate({
        scrollTop: this._offset + this._diff
      }, {
        duration: duration,
        easing: easing
      });
    };

    return Scroll;
  }());

  /*--------------------------------------------------------------------------
		 scrollTop
	---------------------------------------------------------------------------*/
  Common.scrollTop = (function(){
    var p = ScrollTop.prototype;

    function ScrollTop(){
      this._$elem = $('#ScrollTop');
      this._triggerVal = 0;
      this._isShow = false;
      this._isAnimated = false;
      this._$footer = $('#Footer');
      this._fixPoint = 0;
      this._fixDiff = 170;

      this._init();
    }

    p._init = function(){
      this._initSet();
      Common.controller.on('resize', this._onResize.bind(this));
      Common.controller.on('modeChange', this._onModeChange.bind(this));
    };

    p._initSet = function(){
      if($html.hasClass('modePC')){
        this._setPos();
        this._setMax();
        this._onEvent();
      }
    };

    p._setPos = function(){
      var winH = parseInt($win.height(), 10);
      this._triggerVal = winH;
    };

    p._onEvent = function(){
      $doc.on('scroll.scrollTop', this._onScroll.bind(this));
    };

    p._offEvent = function(){
      $doc.off('scroll.scrollTop');
    };

    p._onResize = function(){
      if($html.hasClass('modePC')){
        this._setPos();
      }
    };

    p._onModeChange = function(){
      if($html.hasClass('modePC')){
        this._setPos();
        this._setMax();
        this._offEvent();
        this._onEvent();
      } else {
        this._offEvent();
      }
      this._reset();
    };

    p._onScroll = function(){
      var scrollVal = $doc.scrollTop();

      if(scrollVal > this._triggerVal){
        this._show();
      } else {
        this._hide();
      }
      if(scrollVal > this._fixPoint){
        this._$elem.addClass('is-reached');
      } else {
        this._$elem.removeClass('is-reached');
      }
    };

    p._setMax = function(){
      this._fixPoint = $doc.height() - $win.height() - this._fixDiff + this._$elem.height();
    };

    p._show = function(){
      var _this = this;
      var duration = 300;
      if(!this._isShow && !this._isAnimated){
        this._isAnimated = true;
        this._$elem.velocity({ opacity: 1 }, {
          duration: duration,
          display: 'block',
          complete: function(){
            _this._isShow = true;
            _this._isAnimated = false;
          }
        });
      }
    };

    p._hide = function(){
      var _this = this;
      var duration = 300;
      if(this._isShow && !this._isAnimated){
        this._isAnimated = true;
        this._$elem.velocity({ opacity: 0 }, {
          duration: duration,
          display: 'none',
          complete: function(){
            _this._isShow = false;
            _this._isAnimated = false;
          }
        });
      }
    };

    p._reset = function(){
      if($html.hasClass('modePC')){
        this._$elem.attr('style', '');
        this._isShow = false;
        this._isAnimated = false;
      } else {
        this._$elem.attr('style', '');
        this._isShow = true;
        this._isAnimated = false;
      }
    };

    return ScrollTop;
  }());

  /*--------------------------------------------------------------------------
     checkSubmit
  ---------------------------------------------------------------------------*/
  Common.checkSubmit = (function(){
    var p = CheckSubmit.prototype;

    function CheckSubmit($elem){
      this._$elem = $elem;
      this._$trigger = this._$elem.find('.js-checkSubmit-trigger');
      this._$target = this._$elem.find('.js-checkSubmit-target');

      this._init();
    }

    p._init = function(){
      this._onChange();
      this._onEvent();
    };

    p._onEvent = function(){
      var _this = this;
      this._$trigger.on('change', function(){
        _this._onChange();
        return false;
      });
    };

    p._onChange = function(){
      if(this._$trigger.is(':checked')){
        this._enable();
      } else {
        this._disable();
      }
    };

    p._enable = function(){
      this._$target.attr('disabled', false);
    };

    p._disable = function(){
      this._$target.attr('disabled', true);
    };

    return CheckSubmit;
  }());

/*----------------------------------------------------------------------
	DOM READY
----------------------------------------------------------------------*/
jQuery(function($){
	//タブレットSP表示
  if($.isDevice('tablet')) {
    $("meta[name='viewport']").attr('content','width=768');
  }

	new Common.pageView();

  new Common.scrollTop();

  $('.js-scroll').each(function(){
    new Common.scroll($(this));
  });

  $('.js-checkSubmit').each(function(){
    new Common.checkSubmit($(this));
  });
});

}(jQuery));
