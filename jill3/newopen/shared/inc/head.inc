<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no,address=no,email=no">

<link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/favicon.ico">

<link rel="stylesheet" href="/newopen/shared/css/global.css">
<link rel="stylesheet" href="/newopen/shared/css/common.css">
