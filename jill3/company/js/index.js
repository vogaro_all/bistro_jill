;// Global Object
var Page = Page || {};

(function($){

	var $win = $(window);
	var $html = $('html');
	var $body = $('body');
	var $htmlBody = $('html,body');
	var $doc = $(document);

	var manifestPC = [
		{ src: 'images/bg_concept.png' },
		{ src: 'images/bg_concept_objects.png' },
		{ src: 'images/bg_project01.png' },
		{ src: 'images/bg_vision_object.png' },
		{ src: 'images/bg_vision_tip_cs.png' },
		{ src: 'images/bg_vision_tip_es.png' },
		{ src: 'images/bg_vision_tip_fs.png' },
		{ src: 'images/img_concept.jpg' },
		{ src: 'images/img_kv.jpg' },
		{ src: 'images/img_mission01.png' },
		{ src: 'images/img_mission02.png' },
		{ src: 'images/img_mission03.png' },
		{ src: 'images/img_pin.png' },
		{ src: 'images/img_project01.png' },
		{ src: 'images/img_vision.jpg' },
		{ src: 'images/ttl_company.png' },
		{ src: 'images/ttl_concept.png' },
		{ src: 'images/ttl_mission.png' },
		{ src: 'images/ttl_page.png' },
		{ src: 'images/ttl_project.png' },
		{ src: 'images/ttl_vision.png' },
		{ src: 'images/ico_vision_cs.png' },
		{ src: 'images/ico_vision_es.png' },
		{ src: 'images/ico_vision_fs.png' }
	];

	var manifestSP = [
		{ src: 'images/img_kv_sp.jpg' },
		{ src: 'images/ttl_page_sp.png' },
		{ src: 'images/ttl_concept_sp.png' },
		{ src: 'images/img_concept_sp.jpg' },
		{ src: 'images/ttl_mission_sp.png' },
		{ src: 'images/img_mission01_sp.png' },
		{ src: 'images/img_mission02_sp.png' },
		{ src: 'images/img_mission03_sp.png' },
		{ src: 'images/ttl_vision_sp.png' },
		{ src: 'images/img_vision_sp.jpg' },
		{ src: 'images/bg_vision_object.png' },
		{ src: 'images/ico_vision_cs_sp.png' },
		{ src: 'images/ico_vision_es_sp.png' },
		{ src: 'images/ico_vision_fs_sp.png' },
		{ src: 'images/ttl_project_sp.png' },
		{ src: 'images/img_project01_sp.png' },
		{ src: 'images/bg_project01_sp.png' },
		{ src: 'images/ttl_company_sp.png' }
	];

	/*--------------------------------------------------------------------------
		 companyMap
	---------------------------------------------------------------------------*/
	Page.companyMap = (function(){
		var p = CompanyMap.prototype;

		/**
		 * カスタマイズgoogle map
		 */
		function CompanyMap(){

			this._init();
		}

		/**
		 * 初期化
		 */
		p._init = function(){
			// マップ基本設定
			var latlng = new google.maps.LatLng(35.634208, 139.714712);
			var myOptions = {
				zoom: 18,
				center: latlng,
				disableDefaultUI: true
			};
			var map = new google.maps.Map($('#Map')[0], myOptions);

			// マーカー
			var markerImg = {
				url: 'images/img_pin.png',
				scaledSize: new google.maps.Size(49, 41)
			};
			var marker = new google.maps.Marker({
				position: latlng,
				map: map,
				icon: markerImg
			});

			// デザイン変更
			var mapStyle = [
				{
					'stylers': [
						{ 'saturation': -95 }
					]
				}
			];
			var mapType = new google.maps.StyledMapType(mapStyle);
			map.mapTypes.set('GrayScaleMap', mapType);
			map.setMapTypeId('GrayScaleMap');

			google.maps.event.addDomListener(window, 'resize', function(){
				map.panTo(latlng);
			});
		};

		return CompanyMap;
	}());

	/*--------------------------------------------------------------------------
		 ScrollAnimation
	---------------------------------------------------------------------------*/
	Page.scrollAnimation = (function(){
		var p = ScrollAnimation.prototype;

		/**
		 * スクロールアニメーション
		 * 初期ロード時PC表示で且つ、スクロール量が0の場合のみ発動
		 * ※他ページから遷移してきたユーザーのみアニメーションターゲットとして考える。
		 * PC→SPにウィンドウリサイズした場合、イベントキャンセル。その後アニメーションしない
		 * @param {jQuery} $elem アニメーション対象要素
		 */
		function ScrollAnimation($elem){
			this._$elem = $elem;
			this._posY = 0;
			this._scrVal = 0;
			this._winHalfY = 0;
			this._diffY = 100;
			this._tweenInitClass = 'is-tweenInit';
			this._tweenClass = 'is-tween';

			// スクロール量が0の場合のみ発動
			if($doc.scrollTop() === 0){
				this._init();
			}

		}

		/**
		 * 初期化
		 */
		p._init = function(){
			// 初期スタイル設定
			this._initSet();
			this._onResize();
			$win.resize(this._onResize.bind(this));
			$win.on('scroll.scrollAnimation', this._onScroll.bind(this));
			Common.controller.on('modeChange', this._onModeChange.bind(this));
		};

		/**
		 * 初期設定
		 */
		p._initSet = function(){
			this._$elem.addClass(this._tweenInitClass);
		};

		/**
		 * 要素のオフセットトップ値を取得
		 */
		p._setPos = function(){
			this._posY = this._$elem.offset().top;
		};

		/**
		 * リサイズ時の処理
		 */
		p._onResize = function(){
			this._setPos();
			this._winH = window.innerHeight;
		};

		/**
		 * スクロール時の処理
		 */
		p._onScroll = function(){
			this._scrVal = $doc.scrollTop();
			this._add();
		};

		/**
		 * レスポンシブ時の処理
		 */
		p._onModeChange = function(){
			this._reset();
		};

		/**
		 * is-tweenクラスの付与
		 */
		p._add = function(){
			if(!this._$elem.hasClass(this._tweenClass)){
				if(this._posY < this._scrVal + this._winH / 2){
					this._$elem.addClass(this._tweenClass);
				}
			}
		};

		/**
		 * スタイルリセット処理
		 */
		p._reset = function(){
			this._$elem.removeClass(this._tweenInitClass);
			this._$elem.removeClass(this._tweenClass);
			$win.off('scroll.scrollAnimation');
		};

		return ScrollAnimation;
	}());

/*----------------------------------------------------------------------
	DOM READY
----------------------------------------------------------------------*/
	jQuery(function($){

		// PC
		if($html.hasClass('modePC')){
			if(!$html.hasClass('lt-ie9')){
				new Common.loading(manifestPC);
				$('.js-scrollanimation').each(function(){
					new Page.scrollAnimation($(this));
				});
			}
		} else {
			// SP
			new Common.loading(manifestSP);
		}

		new Page.companyMap();
	});

}(jQuery));
