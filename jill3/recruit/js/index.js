;// Global Object
var Page = Page || {};

(function($){

	var $win = $(window);
	var $html = $('html');
	var $body = $('body');
	var $htmlBody = $('html,body');
	var $doc = $(document);

	var manifestPC = [
		{ src: 'images/bg_people.jpg' },
		{ src: 'images/btn_scrolldown.png' },
		{ src: 'images/img_kv01.jpg' },
		{ src: 'images/img_kv02.jpg' },
		{ src: 'images/img_kv03.jpg' },
		{ src: 'images/img_talk01.jpg' },
		{ src: 'images/img_talk02.jpg' },
		{ src: 'images/logo_recruit.png' },
		{ src: 'images/txt_talk_num01.png' },
		{ src: 'images/txt_talk_num02.png' },
		{ src: 'images/bg_border.png' },
		{ src: 'images/bg_challenge.png' },
		{ src: 'images/bg_message.png' },
		{ src: 'images/bg_message02.png' }
	];

	var manifestSP = [
		{ src: 'images/bg_people_sp.jpg' },
		{ src: 'images/img_kv01_sp.jpg' },
		{ src: 'images/img_kv02_sp.jpg' },
		{ src: 'images/img_kv03_sp.jpg' },
		{ src: 'images/img_talk01.jpg' },
		{ src: 'images/img_talk02.jpg' },
		{ src: 'images/logo_recruit_sp.png' },
		{ src: 'images/txt_talk_num01_sp.png' },
		{ src: 'images/txt_talk_num02_sp.png' },
		{ src: 'images/bg_border.png' },
		{ src: 'images/bg_challenge_sp.png' },
		{ src: 'images/bg_message_sp.png' },
		{ src: 'images/bg_message02_sp.png' }
	];

	/*----------------------------------------------------------------------
		Fader
	----------------------------------------------------------------------*/
	Page.fader = (function(){
		var p = Fader.prototype;

		/**
		 * メインビジュアルのフェーダー
		 * @param {jQuery} $elem
		 */
		function Fader($elem){
			this._$fader = $elem;
			this._$list   = $elem.find('.mv-list');
			this._prevIdx = 0;
			this._currentIdx = 0;
			this._len = this._$list.length;
			this._isAnimated = false;
			this._min = 0;
			this._max = 0;
			this._activeClass = 'is-active';
			this._timer = 0;

			this._init();
		}

		/**
		 * 初期設定
		 */
		p._init = function(){
			this._setMin();
			this._setMax();
			this._autoRun();
		};

		/**
		 * スライダー数の最小値設定
		 */
		p._setMin = function(){
			this._min = 0;
		};

		/**
		 * スライダー数の最大値設定
		 */
		p._setMax = function(){
			this._max = this._$list.length;
		};

		/**
		 * 更新処理
		 */
		p._update = function(){
			if(this._currentIdx === this._max){
				this._currentIdx = this._min;
			}
			if(this._currentIdx < this._min){
				this._currentIdx = this._max - 1;
			}
		};

		/**
		 * 自動ループ
		 */
		p._autoRun = function(){
			var _this = this;
			var duration = 3000;
			clearTimeout(this._timer);
			this._timer = setTimeout(function(){
				_this._isAnimated = true;
				_this._prevIdx = _this._currentIdx;
				_this._currentIdx++;
				_this._tween();
			}, duration);
		};

		/**
		 * アニメーション設定
		 */
		p._tween = function(){
			var _this = this;
			var duration = 2000;

			clearTimeout(this._timer);
			this._update();
			this._$list.eq(this._prevIdx).animate({
				opacity: 0
			}, {
				duration: duration,
				complete: function(){
					_this._$list.eq(_this._prevIdx).css({'visibility': 'hidden'});
				}
			});
			this._$list.eq(this._currentIdx).animate({
				opacity: 1
			}, {
				duration: duration,
				start: function(){
					_this._$list.eq(_this._currentIdx).css({'visibility': 'visible'});
				},
				complete: function(){
					_this._isAnimated = false;
					_this._autoRun();
				}
			});
		};

		return Fader;
	}());

	/*--------------------------------------------------------------------------
		 fullScreen
	---------------------------------------------------------------------------*/
	Page.fullScreen = (function(){
		var p = FullScreen.prototype;

		/**
		 * フルスクリーン処理
		 * Common.FullScreenはPC/SPでのフルスクリーン処理
		 * Page.fullScreenはPC時のみフルスクリーン処理
		 * @param {jQuery} $elem
		 */
		function FullScreen($elem){
			this._$elem = $elem;
			this._minHeightPC = 650;
			this._minHeightSP = 320;
			this._winW = 0;

			this._init();
		}

		/**
		 * 初期設定
		 */
		p._init = function(){
			this._onResize();
			Common.controller.on('resize', this._onResize.bind(this));
		};

		/**
		 * 高さ設定
		 */
		p._setHeight = function(){
			var _this = this;

			setTimeout(function(){
				var winH = window.innerHeight ? window.innerHeight : $(window).height();
				var minHeight = ($html.hasClass('modePC')) ? _this._minHeightPC : _this._minHeightSP;
				var height = (minHeight < winH) ? winH : minHeight;

				_this._winW = $win.width();
				_this._$elem.css({ height: height });
			},300);
		};

		/**
		 * リサイズ時の処理
		 */
		p._onResize = function(){
			var winW = parseInt($win.width(), 10);

			if($html.hasClass('modePC')){
				if(this._winW !== winW){
					this._setHeight();
				}
			} else {
				this._reset();
			}
		};

		/**
		 * 初期化
		 */
		p._reset = function(){
			this._$elem.attr('style', '');
			this._winW = 0;
		};

		return FullScreen;
	}());

	/*----------------------------------------------------------------------
		slider
	----------------------------------------------------------------------*/
	Page.slider = (function(){
		var p = Slider.prototype;

		/**
		 * スライダー
		 */
		function Slider(){
			this._$wrap = $('#People');
			this._$lists = this._$wrap.find('.people-lists');
			this._moveVal = 0;
			this._speed = 0.5;

			Common.controller.on('load', this._init.bind(this));
		}

		/**
		 * 初期化
		 */
		p._init = function(){
			this._onResize();
			this._tween();
			Common.controller.on('resize', this._onResize.bind(this));
		};

		/**
		 * リサイズ時の処理
		 */
		p._onResize = function(){
			if($html.hasClass('modePC')){
				this._reset();
			} else {
				this._setWidth();
			}
		};

		/**
		 * 各スライドｌの幅設定
		 */
		p._setWidth = function(){
			var $list = this._$lists.find('.people-list'),
				width = $win.width(),
				len = $list.length,
				totalW = width * len;

			$list.css({ width: width });

			this._$lists.css({ width: totalW });
		};

		/**
		 * 初期化
		 */
		p._reset = function(){
			this._$lists.attr('style', '');
			this._$lists.find('.people-list').attr('style', '');
		};

		/**
		 * アニメーション設定
		 */
		p._tween = function(){
			var $first = this._$lists.find('.people-list:first');

			this._moveVal += this._speed;

			if(Math.abs($first.offset().left) >= $first.width()){
				this._moveVal = 0;
				this._$lists.append($first);
			}

			if($html.hasClass('csstransforms3d')){
				this._$lists.css({ 'transform': 'translate3d(' + -this._moveVal  + 'px,0,0)'});
			} else {
				this._$lists.css({ marginLeft: -this._moveVal });
			}

			requestAnimationFrame(this._tween.bind(this));

		};

		return Slider;
	}());
	
	/*----------------------------------------------------------------------
		tracker
	----------------------------------------------------------------------*/
	Page.tracker = (function(){
		var p = Tracker.prototype;
		
		function Tracker($elem){
			this._$elem = $elem;
			this._$footer = $('#Footer');
			
			this._scroll = null;
			this._point = null;
			this._hidden = 'is-hidden';
			
			this._init();
		}
		
		p._init = function(){
			this._onResize();
			this._onScroll();
			this._handleEvents();
		};
		
		p._handleEvents = function(){
			Common.controller.on('resize', this._onResize.bind(this));
			$win.on('scroll', this._onScroll.bind(this));
		};
		
		p._onResize = function(){
			this._point = this._$footer.offset().top;
		};
		
		p._onScroll = function(){
			this._point = this._$footer.offset().top;
			this._scroll = $win.scrollTop() + $win.height();
				
			if(this._scroll > this._point){
				this._$elem.addClass(this._hidden);
			}else{
				this._$elem.removeClass(this._hidden);
			}
		};
		
		return Tracker;
	}());

/*----------------------------------------------------------------------
	DOM READY
----------------------------------------------------------------------*/
	jQuery(function($){
		
		$('.talk-slider').slick({
			infinite: true,
			autoplay: true,
			autoplaySpeed: 5000,
			speed: 600,
			dots: true,
			dotsClass: 'talk-slider-btns',
			prevArrow: '<div class="talk-slider-nav talk-slider-nav_prev"></div>',
			nextArrow: '<div class="talk-slider-nav talk-slider-nav_next"></div>',
			slidesToShow: 3,
			slidesToScroll: 3,
			responsive: [
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		});

		new Page.fullScreen($('#MV'));
		new Page.fader($('#Fader'));
		new Page.slider();
		new Page.tracker($('.js-tracker'));

		$('.js-accordion').each(function(){
			new Common.accordion($(this));
		});

		$('.js-parallax-target').each(function(){
			new Common.parallax($(this));
		});

		// PC
		if($html.hasClass('modePC')){
			if(!$html.hasClass('lt-ie9')){
				new Common.loading(manifestPC);
			}
		} else {
			// SP
			new Common.loading(manifestSP);
		}

	});

}(jQuery));
