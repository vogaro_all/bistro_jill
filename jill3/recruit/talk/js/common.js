;// Global Object
var PageCommon = PageCommon || {};

(function($){

	var $win = $(window);
	var $html = $('html');
	var $body = $('body');
	var $htmlBody = $('html,body');
	var $doc = $(document);

	/*--------------------------------------------------------------------------
		 ScrollAnimation
	---------------------------------------------------------------------------*/
	PageCommon.scrollAnimation = (function(){
		var p = ScrollAnimation.prototype;

		/**
		 * スクロールアニメーション
		 * 初期ロード時PC表示で且つ、スクロール量が0の場合のみ発動
		 * ※他ページから遷移してきたユーザーのみアニメーションターゲットとして考える。
		 * PC→SPにウィンドウリサイズした場合、イベントキャンセル。その後アニメーションしない
		 *
		 * @param {jQuery} $elem
		 */
		function ScrollAnimation($elem){
			this._$elem = $elem;
			this._posY = 0;
			this._scrVal = 0;
			this._winHalfY = 0;
			this._diffY = 100;
			this._tweenInitClass = 'is-tweenInit';
			this._tweenClass = 'is-tween';

			// スクロール量が0の場合のみ発動
			if($doc.scrollTop() === 0){
				this._init();
			}

		}

		/**
		 * @method _init 初期化
		 */
		p._init = function(){
			// 初期スタイル設定
			this._initSet();
			this._onResize();
			$win.resize(this._onResize.bind(this));
			$win.on('scroll.scrollAnimation', this._onScroll.bind(this));
			Common.controller.on('modeChange', this._onModeChange.bind(this));
		};

		/**
		 * @method _initSet 初期時のスタイル設定
		 */
		p._initSet = function(){
			this._$elem.addClass(this._tweenInitClass);
		};

		/**
		 * @method _setPos 要素のオフセットトップ値を取得
		 */
		p._setPos = function(){
			this._posY = this._$elem.offset().top;
		};

		/**
		 * @method _onResize リサイズ時の処理
		 */
		p._onResize = function(){
			this._setPos();
			this._winH = window.innerHeight;
		};

		/**
		 * @method _onScroll スクロール時の処理
		 */
		p._onScroll = function(){
			this._scrVal = $doc.scrollTop();
			this._add();
		};

		/**
		 * @method _onModeChange レスポンシブ時の処理
		 */
		p._onModeChange = function(){
			this._reset();
		};

		/**
		 * @method _add is-tweenクラスの付与
		 */
		p._add = function(){
			if(!this._$elem.hasClass(this._tweenClass)){
				if(this._posY < this._scrVal + this._winH){
					this._$elem.addClass(this._tweenClass);
				}
			}
		};

		/**
		 * @method _reset スタイルリセット処理
		 */
		p._reset = function(){
			this._$elem.removeClass(this._tweenInitClass);
			this._$elem.removeClass(this._tweenClass);
			$win.off('scroll.scrollAnimation');
		};

		return ScrollAnimation;
	}());

/*----------------------------------------------------------------------
	DOM READY
----------------------------------------------------------------------*/
	jQuery(function($){

	});

}(jQuery));
