;// Global Object
var Page = Page || {};

(function($){

	var $win = $(window);
	var $html = $('html');
	var $body = $('body');
	var $htmlBody = $('html,body');
	var $doc = $(document);

	var manifestPC = [
		{ src: 'images/talk05/img_kv.jpg' },
		{ src: 'images/talk05/ttl_page.png' },
		{ src: 'images/talk05/ttl_profile01.png' },
		{ src: 'images/talk05/ttl_profile02.png' },
		{ src: 'images/common/ico_cross.png' },
		{ src: 'images/talk05/img_thumb_niimi.png' },
		{ src: 'images/talk05/img_thumb_yoshida.png' },
		{ src: 'images/talk05/img_talk01.jpg' },
		{ src: 'images/talk05/img_talk02.jpg' },
		{ src: 'images/talk05/img_talk03.jpg' },
		{ src: 'images/talk05/img_talk04.jpg' },
		{ src: 'images/talk05/img_talk05.jpg' },
		{ src: 'images/talk05/img_talk06.jpg' },
		{ src: 'images/talk05/img_talk07.jpg' },
		{ src: 'images/talk05/img_talk08.jpg' },
		{ src: 'images/talk05/img_talk09.jpg' },
		{ src: 'images/talk05/ttl_pager_num.png' },
		{ src: 'images/talk05/img_pager.jpg' }
	];

	var manifestSP = [
		{ src: 'images/talk05/img_kv_sp.jpg' },
		{ src: 'images/talk05/ttl_page_sp.png' },
		{ src: 'images/talk05/ttl_profile01_sp.png' },
		{ src: 'images/talk05/ttl_profile02_sp.png' },
		{ src: 'images/common/ico_cross_sp.png' },
		{ src: 'images/talk05/img_thumb_niimi_sp.png' },
		{ src: 'images/talk05/img_thumb_yoshida_sp.png' },
		{ src: 'images/talk05/img_talk01.jpg' },
		{ src: 'images/talk05/img_talk02.jpg' },
		{ src: 'images/talk05/img_talk03.jpg' },
		{ src: 'images/talk05/img_talk04.jpg' },
		{ src: 'images/talk05/img_talk05.jpg' },
		{ src: 'images/talk05/img_talk06.jpg' },
		{ src: 'images/talk05/img_talk07.jpg' },
		{ src: 'images/talk05/img_talk08.jpg' },
		{ src: 'images/talk05/img_talk09.jpg' },
		{ src: 'images/talk05/ttl_pager_num_sp.png' },
		{ src: 'images/talk05/img_pager_sp.jpg' }
	];

/*----------------------------------------------------------------------
	DOM READY
----------------------------------------------------------------------*/
	jQuery(function($){

		// PC
		if($html.hasClass('modePC')){
			if(!$html.hasClass('lt-ie9')){
				new Common.loading(manifestPC);
				$('.js-scrollanimation').each(function(){
					new PageCommon.scrollAnimation($(this));
				});
			}
		} else {
			// SP
			new Common.loading(manifestSP);
		}

	});

}(jQuery));
