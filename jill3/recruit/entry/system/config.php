<?php
ini_set('error_reporting', E_ALL | ~E_STRICT);

require_once(dirname(__FILE__).'/lib/common.php');
require_once(dirname(__FILE__).'/lib/validation.php');
require_once(dirname(__FILE__).'/lib/sendmail.php');

//入力画面テンプレート
define("FORM_INPUT", dirname(__FILE__)."/tmpl/index.php");

//確認画面テンプレート
define("FORM_CONFIRM", dirname(__FILE__)."/tmpl/confirm.php");

//管理者メールアドレス
define("ADMIN_MAIL_ADDRESS", "contact@jillion.co.jp");
//define("ADMIN_MAIL_ADDRESS", "okada@invogue.co.jp");
// define("ADMIN_MAIL_ADDRESS", "okada@invogue.co.jp,shibata@vogaro.co.jp");
//define("ADMIN_MAIL_ADDRESS", "yoshida@jillion.co.jp");

// 性別
$gender_select = array(
		1 => '男性',
		2 => '女性',
);

// 希望職種プルダウン
$job_select = array(
		'' => '--',
		1 => 'ホールスタッフ',
		2 => 'キッチンスタッフ',
);

// 雇用形態プルダウン
$job_type_select = array(
		'' => '--',
		1 => 'アルバイト',
		2 => '正社員',
);

// 希望勤務地プルダウン
$shop_type_select = array(
		'' => '--',
		1 => '目黒',
		2 => '中目黒',
		3 => '五反田',
		4 => '学芸大学',
		// 5 => '神田',
		5 => '自由が丘',
		6 => '東京',
		7 => '恵比寿',
);

//バリデーションクラスインスタンス生成
$VALIDATION = new validation();

//メールクラスインスタンス生成
$MAIL = new sendmail();