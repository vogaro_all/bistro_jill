<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie8 lt-ie9" lang="ja">
<![endif]-->
<!--[if IE 9]>
<html class="ie9 lt-ie9" lang="ja">
<![endif]-->
<!--[if !IE]><!-->
<html lang="ja">
<!--<![endif]-->
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>エントリー｜リクルート｜株式会社ジリオン</title>
<meta name="description" content="採用エントリーフォームです。目黒,中目黒,五反田,学芸大学,神田で、大衆ビストロジル/大衆ビストロ煮ジル/JBの3業態を展開しています。">
<meta name="keywords" content="株式会社ジリオン,大衆ビストロジル,煮ジル,JB,肉料理,居酒屋,バル,ワイン,目黒,中目黒,五反田,学芸大学,神田">

<? include_once(dirname(__FILE__).'/../../../../shared/inc/head.inc'); ?>
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/index.css">

<? include_once(dirname(__FILE__).'/../../../../shared/inc/ie.inc'); ?>

</head>

<body>
<? include_once(dirname(__FILE__).'/../../../../shared/inc/ga.inc'); ?>

<? include_once(dirname(__FILE__).'/../../../../shared/inc/loading.inc'); ?>

<div id="Page">

<? include_once(dirname(__FILE__).'/../../../../shared/inc/header.php'); ?>

<div id="Content">

	<header class="m-pageHeader">
		<div class="m-pageHeader-inner">
			<h1 class="m-pageHeader-ttl">
				<img src="images/ttl_page.png" alt="ENTRY" width="194" height="44" class="u-disp-pcImg">
				<img src="images/ttl_page_sp.png" alt="" width="123" height="27" class="u-disp-spImg">
				<span class="m-pageHeader-txt-contact">採用面談希望</span>
			<!-- .kvTtl // --></h1>
		<!-- .kv-inner // --></div>
	<!-- .kv // --></header>

	<div class="m-container">
		<div class="m-container-inner l-container">

			<div class="m-frame c-shadow">
				<div class="m-frame-inner">

					<div class="m-frame-cornerTop">
						<div class="m-frame-cornerBottom">
							<div class="m-frame-body">

								<div class="m-flow">
									<p class="m-flow-fig">
										<img src="/shared/images/img_flow01.png" alt="入力" width="345" height="84" class="u-disp-pcImg">
										<img src="/shared/images/img_flow01_sp.png" alt="" width="228" height="57" class="u-disp-spImg">
									</p>
									<p class="m-flow-txt">エントリー誠にありがとうございます。採用担当より、別途日程の調整についてご連絡差し上げます。<br>予め、「ご質問欄」に連絡が付きやすいお時間、手段をご記載頂くとスムーズです。</p>
								<!-- .m-flow // --></div>

								<form action="./index.php" method="post">

									<div class="m-form recruitForm">

										<dl class="m-formList m-formList-required recruitFormList-name">
											<dt class="m-formList-ttl">お名前</dt>
											<dd class="m-formList-cont">
											<?php if(!empty($err['name'])): ?>
										<p class="m-formList-error"><?php echo $err['name']; ?></p>
					<?php endif; ?>
												<input type="text" name="name" class="c-input" value="<?php echo (!empty($data_list['name'])) ? $data_list['name']:'';?>">
											</dd>
										</dl>

										<dl class="m-formList m-formList-required recruitFormList-gender">
											<dt class="m-formList-ttl">性別</dt>
											<dd class="m-formList-cont">
										<?php if(!empty($err['gender'])): ?>
										<p class="m-formList-error"><?php echo $err['gender']; ?></p>
					<?php endif; ?>
												<span class="c-radio">
													<input type="radio" name="gender" value="1" id="GenderM" <?php echo (!empty($data_list['gender']) && $data_list['gender'] == 1) ? 'checked':'';?>>
													<label for="GenderM">男性</label>
												</span>
												<span class="c-radio">
													<input type="radio" name="gender" value="2" id="GenderL" <?php echo (!empty($data_list['gender']) && $data_list['gender'] == 2) ? 'checked':'';?>>
													<label for="GenderL">女性</label>
												</span>
											</dd>
										</dl>

										<dl class="m-formList m-formList-required recruitFormList-age">
											<dt class="m-formList-ttl">年齢</dt>
											<dd class="m-formList-cont">
										<?php if(!empty($err['age'])): ?>
										<p class="m-formList-error"><?php echo $err['age']; ?></p>
					<?php endif; ?>
												<input type="text" name="age" class="c-input" value="<?php echo (!empty($data_list['age'])) ? $data_list['age']:'';?>"><span>歳</span>
											</dd>
										</dl>

										<dl class="m-formList recruitFormList-address">
											<dt class="m-formList-ttl">住所</dt>
											<dd class="m-formList-cont">
												<div class="recruitFormList-row">
													<span class="mark">〒</span><input type="text" name="zip1" class="c-input c-input-zip js-autoZip-zip01" value="<?php echo (!empty($data_list['zip1'])) ? $data_list['zip1']:'';?>"><span class="hyphen">ー</span><input type="text" name="zip2" class="c-input c-input-zip js-autoZip-zip02" value="<?php echo (!empty($data_list['zip2'])) ? $data_list['zip2']:'';?>">
													<p class="zipBtn"><a href="#" class="js-autoZip-btn">郵便番号から住所入力</a></p>
												<!-- .recruitFormList-row // --></div>
												<div class="recruitFormList-row">
													<input type="text" name="address" class="c-input js-autoZip-address" value="<?php echo (!empty($data_list['address'])) ? $data_list['address']:'';?>">
												<!-- .recruitFormList-row // --></div>
											</dd>
										</dl>
										
										<dl class="m-formList m-formList-required recruitFormList-email">
											<dt class="m-formList-ttl">メールアドレス</dt>
											<dd class="m-formList-cont">
										<?php if(!empty($err['email'])): ?>
										<p class="m-formList-error"><?php echo $err['email']; ?></p>
					<?php endif; ?>
												<input type="text" name="email" class="c-input" value="<?php echo (!empty($data_list['email'])) ? $data_list['email']:'';?>">
											</dd>
										</dl>
										<dl class="m-formList m-formList-required recruitFormList-emailConfirm">
											<dt class="m-formList-ttl">メールアドレス（確認）</dt>
											<dd class="m-formList-cont">
										<?php if(!empty($err['email_confirm'])): ?>
										<p class="m-formList-error"><?php echo $err['email_confirm']; ?></p>
					<?php endif; ?>
												<input type="text" name="email_confirm" class="c-input" value="<?php echo (!empty($data_list['email_confirm'])) ? $data_list['email_confirm']:'';?>">
											</dd>
										</dl>

										<dl class="m-formList m-formList-required recruitFormList-tel">
											<dt class="m-formList-ttl">電話番号</dt>
											<dd class="m-formList-cont">
										<?php if(!empty($err['tel'])): ?>
										<p class="m-formList-error"><?php echo $err['tel']; ?></p>
					<?php endif; ?>
												<input type="text" name="tel" class="c-input" value="<?php echo (!empty($data_list['tel'])) ? $data_list['tel']:'';?>">
											</dd>
										</dl>

										<dl class="m-formList m-formList-required recruitFormList-jobType">
											<dt class="m-formList-ttl">希望雇用形態</dt>
											<dd class="m-formList-cont">
										<?php if(!empty($err['job_type'])): ?>
										<p class="m-formList-error"><?php echo $err['job_type']; ?></p>
					<?php endif; ?>
												<label class="c-select">
													<select name="job_type">
												 <?php foreach ($job_type_select as $key => $value) {
												?>
												<option value="<?php echo $key; ?>" <?php echo (!empty($data_list['job_type']) && $data_list['job_type'] == $key) ? 'selected':'';?>><?php echo $value; ?></option>
												<?php
												}?>
													</select>
												</label>
											</dd>
										</dl>
										
										<dl class="m-formList recruitFormList-shop">
											<dt class="m-formList-ttl">希望勤務地</dt>
											<dd class="m-formList-cont">
										<?php if(!empty($err['shop'])): ?>
										<p class="m-formList-error"><?php echo $err['shop']; ?></p>
					<?php endif; ?>
												<label class="c-select">
													<select name="shop">
													
													<?php foreach ($shop_type_select as $key => $value) {
													?>
													<option value="<?php echo $key; ?>" <?php echo (!empty($data_list['shop']) && $data_list['shop'] == $key) ? 'selected':'';?>><?php echo $value; ?></option>
													<?php
													}?>
													
													</select>
												</label>
											</dd>
										</dl>
										
										<dl class="m-formList recruitFormList-job">
											<dt class="m-formList-ttl">希望職種</dt>
											<dd class="m-formList-cont">
										<?php if(!empty($err['job'])): ?>
										<p class="m-formList-error"><?php echo $err['job']; ?></p>
					<?php endif; ?>
												<label class="c-select">
													<select name="job">
													
												<?php foreach ($job_select as $key => $value) {
												?>
												<option value="<?php echo $key; ?>" <?php echo (!empty($data_list['job']) && $data_list['job'] == $key) ? 'selected':'';?>><?php echo $value; ?></option>
												<?php
												}?>
												
													</select>
												</label>
											</dd>
										</dl>

										<dl class="m-formList recruitFormList-startday">
											<dt class="m-formList-ttl">勤務開始可能日</dt>
											<dd class="m-formList-cont">
												<input type="text" name="start_day" class="c-input" value="<?php echo (!empty($data_list['start_day'])) ? $data_list['start_day']:'';?>">
											</dd>
										</dl>

										<dl class="m-formList recruitFormList-ask">
											<dt class="m-formList-ttl">採用に関しての<br class="u-disp-pc">ご希望ご質問</dt>
											<dd class="m-formList-cont">
												<textarea name="ask" cols="30" rows="10" class="c-text"><?php echo (!empty($data_list['ask'])) ? $data_list['ask']:'';?></textarea>
											</dd>
										</dl>

									<!-- .entryForm // --></div>

									<div class="m-privacy">
										<p class="m-privacyTxt">ご送信の前に、必ず「個人情報の取扱いについて」の内容をお読みのうえ、ご同意願います。</p>
										<div class="m-privacyCont">
											<div class="m-privacyCont-inner">

												<div class="m-privacyList">
													<p class="m-privacyList-ttl">（１）事業者の氏名または名称</p>
													<p class="m-privacyList-txt">株式会社ジリオン</p>
												</div>

												<div class="m-privacyList">
													<p class="m-privacyList-ttl">（２）個人情報保護管理者</p>
													<p class="m-privacyList-txt">吉田裕司</p>
												</div>

												<div class="m-privacyList">
													<p class="m-privacyList-ttl">（３）個人情報の利用目的 ・当社事業に関してお問い合わせいただいた内容に回答するため 。</p>
												</div>

												<div class="m-privacyList">
													<p class="m-privacyList-ttl">（４）個人情報の第三者提供について</p>
													<p class="m-privacyList-txt">取得した個人情報は法令等による場合を除いて第三者に提供することはありません。</p>
												</div>

												<div class="m-privacyList">
													<p class="m-privacyList-ttl">（５）個人情報の取扱いの委託について</p>
													<p class="m-privacyList-txt">取得した個人情報の取扱いの全部又は、一部を委託することがあります。</p>
												</div>

												<div class="m-privacyList">
													<p class="m-privacyList-ttl">（６）開示対象個人情報の開示等および問い合わせ窓口について</p>
													<p class="m-privacyList-txt">ご本人からの求めにより、当社が保有する開示対象個人情報の利用目的の通知・開示・内容の訂正・追加または削除・利用の停止・消去および第三者への提供の停止（「開示等」といいます。）に応じます。開示等に応ずる窓口は、お問合せいただきました当該店舗になります。</p>
												</div>

												<div class="m-privacyList">
													<p class="m-privacyList-ttl">（７）個人情報の安全管理措置について</p>
													<p class="m-privacyList-txt">取得した個人情報については、漏洩、減失またはき損の防止と是正、その他個人情報の安全管理のために必要かつ適切な措置を講じます。<br>お問合せへの回答後、取得した個人情報は当社内において削除致します。</p>
												</div>

												<div class="m-privacyList">
													<p class="m-privacyList-ttl">（８）個人情報保護方針</p>
													<p class="m-privacyList-txt">当社ホームページの個人情報保護方針をご覧下さい。</p>
												</div>

												<div class="m-privacyList">
													<p class="m-privacyList-ttl">（９）個人情報を与えなかった場合に生じる結果</p>
													<p class="m-privacyList-txt">個人情報を与えることは任意です。お問い合わせいただいた内容に回答するために必要な個人情報をご提供いただけない場合は、お問い合わせいただいた内容に回答できない場合がございますので了承願います。</p>
												</div>

												<div class="m-privacyList">
													<p class="m-privacyList-ttl">（１０）当社の個人情報の取扱いに関する苦情、相談等の問合せ先</p>
													<p class="m-privacyList-txt">
														窓口の名称 お問い合わせ窓口<br>
														連絡先 住所 ：〒141-0021<br>
														東京都品川区上大崎2-26-5　メグロードビル地下1階<br>
														電話 ：03-6417-0015</p>
												</div>

											<!-- .m-privacyCont-inner // --></div>
										<!-- .m-privacyCont // --></div>
									<!-- .m-privacy // --></div>

									<div class="m-formSubmit recruitSubmit js-checkSubmit">
										<p class="m-formSubmit-agree">
											<span class="c-check">
												<input type="checkbox" id="Agree" class="js-checkSubmit-trigger">
												<label for="Agree">個人情報の取扱いに同意する</label>
											</span>
										</p>
										<div class="m-formSubmit-btns">
											<p class="m-formSubmit-btn"><input type="submit" value="NEXT" class="c-btn c-btn-black js-checkSubmit-target" disabled></p>
										<!-- .entrySubmit-btns // --></div>
									<!-- .entrySubmit // --></div>

					<input type="hidden" name="input" value="1">
								</form>

							<!-- .m-frame-body // --></div>
						<!-- .m-frame-cornerBottom // --></div>
					<!-- .m-frame-cornerTop // --></div>

				<!-- .m-frame-inner // --></div>
			<!-- .m-frame // --></div>

		<!-- .m-container-inner // --></div>
	<!-- .m-container // --></div>

<!-- #Content // --></div>

<? include_once(dirname(__FILE__).'/../../../../shared/inc/footer.inc'); ?>

<!-- #Page // --></div>

<? include_once(dirname(__FILE__).'/../../../../shared/inc/js.inc'); ?>
<script src="/shared/js/lib/ajaxzip3.js" charset="UTF-8"></script>
<script src="js/common.js"></script>
<script src="js/index.js"></script>
</body>
</html>
