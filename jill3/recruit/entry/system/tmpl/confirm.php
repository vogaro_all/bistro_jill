<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie8 lt-ie9" lang="ja">
<![endif]-->
<!--[if IE 9]>
<html class="ie9 lt-ie9" lang="ja">
<![endif]-->
<!--[if !IE]><!-->
<html lang="ja">
<!--<![endif]-->
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>エントリー｜リクルート｜株式会社ジリオン</title>
<meta name="description" content="採用エントリーフォームです。目黒,中目黒,五反田,学芸大学,神田で、大衆ビストロジル/大衆ビストロ煮ジル/JBの3業態を展開しています。">
<meta name="keywords" content="株式会社ジリオン,大衆ビストロジル,煮ジル,JB,肉料理,居酒屋,バル,ワイン,目黒,中目黒,五反田,学芸大学,神田">

<? include_once(dirname(__FILE__).'/../../../../shared/inc/head.inc'); ?>
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/confirm.css">

<? include_once(dirname(__FILE__).'/../../../../shared/inc/ie.inc'); ?>

</head>

<body>
<? include_once(dirname(__FILE__).'/../../../../shared/inc/ga.inc'); ?>

<? include_once(dirname(__FILE__).'/../../../../shared/inc/loading.inc'); ?>

<div id="Page">

<? include_once(dirname(__FILE__).'/../../../../shared/inc/header.php'); ?>

<div id="Content">

	<header class="m-pageHeader">
		<div class="m-pageHeader-inner">
			<h1 class="m-pageHeader-ttl">
				<img src="images/ttl_page.png" alt="ENTRY" width="194" height="44" class="u-disp-pcImg">
				<img src="images/ttl_page_sp.png" alt="" width="123" height="27" class="u-disp-spImg">
				<span class="m-pageHeader-txt-contact">採用面談希望</span>
			<!-- .kvTtl // --></h1>
		<!-- .kv-inner // --></div>
	<!-- .kv // --></header>

	<div class="m-container">
		<div class="m-container-inner l-container">

			<div class="m-frame c-shadow">
				<div class="m-frame-inner">
		<form method="POST">
					<div class="m-frame-cornerTop">
						<div class="m-frame-cornerBottom">
							<div class="m-frame-body">

								<div class="m-flow">
									<p class="m-flow-fig">
										<img src="/shared/images/img_flow02.png" alt="確認" width="345" height="84" class="u-disp-pcImg">
										<img src="/shared/images/img_flow02_sp.png" alt="" width="228" height="57" class="u-disp-spImg">
									</p>
									<p class="m-flow-txt">入力内容をご確認の上、「 ENTRY 」ボタンをクリックしてください。</p>
								<!-- .m-flow // --></div>

								<div class="m-form m-form-confirm recruitForm">

									<dl class="m-formList">
										<dt class="m-formList-ttl">お名前</dt>
										<dd class="m-formList-cont">
											<?php echo (!empty($data_list['name'])) ? $data_list['name']:'';?>
										</dd>
									</dl>

									<dl class="m-formList">
										<dt class="m-formList-ttl">性別</dt>
										<dd class="m-formList-cont">
											<?php echo (!empty($data_list['gender'])) ? $gender_select[$data_list['gender']]:'';?>
										</dd>
									</dl>

									<dl class="m-formList">
										<dt class="m-formList-ttl">年齢</dt>
										<dd class="m-formList-cont">
											<?php echo (!empty($data_list['age'])) ? $data_list['age']:'';?>歳
										</dd>
									</dl>

									<dl class="m-formList">
										<dt class="m-formList-ttl">住所</dt>
										<dd class="m-formList-cont">
											<?php echo (!empty($data_list['zip1'])) ? $data_list['zip1']:'';?><span class="hyphen">ー</span><?php echo (!empty($data_list['zip2'])) ? $data_list['zip2']:'';?> <?php echo (!empty($data_list['address'])) ? $data_list['address']:'';?>
										</dd>
									</dl>
									
									<dl class="m-formList">
										<dt class="m-formList-ttl">メールアドレス</dt>
										<dd class="m-formList-cont">
											<?php echo (!empty($data_list['email'])) ? $data_list['email']:'';?>
										</dd>
									</dl>

									<dl class="m-formList">
										<dt class="m-formList-ttl">電話番号</dt>
										<dd class="m-formList-cont">
											<?php echo (!empty($data_list['tel'])) ? $data_list['tel']:'';?>
										</dd>
									</dl>

									<dl class="m-formList">
										<dt class="m-formList-ttl">希望雇用形態</dt>
										<dd class="m-formList-cont">
											<?php echo (!empty($data_list['job_type'])) ? $job_type_select[$data_list['job_type']]:'';?>
										</dd>
									</dl>

									<dl class="m-formList">
										<dt class="m-formList-ttl">希望勤務地</dt>
										<dd class="m-formList-cont">
											<?php echo (!empty($data_list['shop'])) ? $shop_type_select[$data_list['shop']]:'';?>
										</dd>
									</dl>
									
									<dl class="m-formList">
										<dt class="m-formList-ttl">希望職種</dt>
										<dd class="m-formList-cont">
											<?php echo (!empty($data_list['job'])) ? $job_select[$data_list['job']]:'';?>
										</dd>
									</dl>

									<dl class="m-formList">
										<dt class="m-formList-ttl">勤務開始可能日</dt>
										<dd class="m-formList-cont"><?php echo (!empty($data_list['start_day'])) ? $data_list['start_day']:'';?></dd>
									</dl>

									<dl class="m-formList">
										<dt class="m-formList-ttl">採用に関しての<br class="u-disp-pc">ご希望ご質問</dt>
										<dd class="m-formList-cont"><?php echo (!empty($data_list['ask'])) ? $data_list['ask']:'';?></dd>
									</dl>

								<!-- .entryForm // --></div>

								<div class="m-formSubmit recruitSubmit">
									<div class="m-formSubmit-btns">
										<p class="m-formSubmit-btn"><input name="back" type="submit" value="BACK" class="c-btn c-btn-red"></p>
										<p class="m-formSubmit-btn"><input name="comp" type="submit" value="ENTRY" class="c-btn c-btn-black"></p>
									<!-- .entrySubmit-btns // --></div>
								<!-- .entrySubmit // --></div>

							<!-- .m-frame-body // --></div>
						<!-- .m-frame-cornerBottom // --></div>
						<?php foreach ($data_list as $key => $value) : ?>
			<input type="hidden" name="<?php echo $key; ?>" value="<?php echo $value; ?>">
			<?php endforeach; ?>
						
					<!-- .m-frame-cornerTop // --></div>
				</form>
				<!-- .m-frame-inner // --></div>
			<!-- .m-frame // --></div>

		<!-- .m-container-inner // --></div>
	<!-- .m-container // --></div>

<!-- #Content // --></div>

<? include_once(dirname(__FILE__).'/../../../../shared/inc/footer.inc'); ?>

<!-- #Page // --></div>

<? include_once(dirname(__FILE__).'/../../../../shared/inc/js.inc'); ?>
<script src="js/common.js"></script>
</body>
</html>
