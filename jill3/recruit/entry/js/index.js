;// Global Object
var Page = Page || {};

(function($){

	var $win = $(window);
	var $html = $('html');
	var $body = $('body');
	var $htmlBody = $('html,body');
	var $doc = $(document);

	/*--------------------------------------------------------------------------
		 autoInputZipCode
	---------------------------------------------------------------------------*/
	Page.autoInputZipCode = (function(){
		var p = AutoInputZipCode.prototype;

		/**
		 * 郵便番号を入力してから住所を自動入力
		 *
		 */
		function AutoInputZipCode(){
			this._$trigger = $('.js-autoZip-btn');
			this._zip1 = $('.js-autoZip-zip01').attr('name');
			this._zip2 = $('.js-autoZip-zip02').attr('name');
			this._address = $('.js-autoZip-address').attr('name');

			this._init();
		}

		/**
		 * 初期設定
		 */
		p._init = function(){
			this._onEvent();
		};

		p._onEvent = function(){
			var _this = this;
			this._$trigger.on('click', function(){
				AjaxZip3.zip2addr(_this._zip1, _this._zip2, _this._address, _this._address);
				return false;
			});
		};

		return AutoInputZipCode;
	}());

/*----------------------------------------------------------------------
	DOM READY
----------------------------------------------------------------------*/
	jQuery(function($){
		new Page.autoInputZipCode();
	});

}(jQuery));
