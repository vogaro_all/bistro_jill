;// Global Object
var Page = Page || {};

(function($){

	var $win = $(window);
	var $html = $('html');
	var $body = $('body');
	var $htmlBody = $('html,body');
	var $doc = $(document);

	var manifestPC = [
		{ src: 'images/nijill/img_kv.jpg' },
		{ src: 'images/common/ttl_page.png' },
		{ src: 'images/common/bg_item_bottle.png' },
		{ src: 'images/common/bg_item_chicken.png' },
		{ src: 'images/common/bg_item_cow.png' },
		{ src: 'images/common/bg_item_drink.png' },
		{ src: 'images/common/bg_item_food.png' },
		{ src: 'images/common/bg_item_food02.png' },
		{ src: 'images/common/bg_item_seagal01.png' },
		{ src: 'images/common/bg_item_seagal02.png' },
		{ src: 'images/common/bg_item_wine.png' },
		{ src: 'images/common/bg_menu_list_repeat.png' },
		{ src: 'images/common/bg_repeat_menu_ttl.png' },
		{ src: 'images/common/ttl_menu_beer.png' },
		{ src: 'images/common/ttl_menu_cocktail.png' },
		{ src: 'images/common/ttl_menu_dessert.png' },
		{ src: 'images/common/ttl_menu_dish.png' },
		{ src: 'images/common/ttl_menu_highball.png' },
		{ src: 'images/common/ttl_menu_rice.png' },
		{ src: 'images/common/ttl_menu_salad.png' },
		{ src: 'images/common/ttl_menu_softdrink.png' },
		{ src: 'images/common/ttl_menu_sour.png' },
		{ src: 'images/common/ttl_menu_spirits.png' },
		{ src: 'images/common/ttl_menu_wine.png' },
		{ src: 'images/common/ttl_recommend.png' },
		{ src: 'images/nijill/bg_food01.png' },
		{ src: 'images/nijill/bg_food02.png' },
		{ src: 'images/nijill/bg_food03.png' },
		{ src: 'images/nijill/bg_recommend.jpg' },
		{ src: 'images/nijill/img_drink01.jpg' },
		{ src: 'images/nijill/img_drink02.jpg' },
		{ src: 'images/nijill/img_food01.jpg' },
		{ src: 'images/nijill/img_food02.jpg' },
		{ src: 'images/nijill/img_food03.jpg' }
	];

	var manifestSP = [
		{ src: 'images/nijill/img_kv_sp.jpg' },
		{ src: 'images/common/ttl_page_sp.png' },
		{ src: 'images/common/bg_item_chicken_sp.png' },
		{ src: 'images/common/bg_item_cow_sp.png' },
		{ src: 'images/common/bg_item_drink_sp.png' },
		{ src: 'images/common/bg_item_food_sp.png' },
		{ src: 'images/common/bg_item_food02_sp.png' },
		{ src: 'images/common/bg_item_wine_sp.png' },
		{ src: 'images/common/bg_menu_list_repeat_sp.png' },
		{ src: 'images/common/ttl_menu_beer_sp.png' },
		{ src: 'images/common/ttl_menu_cocktail_sp.png' },
		{ src: 'images/common/ttl_menu_dessert_sp.png' },
		{ src: 'images/common/ttl_menu_dish_sp.png' },
		{ src: 'images/common/ttl_menu_highball_sp.png' },
		{ src: 'images/common/ttl_menu_rice_sp.png' },
		{ src: 'images/common/ttl_menu_salad_sp.png' },
		{ src: 'images/common/ttl_menu_softdrink_sp.png' },
		{ src: 'images/common/ttl_menu_sour_sp.png' },
		{ src: 'images/common/ttl_menu_spirits_sp.png' },
		{ src: 'images/common/ttl_menu_wine_sp.png' },
		{ src: 'images/common/ttl_recommend_sp.png' },
		{ src: 'images/nijill/bg_food01_sp.png' },
		{ src: 'images/nijill/bg_food02_sp.png' },
		{ src: 'images/nijill/bg_food03_sp.png' },
		{ src: 'images/nijill/bg_recommend_sp.jpg' },
		{ src: 'images/nijill/img_drink01_sp.jpg' },
		{ src: 'images/nijill/img_drink02_sp.jpg' },
		{ src: 'images/nijill/img_food01_sp.jpg' },
		{ src: 'images/nijill/img_food02_sp.jpg' },
		{ src: 'images/nijill/img_food03_sp.jpg' }
	];

/*----------------------------------------------------------------------
	DOM READY
----------------------------------------------------------------------*/
	jQuery(function($){

		// PC
		if($html.hasClass('modePC')){
			if(!$html.hasClass('lt-ie9')){
				new Common.loading(manifestPC);
			}
		} else {
			// SP
			new Common.loading(manifestSP);
		}

		$('.js-tab').each(function(){
			new PageCommon.tab($(this));
		});

		$('.js-parallax-target').each(function(){
			new Common.parallax($(this));
		});
	});

}(jQuery));
