;// Global Object
var PageCommon = PageCommon || {};

(function($){

	var $win = $(window);
	var $html = $('html');
	var $body = $('body');
	var $htmlBody = $('html,body');
	var $doc = $(document);

	/*--------------------------------------------------------------------------
		 tab
	---------------------------------------------------------------------------*/
	/*
	* .js-tab-triggerはis-currentクラスによってスタイルを変化させる。
	* .js-tab-targetはjs側で表示・非表示を行う
	*/
	PageCommon.tab = (function(){
		var p = Tab.prototype;

		function Tab($elem){
			this._$tab = $elem;
			this._$trigger = $elem.find('.js-tab-trigger');
			this._$target = $elem.find('.js-tab-target');
			this._activeClass = 'is-current';

			this._init();
		}

		p._init = function(){
			this._activate();
			this._offEvent();
			this._onEvent();
		};

		p._activate = function(){
			var _this = this;
			var hash = location.hash.split('#');
			var defaultId = 'Food';
			var id = hash[1];

			this._$trigger.removeClass(this._activeClass);

			if(id){
				this._$trigger.each(function(){
					if($(this).data('tab') === id){
						$(this).addClass(_this._activeClass);
					}
				});

				this._$target.each(function(){
					if($(this).data('tab') === id){
						_this._show($(this));
					}
				});
			} else {
				this._$trigger.each(function(){
					if($(this).data('tab') === defaultId){
						$(this).addClass(_this._activeClass);
					}
				});

				this._$target.each(function(){
					if($(this).data('tab') === defaultId){
						_this._show($(this));
					}
				});
			}
		};

		p._onEvent = function(){
			var _this = this;
			this._$trigger.on('click.tab', function(){
				_this._onClick($(this));
				return false;
			});
		};

		p._offEvent = function(){
			this._$trigger.off('click.tab');
		};

		p._onClick = function($trigger){
			var _this = this;
			var id = $trigger.data('tab');

			this._$trigger.removeClass(this._activeClass);
			$trigger.addClass(this._activeClass);

			this._$target.each(function(){
				if($(this).data('tab') === id){
					_this._show($(this));
				}
			});
		};

		p._show = function($elem){
			this._$target.removeClass(this._activeClass);
			$elem.addClass(this._activeClass);
		};

		return Tab;
	}());

/*----------------------------------------------------------------------
	DOM READY
----------------------------------------------------------------------*/
	jQuery(function($){

		$('.js-btnHover').each(function(){
			new Common.btnHover($(this));
		});

	});

}(jQuery));
