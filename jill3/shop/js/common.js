;// Global Object
var PageCommon = PageCommon || {};

(function($){

	var $win = $(window);
	var $html = $('html');
	var $body = $('body');
	var $htmlBody = $('html,body');
	var $doc = $(document);

/*----------------------------------------------------------------------
	slider
----------------------------------------------------------------------*/
	PageCommon.slider = (function(){
		var p = Slider.prototype;

		/**
		 * スライダー
		 */
		function Slider(){
			this._$wrap = $('#Slider');
			this._$lists = this._$wrap.find('.slider-lists');
			this._moveVal = 0;
			this._speed = 0.5;

			Common.controller.on('load', this._init.bind(this));
		}

		/**
		 * 初期化
		 */
		p._init = function(){
			this._onResize();
			TweenMax.to(this._$wrap, 1, {opacity: 1});
			this._tween();
			Common.controller.on('resize', this._onResize.bind(this));
		};

		/**
		 * リサイズ時の処理
		 */
		p._onResize = function(){
			if($html.hasClass('modePC')){
				this._reset();
			} else {
				this._setWidth();
			}
		};

		/**
		 * 各スライドの幅設定
		 */
		p._setWidth = function(){
			var $list = this._$lists.find('.slider-list'),
				width = $win.width() / 2,
				len = $list.length,
				totalW = width * len;

			$list.css({ width: width });

			this._$lists.css({ width: totalW });
		};

		/**
		 * 初期化
		 */
		p._reset = function(){
			this._$lists.attr('style', '');
			this._$lists.find('.slider-list').attr('style', '');
		};

		/**
		 * アニメーション設定
		 */
		p._tween = function(){
			var $first = this._$lists.find('.slider-list:first');

			this._moveVal += this._speed;

			if(Math.abs($first.offset().left) >= $first.width()){
				this._moveVal = 0;
				this._$lists.append($first);
			}

			if($html.hasClass('csstransforms3d')){
				this._$lists.css({ 'transform': 'translate3d(' + -this._moveVal  + 'px,0,0)'});
			} else {
				this._$lists.css({ marginLeft: -this._moveVal });
			}

			requestAnimationFrame(this._tween.bind(this));

		};

		return Slider;
	}());

/*----------------------------------------------------------------------
	DOM READY
----------------------------------------------------------------------*/
	jQuery(function($){

		$('.js-btnHover').each(function(){
			new Common.btnHover($(this));
		});

		new PageCommon.slider();

		$('.js-parallax-target').each(function(){
			new Common.parallax($(this));
		});

	});

}(jQuery));
