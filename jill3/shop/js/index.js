;// Global Object
var Page = Page || {};

(function($){

	var $win = $(window);
	var $html = $('html');
	var $body = $('body');
	var $htmlBody = $('html,body');
	var $doc = $(document);

	var manifestPC = [
		{ src: 'images/index/img_kv.jpg' },
		{ src: 'images/index/ttl_page.png' },
		{ src: 'images/index/img_slider01.jpg' },
		{ src: 'images/index/img_slider02.jpg' },
		{ src: 'images/index/img_slider03.jpg' },
		{ src: 'images/index/img_slider04.jpg' },
		{ src: 'images/index/img_drink.jpg' },
		{ src: 'images/index/img_food.jpg' }
	];

	var manifestSP = [
		{ src: 'images/index/img_kv_sp.jpg' },
		{ src: 'images/index/ttl_page_sp.png' },
		{ src: 'images/index/img_slider01.jpg' },
		{ src: 'images/index/img_slider02.jpg' },
		{ src: 'images/index/img_slider03.jpg' },
		{ src: 'images/index/img_slider04.jpg' },
		{ src: 'images/index/img_drink.jpg' },
		{ src: 'images/index/img_food.jpg' }
	];

/*----------------------------------------------------------------------
	DOM READY
----------------------------------------------------------------------*/
	jQuery(function($){

		// PC
		if($html.hasClass('modePC')){
			if(!$html.hasClass('lt-ie9')){
				new Common.loading(manifestPC);
			}
		} else {
			// SP
			new Common.loading(manifestSP);
		}
	});

}(jQuery));
