;// Global Object
var Page = Page || {};

(function($){

	var $win = $(window);
	var $html = $('html');
	var $body = $('body');
	var $htmlBody = $('html,body');
	var $doc = $(document);

	var manifestPC = [
		{ src: 'images/nijill/img_kv.jpg' },
		{ src: 'images/nijill/ttl_page.png' },
		{ src: 'images/nijill/img_slider01.jpg' },
		{ src: 'images/nijill/img_slider02.jpg' },
		{ src: 'images/nijill/img_slider03.jpg' },
		{ src: 'images/nijill/img_slider04.jpg' },
		{ src: 'images/nijill/img_drink.jpg' },
		{ src: 'images/nijill/img_food.jpg' }
	];

	var manifestSP = [
		{ src: 'images/nijill/img_kv_sp.jpg' },
		{ src: 'images/nijill/ttl_page_sp.png' },
		{ src: 'images/nijill/img_slider01.jpg' },
		{ src: 'images/nijill/img_slider02.jpg' },
		{ src: 'images/nijill/img_slider03.jpg' },
		{ src: 'images/nijill/img_slider04.jpg' },
		{ src: 'images/nijill/img_drink.jpg' },
		{ src: 'images/nijill/img_food.jpg' }
	];

/*----------------------------------------------------------------------
	DOM READY
----------------------------------------------------------------------*/
	jQuery(function($){

		// PC
		if($html.hasClass('modePC')){
			if(!$html.hasClass('lt-ie9')){
				new Common.loading(manifestPC);
			}
		} else {
			// SP
			new Common.loading(manifestSP);
		}
	});

}(jQuery));
