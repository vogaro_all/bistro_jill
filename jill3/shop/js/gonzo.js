;// Global Object
var Page = Page || {};

(function($){

	var $win = $(window);
	var $html = $('html');
	var $body = $('body');
	var $htmlBody = $('html,body');
	var $doc = $(document);

	var manifestPC = [
		{ src: 'images/jb/img_kv.jpg' },
		{ src: 'images/jb/ttl_page.png' },
		{ src: 'images/jb/img_slider01.jpg' },
		{ src: 'images/jb/img_slider02.jpg' },
		{ src: 'images/jb/img_slider03.jpg' },
		{ src: 'images/jb/img_slider04.jpg' },
		{ src: 'images/jb/img_drink.jpg' },
		{ src: 'images/jb/img_food.jpg' }
	];

	var manifestSP = [
		{ src: 'images/jb/img_kv_sp.jpg' },
		{ src: 'images/jb/ttl_page_sp.png' },
		{ src: 'images/jb/img_slider01.jpg' },
		{ src: 'images/jb/img_slider02.jpg' },
		{ src: 'images/jb/img_slider03.jpg' },
		{ src: 'images/jb/img_slider04.jpg' },
		{ src: 'images/jb/img_drink.jpg' },
		{ src: 'images/jb/img_food.jpg' }
	];
	
	
	/*--------------------------------------------------------------------------
		 mapInit
	---------------------------------------------------------------------------*/
	Page.mapInit = (function(){
		var p = mapInit.prototype;

		/**
		 * カスタマイズgoogle map
		 */
		function mapInit(){

			this._init();
		}

		/**
		 * 初期化
		 */
		p._init = function(){
			// マップ基本設定
			var latlng = new google.maps.LatLng(35.606441, 139.666997);
			var myOptions = {
				zoom: 18,
				center: latlng,
				disableDefaultUI: true,
				scrollwheel: false,
				zoomControl: true,//falseで非表示
				zoomControlOptions: {
					style: google.maps.ZoomControlStyle.LARGE,
					position: google.maps.ControlPosition.TOP_LEFT
				}
			};
			var map = new google.maps.Map($('#Map')[0], myOptions);

			// マーカー
			var markerImg = {
				url: 'images/common/img_pin.png',
				scaledSize: new google.maps.Size(49, 41)
			};
			var marker = new google.maps.Marker({
				position: latlng,
				map: map,
				icon: markerImg
			});

			// デザイン変更
			var mapStyle = [
				{
					'stylers': [
						{ 'saturation': -95 }
					]
				}
			];
			var mapType = new google.maps.StyledMapType(mapStyle);
			map.mapTypes.set('GrayScaleMap', mapType);
			map.setMapTypeId('GrayScaleMap');

			google.maps.event.addDomListener(window, 'resize', function(){
				map.panTo(latlng);
			});
		};

		return mapInit;
	}());

/*----------------------------------------------------------------------
	DOM READY
----------------------------------------------------------------------*/
	jQuery(function($){

		// PC
		if($html.hasClass('modePC')){
			if(!$html.hasClass('lt-ie9')){
				new Common.loading(manifestPC);
			}
		} else {
			// SP
			new Common.loading(manifestSP);
		}
		
		new Page.mapInit();
	});

}(jQuery));
