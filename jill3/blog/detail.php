<?php
include_once(dirname(__FILE__)."/../lacne/blog/output/post.php");

$search_param = array();
$category = "";
if (isset($_GET['cat']) && is_numeric($_GET['cat'])) {
	$category = $_GET['cat'];
	$search_param["post.category ="] = fn_esc($category);
}

if ($_GET['date'] && is_numeric($_GET['date']) && mb_strlen($_GET['date']) == 6) {

	$date = array();
	preg_match("/([0-9]{4,})([0-9]{2,})/",$_GET['date'],$date);
	$label_data = $date[1].'-'.$date[2];
	$search_date = $_GET['date'];
	$search_param["post.output_date LIKE"] = "%".fn_esc($label_data)."%";
}

$_post_data = LACNE_Post("", $search_param); 
$post_data = fn_esc($_post_data);
?>
<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie8 lt-ie9" lang="ja">
<![endif]-->
<!--[if IE 9]>
<html class="ie9 lt-ie9" lang="ja">
<![endif]-->
<!--[if !IE]><!-->
<html lang="ja">
<!--<![endif]-->
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title><?=the_title()?>｜ブログ｜株式会社ジリオン</title>
<meta name="description" content="<?=the_meta_description()?>">
<meta name="keywords" content="<?=the_meta_keyword()?>">

<? include_once(dirname(__FILE__).'/../shared/inc/head.inc'); ?>
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/detail.css">

<? include_once(dirname(__FILE__).'/../shared/inc/ie.inc'); ?>

</head>

<body>
<? include_once(dirname(__FILE__).'/../shared/inc/ga.inc'); ?>

<? include_once(dirname(__FILE__).'/../shared/inc/loading.inc'); ?>

<div id="Page">

<? include_once(dirname(__FILE__).'/../shared/inc/header.php'); ?>

<div id="Content">

	<header class="m-pageHeader">
		<div class="m-pageHeader-inner">
			<p class="m-pageHeader-ttl">
				<img src="images/ttl_page.png" alt="SHOP BLOG" width="347" height="41" class="u-disp-pcImg">
				<img src="images/ttl_page_sp.png" alt="" width="223" height="26" class="u-disp-spImg">
			<!-- .m-pageHeader-ttl // --></p>
		<!-- .m-pageHeader-inner // --></div>
	<!-- .m-pageHeade // --></header>

	<div class="blogNav">
		<div class="blogNav-inner u-cf">
				<?php 
				$category_list = get_categories();
				?>
        <div class="blogNavSelects">
            <div class="blogNavSelect">
                <form action="index.html" method="get">
                    <label class="blogSelect">
                        <select name="shop" class="js-sendURL">
                        <option value="./">店舗を選択</option>
                        <option value="./?all=1" <?=($_GET['all'] == 1) ? "selected" : ""?>>ALL</option>
                        <?php foreach ($category_list as $key => $value) {?>
                        <option value="./?cat=<?=$value['id']?>" <?=($value['id'] == $category) ? "selected" : ""?>><?php echo($value['name']);?></option>
                        <?php }?>
                        </select>
                    </label>
                </form>
            <!-- .blogNavSelect // --></div>
            
            
		<div class="blogNavSelect">
			<form action="detail.html" method="get">
				<label class="blogSelect">
					<select name="date" class="js-sendURL">
					
<?php
$date_archive =  printDate($category);
foreach ($date_archive as $key => $value) {
?>
<option value="detail.php?date=<?=$value['output_year_month']?>&cat=<?=$category?>" <?=($value['output_year_month'] == $search_date) ? "selected" : ""?>><?=$value['output_year']?>年 <?=$value['output_month']?>月</option>
<?php
}
?>

					</select>
				</label>
			</form>
		<!-- .blogNavSelect // --></div>
      <!-- .blogNavSelects // --></div>
      
		<!-- .blogNav-inner // --></div>
	<!-- .blogNav // --></div>

	<div class="m-container">
		<div class="m-container-inner l-container">

			<div class="detail">

				<div class="detailInfo">
				<?php
					$category_tag = "";
					if ($post_data['category_group'] == 1) {
						$category_tag = '<span class="detailInfo-tag c-tag c-tag-jill c-tag-large">'.the_categoryname().'</span>';
					} elseif ($post_data['category_group'] == 2) {
						$category_tag = '<span class="detailInfo-tag c-tag c-tag-nijill c-tag-large">'.the_categoryname().'</span>';
					}  elseif ($post_data['category_group'] == 3) {
						$category_tag = '<span class="detailInfo-tag c-tag c-tag-jb c-tag-large">'.the_categoryname().'</span>';
					}  elseif ($post_data['category_group'] == 4) {
						$category_tag = '<span class="detailInfo-tag c-tag c-tag-shinatora c-tag-large">'.the_categoryname().'</span>';
					}  elseif ($post_data['category_group'] == 5) {
						$category_tag = '<span class="detailInfo-tag c-tag c-tag-jillion c-tag-large">'.the_categoryname().'</span>';
					}  elseif ($post_data['category_group'] == 6) {
						$category_tag = '<span class="detailInfo-tag c-tag c-tag-rikuu c-tag-large">'.the_categoryname().'</span>';
					}
				?>
				<?=$category_tag?>
					<span class="detailInfo-date"><?=the_date()?></span>
				<!-- .detailInfo // --></div>

				<h1 class="detailTtl"><?=the_title()?></h1>


<?php
//---------------------------------------------
// パーツデータ（セクションデータ）を表示
//---------------------------------------------
if(!empty($_post_data['section'])) {
	foreach($_post_data['section'] as $section_no => $section) {
		$key = key($section);
		$param = $section[$key];
		$param['section_no'] = $section_no;
		//各パーツのテンプレートを lacne/ownedmedia/output/template/ 以下に設置する
		$LACNE->template->setViewDir(LACNE_APP_DIR_OUTPUT_TEMPLATE); //テンプレのディレクトリ指定
	
		$escape_flg = true;
		if ($key == 'img2' || $key == 'text1') {
			$escape_flg = false;
		}
		$parts_html = $LACNE->render('parts_'.$key , $param , true, $escape_flg); //パーツのテンプレにデータを入れて生成htmlを取得

		echo $parts_html; //表示
	}
}
?>
			<!-- .detail // --></div>


<?php
//----------------------------------------
//前後のデータを取得して表示
//----------------------------------------
$page_data = getNextAndPrevData($post_data['id'], $search_param);

$add_param = "";
if (isset($search_date) && $search_date) {
	$add_param .= "&date=".$search_date;
}
if (isset($category) && $category) {
	$add_param .= "&cat=".$category;
}
?>



			<div class="detailPager u-cf">
<?php if(!empty($page_data['prev'])) : ?>
<p class="detailPager-prev"><a href="/blog/detail.php?id=<?=$page_data['prev']['id']?><?=$add_param?>"><span class="detailPager-cell"><span>前の記事</span></span></a></p>
<?php else: ?>
<p class="detailPager-prev"><a href="javascript:void(0)" class="no-link"><span class="detailPager-cell"><span>前の記事</span></span></a></p>
<?php endif; ?>

<p class="detailPager-back"><a href="/blog/"><span class="detailPager-cell"><span>一覧に戻る</span></span></a></p>

<?php if(!empty($page_data['next'])) : ?>
<p class="detailPager-next"><a href="/blog/detail.php?id=<?=$page_data['next']['id']?><?=$add_param?>"><span class="detailPager-cell"><span>次の記事</span></span></a></p>
<?php else: ?>
<p class="detailPager-next"><a href="javascript:void(0)" class="no-link"><span class="detailPager-cell"><span>次の記事</span></span></a></p>
<?php endif; ?>
<!--
				<p class="detailPager-prev"><a href="detail.html"><span class="detailPager-cell"><span>前の記事</span></span></a></p>
				<p class="detailPager-back"><a href="index.html"><span class="detailPager-cell"><span>一覧に戻る</span></span></a></p>
				<p class="detailPager-next"><a href="detail.html"><span class="detailPager-cell"><span>次の記事</span></span></a></p>
-->
			<!-- .detailPager // --></div>

		<!-- .container-inner // --></div>
	<!-- .container // --></div>

<!-- #Content // --></div>

<? include_once(dirname(__FILE__).'/../shared/inc/footer.inc'); ?>

<!-- #Page // --></div>

<? include_once(dirname(__FILE__).'/../shared/inc/js.inc'); ?>
<script src="js/detail.js"></script>
</body>
</html>
