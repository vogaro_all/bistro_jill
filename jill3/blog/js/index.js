;// Global Object
var Page = Page || {};

(function($){

	var $win = $(window);
	var $html = $('html');
	var $body = $('body');
	var $htmlBody = $('html,body');
	var $doc = $(document);

	var manifestPC = [
		{ src: 'images/img_kv.jpg' },
		{ src: 'images/ttl_page.png' },
		{ src: 'images/ico_arrow_blog_nav.png' }
	];

	var manifestSP = [
		{ src: 'images/img_kv_sp.jpg' },
		{ src: 'images/ttl_page_sp.png' },
		{ src: 'images/ico_arrow_blog_nav_sp.png' }
	];

/*----------------------------------------------------------------------
	DOM READY
----------------------------------------------------------------------*/
	jQuery(function($){
		new Common.FlatHeight($('.blogCard-body'), 3);

		// PC
		if($html.hasClass('modePC')){
			if(!$html.hasClass('lt-ie9')){
				new Common.loading(manifestPC);
			}
		} else {
			// SP
			new Common.loading(manifestSP);
		}

	});

}(jQuery));
