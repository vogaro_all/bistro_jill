<?php
include_once(dirname(__FILE__)."/../lacne/blog/output/post.php");

$category = "";
if (isset($_GET['cat']) && is_numeric($_GET['cat'])) {
	$category = $_GET['cat'];
}

if ($_GET['date'] && is_numeric($_GET['date']) && mb_strlen($_GET['date']) == 6) {

	$date = array();
	preg_match("/([0-9]{4,})([0-9]{2,})/",$_GET['date'],$date);
	$label_data = $date[1].'-'.$date[2];
	$search_date = $_GET['date'];
}
?>
<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie8 lt-ie9" lang="ja">
<![endif]-->
<!--[if IE 9]>
<html class="ie9 lt-ie9" lang="ja">
<![endif]-->
<!--[if !IE]><!-->
<html lang="ja">
<!--<![endif]-->
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>ブログ｜株式会社ジリオン</title>
<meta name="description" content="「ブログ」をご紹介いたします。目黒,中目黒,五反田,学芸大学で、大衆ビストロジル/大衆ビストロ煮ジルを展開しています。">
<meta name="keywords" content="株式会社ジリオン,大衆ビストロジル,煮ジル,肉料理,居酒屋,バル,ワイン,目黒,中目黒,五反田,学芸大学">

<? include_once(dirname(__FILE__).'/../shared/inc/head.inc'); ?>
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/index.css">

<? include_once(dirname(__FILE__).'/../shared/inc/ie.inc'); ?>

</head>

<body>
<? include_once(dirname(__FILE__).'/../shared/inc/ga.inc'); ?>

<? include_once(dirname(__FILE__).'/../shared/inc/loading.inc'); ?>

<div id="Page">

<? include_once(dirname(__FILE__).'/../shared/inc/header.php'); ?>

<div id="Content">

	<header class="m-pageHeader">
		<div class="m-pageHeader-inner">
			<h1 class="m-pageHeader-ttl">
				<img src="images/ttl_page.png" alt="SHOP BLOG" width="347" height="41" class="u-disp-pcImg">
				<img src="images/ttl_page_sp.png" alt="" width="223" height="26" class="u-disp-spImg">
			<!-- .m-pageHeader-ttl // --></h1>
		<!-- .m-pageHeader-inner // --></div>
	<!-- .m-pageHeade // --></header>

	<div class="blogNav">
		<div class="blogNav-inner u-cf">
				<?php
				$category_list = get_categories();
				$cat_title = "";
				foreach ($category_list as $key => $value) {
					if (!isset($cat_title) || empty($cat_title)) {
						$cat_title = ($value['id'] == $category) ? $value['name'] : "";
					}
				}
				if (!isset($cat_title) || empty($cat_title)) {
					$cat_title = "ALL";
				}
				?>
        <div class="blogNavSelects">
            <div class="blogNavSelect">
                <form action="index.html" method="get">
                    <label class="blogSelect">
                        <select name="shop" class="js-sendURL">
                        <option value="./">店舗を選択</option>
                        <option value="./?all=1" <?=($_GET['all'] == 1) ? "selected" : ""?>>ALL</option>
                        <?php foreach ($category_list as $key => $value) {?>
                        <option value="./?cat=<?=$value['id']?>" <?=($value['id'] == $category) ? "selected" : ""?>><?php echo($value['name']);?></option>
                        <?php }?>
                        </select>
                    </label>
                </form>
            <!-- .blogNavSelect // --></div>

		<div class="blogNavSelect">
			<form action="index.html" method="get">
				<label class="blogSelect">
					<select name="date" class="js-sendURL">

<?php
$date_archive =  printDate($category);
foreach ($date_archive as $key => $value) {
?>
<option value="index.php?date=<?=$value['output_year_month']?>" <?=($value['output_year_month'] == $search_date) ? "selected" : ""?>><?=$value['output_year']?>年 <?=$value['output_month']?>月</option>
<?php
}
?>
					</select>
				</label>
			</form>
		<!-- .blogNavSelect // --></div>
      <!-- .blogNavSelects // --></div>

		<!-- .blogNav-inner // --></div>
	<!-- .blogNav // --></div>

	<div class="m-container">
		<div class="m-container-inner l-container">

			<p class="c-diagonalTtl"><?=$cat_title?></p>

			<div class="blogCards u-cf">

<?php

$params = array(
		"num"         => '9',
		"category"     => $category,
		"date_target" => $label_data,
		"search_date" => $search_date,
		"postmeta"      => true
);

echo printList($params , 'list');
?>

			<!-- .blogCards // --></div>


<?php
$url_param="cat=".$category.'&date='.$search_date;
echo renderPager(9 , "pager_list" , "" , $url_param);
?>

		<!-- .m-container-inner // --></div>
	<!-- .m-container // --></div>

<!-- #Content // --></div>

<? include_once(dirname(__FILE__).'/../shared/inc/footer.inc'); ?>

<!-- #Page // --></div>

<? include_once(dirname(__FILE__).'/../shared/inc/js.inc'); ?>
<script src="js/index.js"></script>

</body>
</html>
