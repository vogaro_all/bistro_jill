@charset 'UTF-8';
/**
 *
 *  コンテンツ共通
 *
 * c- コンポーネント 単一要素で構成された小さいパーツ
 * m- モジュール 複数要素で構成されたパーツ群
 * u- ユーティリティ 補助的な役割を持ったクラス
 * l- レイアウト レイアウトクラス
 *
 * c-scrollTop スクロールトップボタン
 * c-tag タグ
 *  c-tag-new NEW
 *  c-tag-jill ジルカラー
 *  c-tag-nijill 煮ジルカラー
 *  c-tag-jb JBカラー
 *  c-tag-large ブログページなどで使用される大きいサイズのタグ
 * c-btn ボタン ボタンの初期化などの基本スタイル
 *  c-btn-white 白ボーダー、白文字、背景なし
 *  c-btn-black　黒ボーダー、黒文字、背景なし
 *  c-btn-red　赤ボーダー、赤文字、背景なし
 *  c-btn-bigArrow ホバー時に大きな矢印が表示されるホバー効果。単体では使用できない
 * c-input フォームのテキストボックス
 * c-text テキストエリア
 * c-radio ラジオボタン
 * c-select セレクトボックス
 * c-check チェックボックス
 * c-borderTtl 下線付きタイトル
 * c-loading ローディング
 * c-shadow 影つきフレーム
 * c-diagonalTtl 斜線付きタイトル
 * c-underlinedTtl 下線付きタイトル
 *
 * m-container 下層コンテナ
 * m-frame 装飾つきフレーム
 * m-flow フォームのフロー
 * m-privacy フォームの個人情報保護エリア
 * m-form フォームのラッパー要素
 * m-formList フォーム項目
 * m-formSubmit コンバージョンボタンエリア
 * m-pageHeader 下層タイトルエリア
 * m-pager ページャー
 * m-footLink ページ下部のコンバージョンセクション
 *
 * u-disp- レスポンシブによる要素の表示・非表示
 * u-cf クリアフィックス
 *
 * l-container
 */
/*--------------------------------------------------------------------------

	 component コンポーネント

 ---------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------
	c-scrollTop スクロールトップボタン
---------------------------------------------------------------------------*/
.c-scrollTop{
	position: absolute;
	top: -45px;
	right: 80px;
	z-index: 1000;
	width: 90px;
	height: 90px;
	border-radius: 45px;
	background: #5b5b5b;
	-webkit-transition: background-color .2s ease;
	        transition: background-color .2s ease;
}

.c-scrollTop a{
	display: block;
	position: relative;
	text-align: center;
	line-height: 90px;
}

.c-scrollTop img{
	vertical-align: middle;
}

/* animation
-----------------------------------------------------------------*/
.c-scrollTop{
	-webkit-transition: background-color .2s ease;
	        transition: background-color .2s ease;
}

/* hover
-----------------------------------------------------------------*/
.modePC .c-scrollTop:hover{
	background: #bda671;
}

@media screen and (max-width: 768px){
	.c-scrollTop{
		top: -23px;
		right: 10px;
		opacity: 1;
		width: 46px;
		height: 46px;
		border-radius: 23px;
	}
	.c-scrollTop a{
		line-height: 46px;
	}
	.c-scrollTop img{
		width: 18px;
		height: auto;
	}
}

/*--------------------------------------------------------------------------
	 c-tag タグ
---------------------------------------------------------------------------*/
.c-tag{
	display: inline-block;
	width: 120px;
	border-radius: 3px;
	font-size: 11px;
	font-weight: normal;
	color: #fff;
	text-align: center;
	line-height: 22px;
}

/* modifier
-----------------------------------------------------------------*/
.c-tag-new{
	width: 50px;
	background: #bda671;
}

.c-tag-jill{
	background: #9e2416;
}

.c-tag-nijill{
	background: #0a0404;
}

.c-tag-jb{
	background: #755858;
}

.c-tag-shinatora{
	background: #0f1b6e;
}
.c-tag-shinatora{
	background: #0f1b6e;
}
.c-tag-jillion{
	background: #4caf50;
   }
.c-tag-large{
	width: 130px;
	border-radius: 4px;
	font-size: 12px;
	line-height: 32px;
}

.c-tag-shop01,
.c-tag-shop02{
    width: 58px;
    font-size: 10px;
    font-weight: bold;
}

.c-tag-shop01{
    background: #9e2416;
}

.c-tag-shop02{
    background: #3e130e;
}

@media screen and (max-width: 768px){
	.c-tag{
		width: 110px;
		font-size: 10px;
		line-height: 15px;
	}
	/* modifier
	-----------------------------------------------------------------*/
	.c-tag-new{
		width: 40px;
	}
	.c-tag-large{
		line-height: 27px;
	}
}

/*--------------------------------------------------------------------------
	 c-btn ボタンの基本スタイル
---------------------------------------------------------------------------*/
.c-btn{
	display: inline-block;
	border: none;
	border-radius: 0;
	background: none;
	cursor: pointer;
	font-family: 'MatrixIIOT-ExtraBold','小塚ゴシック Pro','游ゴシック',YuGothic,'ヒラギノ角ゴ ProN W3','Hiragino Kaku Gothic ProN','ヒラギノ角ゴ Pro W3','Hiragino Kaku Gothic Pro','メイリオ','Meiryo','ＭＳ Ｐゴシック','MS PGothic',Sans-Serif;
	text-decoration: none;
	text-align: center;
	letter-spacing: .26em;
}

.ie .c-btn{
	font-family: 'MatrixIIOT-ExtraBold','游ゴシック',YuGothic,'ヒラギノ角ゴ ProN W3','Hiragino Kaku Gothic ProN','ヒラギノ角ゴ Pro W3','Hiragino Kaku Gothic Pro','メイリオ','Meiryo','ＭＳ Ｐゴシック','MS PGothic',Sans-Serif;
}

@media screen and (max-width: 768px){
	.c-btn{
		letter-spacing: .2em;
	}
}

/* c-btn-white 白ボーダー、白文字、背景なし
-----------------------------------------------------------------*/
.c-btn-white{
	border: 1px solid #fff;
	color: #fff;
	-webkit-transition: all .3s ease;
	        transition: all .3s ease;
}

/* hover
-----------------------------------------------------------------*/
.modePC .c-btn-white:hover{
	color: #000;
	background-color: #fff;
}

/* c-btn-black　黒ボーダー、黒文字、背景なし
-----------------------------------------------------------------*/
.c-btn-black{
	border: 1px solid #000;
	color: #000;
	-webkit-transition: all .3s ease;
	        transition: all .3s ease;
}

/* disabled
-----------------------------------------------------------------*/
.c-btn-black:disabled{
	border-color: #ccc;
	color: #bbb;
}

/* hover
-----------------------------------------------------------------*/
.modePC .c-btn-black:hover{
	color: #fff;
	background-color: #000;
}

.modePC .c-btn-black:disabled:hover{
	border-color: #ccc;
	background-color: #fff;
	color: #bbb;
}

/* c-btn-recruit　黒ボーダー、白文字、背景赤
-----------------------------------------------------------------*/
.c-btn-recruit{
	border: 1px solid #000;
	color: #fff;
    	background-color: #9E3529;
	-webkit-transition: all .3s ease;
	        transition: all .3s ease;
}
/* disabled
-----------------------------------------------------------------*/
.c-btn-recruit:disabled{
	border-color: #ccc;
	color: #bbb;
}

/* hover
-----------------------------------------------------------------*/
.modePC .c-btn-recruit:hover{
	color: #fff;
	background-color: #000;
}

.modePC .c-btn-recruit:disabled:hover{
	border-color: #ccc;
	background-color: #fff;
	color: #bbb;
}

/* c-btn-red　赤ボーダー、赤文字、背景なし
-----------------------------------------------------------------*/
.c-btn-red{
	border: 1px solid #9e2415;
	color: #9e2415;
	-webkit-transition: all .3s ease;
	        transition: all .3s ease;
}

/* hover
-----------------------------------------------------------------*/
.modePC .c-btn-red:hover{
	color: #fff;
	background-color: #9e2415;
}

/* c-btn-bigArrow ホバー時に大きな矢印が表示されるホバー効果。単体では使用できない
-----------------------------------------------------------------*/
.c-btn-bigArrow{
	position: relative;
	border: 1px solid #fff;
	color: #fff;
}

.c-btn-bigArrow:after{
	content: '';
	display: block;
	position: absolute;
	top: -23px;
	left: -38px;
	width: 0;
	height: 130px;
	background: url(/shared/images/ico_arrow_big.png) no-repeat;
	-webkit-transition: all .5s cubic-bezier(0, .98, .49, 1.02);
	        transition: all .5s cubic-bezier(0, .98, .49, 1.02);
}

@media screen and (max-width: 768px){
	.c-btn-bigArrow:after{
		display: none;
	}
}

/* animation
-----------------------------------------------------------------*/
.c-btn-bigArrow:after{
	-webkit-transition: all .5s cubic-bezier(0, .98, .49, 1.02);
	        transition: all .5s cubic-bezier(0, .98, .49, 1.02);
}

/* hover
-----------------------------------------------------------------*/
.modePC .c-btn-bigArrow:hover:after{
	width: 453px;
}

/*--------------------------------------------------------------------------
	 c-input フォームのテキストボックス
---------------------------------------------------------------------------*/
.c-input{
	-webkit-appearance: none;
	   -moz-appearance: none;
	        appearance: none;
	width: 100%;
	padding: 6px 10px;
	border: 1px solid #dcd2ba;
	border-radius: 0;
	background: #fefdfa;
}

@media screen and (max-width: 768px){
	.c-input{
		font-size: 12px;
	}
}

/*--------------------------------------------------------------------------
	 c-text テキストエリア
---------------------------------------------------------------------------*/
.c-text{
	-webkit-appearance: none;
	   -moz-appearance: none;
	        appearance: none;
	width: 100%;
	height: 220px;
	padding: 6px 10px;
	border: 1px solid #dcd2ba;
	border-radius: 0;
	background: #fefdfa;
	resize: none;
}

@media screen and (max-width: 768px){
	.c-text{
		font-size: 12px;
	}
}

/*--------------------------------------------------------------------------
	 c-radio ラジオボタン
---------------------------------------------------------------------------*/
.c-radio{
	display: inline-block;
	vertical-align: middle;
}

.c-radio label{
	display: inline-block;
	position: relative;
	padding-left: 30px;
	vertical-align: middle;
	cursor: pointer;
}

.c-radio label:before{
	content: '';
	display: block;
	position: absolute;
	top: 50%;
	left: 0;
	width: 18px;
	height: 18px;
	margin-top: -10px;
	border: 1px solid #dcd2ba;
	border-radius: 9px;
}

.c-radio label:after{
	content: '';
	display: none;
	position: absolute;
	top: 50%;
	left: 10px;
	width: 12px;
	height: 12px;
	margin-top: -6px;
	margin-left: -6px;
	border-radius: 6px;
	background: #9e2416;
}

.c-radio input:checked + label:after{
	display: block;
}

.c-radio input[type='radio']{
	display: none;
}

@media screen and (max-width: 768px){
	.c-radio label{
		padding-left: 24px;
	}
	.c-radio label:before{
		width: 15px;
		height: 15px;
		margin-top: -8px;
		border-radius: 8px;
	}
	.c-radio label:after{
		left: 9px;
		width: 11px;
		height: 11px;
		margin-top: -5px;
	}
}

/*--------------------------------------------------------------------------
	 c-select セレクトボックス
---------------------------------------------------------------------------*/
.c-select{
	display: inline-block;
	position: relative;
	width: 100%;
}

.c-select:after{
	content: '';
	display: block;
	position: absolute;
	right: 12px;
	top: 50%;
	width: 12px;
	height: 8px;
	margin-top: -4px;
	background: url(/shared/images/ico_arrow_select.png) no-repeat 0 0;
}

.c-select select{
	display: inline-block;
	width: 100%;
	padding: 6px 30px 6px 10px;
	-webkit-appearance: none;
	   -moz-appearance: none;
	        appearance: none;
	border: 1px solid #dcd2ba;
	border-radius: 0;
	background: #fefdfa;
	cursor: pointer;
}

/* ie
-----------------------------------------------------------------*/
.c-select select::-ms-expand{
	display: none;
}

.lt-ie9 .c-select:after{
	display: none;
}

@media screen and (max-width: 768px){
	.c-select:after{
		right: 10px;
		background-image: url(/shared/images/ico_arrow_select_sp.png);
		background-size: 12px 8px;
	}
	.c-select select{
		font-size: 12px;
	}
}

/*--------------------------------------------------------------------------
	 c-check チェックボックス
---------------------------------------------------------------------------*/
.c-check{
	display: inline-block;
}

.c-check label{
	display: inline-block;
	position: relative;
	padding-left: 26px;
	vertical-align: middle;
	cursor: pointer;
}

.c-check label:before{
	content: '';
	display: inline-block;
	position: absolute;
	top: 50%;
	left: 0;
	width: 18px;
	height: 18px;
	margin-top: -9px;
	border: 1px solid #dcd2ba;
}

.c-check label:after{
	content: '';
	display: none;
	position: absolute;
	top: 50%;
	left: 4px;
	width: 12px;
	height: 12px;
	margin-top: -5px;
	background: #9e2416;
}

.c-check input[type='checkbox']{
	display: none;
}

.c-check input:checked + label:after{
	display: block;
}

@media screen and (max-width: 768px){
	.c-check label:before{
		width: 15px;
		height: 15px;
		margin-top: -8px;
	}
	.c-check label:after{
		left: 3px;
		width: 11px;
		height: 11px;
	}
}

/*--------------------------------------------------------------------------
	c-borderTtl 下線付きタイトル
---------------------------------------------------------------------------*/
.c-borderTtl{
	position: relative;
	padding-bottom: 25px;
	margin-bottom: 25px;
	text-align: center;
}

.c-borderTtl:before{
	content: '';
	display: block;
	position: absolute;
	bottom: 0;
	left: 50%;
	margin-left: -32px;
	width: 64px;
	height: 4px;
}

@media screen and (max-width: 768px){
	.c-borderTtl{
		padding-bottom: 18px;
		margin-bottom: 18px;
	}
	.c-borderTtl:before{
		margin-left: -16px;
		width: 32px;
		height: 2px;
	}
}

/*--------------------------------------------------------------------------
	 c-loading ローディング
---------------------------------------------------------------------------*/
.c-loading{
	position: fixed;
	top: 0;
	left: 0;
	z-index: 9999;
	width: 100%;
	height: 100%;
}

.c-loading-bgWhite,
.c-loading-bgBlack{
	display: block;
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	-webkit-transform-origin: center bottom;
	        transform-origin: center bottom;
	-webkit-backface-visibility: hidden;
	        backface-visibility: hidden;
}

.c-loading-bgWhite{
	background: #fff;
	-webkit-transform: translateZ(0);
	        transform: translateZ(0);
}

.c-loading-bgBlack{
	-webkit-transform: scaleY(0) translateZ(0);
	        transform: scaleY(0) translateZ(0);
	background: #000;
}

.c-loading-txt{
	display: block;
	position: fixed;
	top: 50%;
	left: 50%;
	z-index: 1;
	overflow: hidden;
	width: 158px;
	height: 61px;
	margin: -31px 0 0 -79px;
	background: url(/shared/images/logo.svg) no-repeat 0 0;
	mix-blend-mode: exclusion;
	-webkit-transform: translateZ(1px);
	        transform: translateZ(1px);
}

/* fallback
-----------------------------------------------------------------*/
.lt-ie9 .c-loading{
	display: none;
}

.no-backgroundblendmode .c-loading-txt{
	background-image: url(/shared/images/logo_black.svg);
}

.no-backgroundblendmode .is-nomixblendLoadLatter .c-loading-txt{
	background-image: url(/shared/images/logo.svg);
}

@media screen and (max-width: 768px){
	.c-loading-txt{
		width: 78px;
		height: 30px;
		margin: -15px 0 0 -39px;
		background-size: 78px 30px;
	}
}

/*--------------------------------------------------------------------------
	 c-shadow 影つきフレーム
---------------------------------------------------------------------------*/
.c-shadow{
	position: relative;
	z-index: 0;
}

.c-shadow:after{
	content: '';
	display: block;
	position: absolute;
	top: 10px;
	left: 10px;
	z-index: -1;
	background: url(/newopen/shared/images/bg_repeat_shadow.png);
	width: 100%;
	height: 100%;
}

@media screen and (max-width: 768px){
	.c-shadow:after{
		top: 5px;
		left: 5px;
		background-image: url(/newopen/shared/images/bg_repeat_shadow_sp.png);
		background-size: 3px 3px;
	}
}

/*--------------------------------------------------------------------------
	c-diagonalTtl 斜線付きタイトル
---------------------------------------------------------------------------*/
.c-diagonalTtl{
	position: relative;
	padding-left: 25px;
	font-size: 24px;
	font-weight: bold;
	line-height: 1;
}

.c-diagonalTtl:before{
	content: '';
	display: inline-block;
	position: absolute;
	top: 50%;
	left: 3px;
	width: 4px;
	height: 26px;
	margin-top: -13px;
	background: #9e2416;
	-webkit-transform: skewX(-15deg);
	        transform: skewX(-15deg);
}

@media screen and (max-width: 768px){
	.c-diagonalTtl{
		padding-left: 15px;
		font-size: 16px;
	}
	.c-diagonalTtl:before{
		width: 3px;
		height: 17px;
		margin-top: -9px;
	}
}

/*--------------------------------------------------------------------------
	c-underlinedTtl 下線付きタイトル
---------------------------------------------------------------------------*/
.c-underlinedTtl{
	padding-bottom: 15px;
	background: url(/shared/images/bg_repeat_ttl.png) repeat-x 0 100%;
	font-size: 26px;
	font-weight: bold;
	line-height: 2.2;
}

.ie .c-underlinedTtl{
	padding-bottom: 0;
}

@media screen and (max-width: 768px){
	.c-underlinedTtl{
		padding-bottom: 10px;
		background-size: auto 1px;
		font-size: 17px;
	}
}

/*--------------------------------------------------------------------------

	module モジュール

---------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------
	 m-container 下層コンテナ
---------------------------------------------------------------------------*/
.m-container{
	padding: 70px 0 90px;
}

@media screen and (max-width: 768px){
	.m-container{
		padding: 30px 10px 45px;
	}
}

/*--------------------------------------------------------------------------
	 m-frame 装飾つきフレーム
---------------------------------------------------------------------------*/
.m-frame-inner{
	padding: 10px;
	background: #fff;
}

.m-frame-cornerTop,
.m-frame-cornerBottom{
	position: relative;
}

.m-frame-cornerTop:before,
.m-frame-cornerTop:after,
.m-frame-cornerBottom:before,
.m-frame-cornerBottom:after{
	content: '';
	display: block;
	position: absolute;
	z-index: 1;
	width: 20px;
	height: 20px;
	background-repeat: no-repeat;
	background-image: url(/shared/images/bg_corner01.png);
}

.m-frame-cornerTop:before{
	top: 0;
	left: 0;
	background-position: 0 0;
}

.m-frame-cornerTop:after{
	top: 0;
	right: 0;
	background-position: 0 -20px;
}

.m-frame-cornerBottom:before{
	bottom: 0;
	left: 0;
	background-position: 0 -60px;
}

.m-frame-cornerBottom:after{
	bottom: 0;
	right: 0;
	background-position: 0 -40px;
}

.m-frame-body{
	border: 3px solid #e3d7c3;
}

@media screen and (max-width: 768px){
	.m-frame-inner{
		padding: 5px;
	}
	.m-frame-cornerTop,
	.m-frame-cornerBottom{
		position: relative;
	}
	.m-frame-cornerTop:before,
	.m-frame-cornerTop:after,
	.m-frame-cornerBottom:before,
	.m-frame-cornerBottom:after{
		width: 10px;
		height: 10px;
		background-image: url(/shared/images/bg_corner01_sp.png);
		background-size: 10px 40px;
	}
	.m-frame-cornerTop:before{
		background-position: 0 0;
	}
	.m-frame-cornerTop:after{
		background-position: 0 -10px;
	}
	.m-frame-cornerBottom:before{
		background-position: 0 -30px;
	}
	.m-frame-cornerBottom:after{
		background-position: 0 -20px;
	}
}

/*--------------------------------------------------------------------------
	 m-flow フォームのフロー
---------------------------------------------------------------------------*/
.m-flow{
	margin-bottom: 50px;
	text-align: center;
}

.m-flow-txt{
	margin-top: 30px;
}

@media screen and (max-width: 768px){
	.m-flow{
		margin-bottom: 25px;
	}
	.m-flow-txt{
		margin-top: 25px;
		font-size: 12px;
	}
}

/*--------------------------------------------------------------------------
	 m-privacy フォームの個人情報保護エリア
---------------------------------------------------------------------------*/
.m-privacy{
	margin-top: 50px;
}

@media screen and (max-width: 768px){
	.m-privacy{
		margin-top: 25px;
	}
}

/* m-privacyTxt
-----------------------------------------------------------------*/
.m-privacyTxt{
	margin-bottom: 20px;
	font-size: 16px;
	text-align: center;
}

@media screen and (max-width: 768px){
	.m-privacyTxt{
		margin-bottom: 10px;
		font-size: 12px;
	}
}

/* m-privacyCont コンテンツ部分
-----------------------------------------------------------------*/
.m-privacyCont{
	position: relative;
	border: 1px solid #dcd2ba;
}

.m-privacyCont:before{
	content: '';
	position: absolute;
	left: 0;
	bottom: 0;
	z-index: 1;
	width: 100%;
	height: 30px;
	background: -webkit-gradient(linear, left bottom, left top, from(white), to(rgba(255, 255, 255, 0)));
	background: -webkit-linear-gradient(bottom, white, rgba(255, 255, 255, 0));
	background:         linear-gradient(to top, white, rgba(255, 255, 255, 0));
}

.m-privacyCont-inner{
	height: 280px;
	padding: 30px 30px 0;
	overflow: auto;
}

.m-privacyCont-inner > *:last-child{
	margin-bottom: 30px;
}

.m-privacyLead{
	margin-bottom: 20px;
}

.m-privacyList + .m-privacyList{
	margin-top: 15px;
}

.m-privacyList-ttl{
	margin-bottom: 5px;
	font-weight: bold;
	font-size: 16px;
}

@media screen and (max-width: 768px){
	.m-privacyCont-inner{
		padding: 20px;
	}
	.m-privacyCont-inner > *:last-child{
		margin-bottom: 20px;
	}
	.m-privacyLead{
		margin-bottom: 10px;
		font-size: 11px;
	}
	.m-privacyList + .m-privacyList{
		margin-top: 10px;
	}
	.m-privacyList-ttl{
		font-size: 13px;
	}
}

/*--------------------------------------------------------------------------
	 m-form フォームのラッパー要素
---------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------
	 m-formList フォーム項目
---------------------------------------------------------------------------*/
.m-formList{
	display: table;
	width: 100%;
	padding: 30px 0;
	border-bottom: 1px solid #e9e3d3;
}

.m-formList:first-child{
	border-top: 1px solid #e9e3d3;
}

.m-formList-ttl,
.m-formList-cont{
	display: table-cell;
}

.m-formList-ttl{
	width: 260px;
	padding-top: 8px;
	padding-bottom: 6px;
	padding-right: 60px;
	font-size: 16px;
	font-weight: bold;
	vertical-align: top;
}

.m-formList-cont{
	vertical-align: middle;
}

.m-formList-cont{
	padding-left: 30px;
}

.m-formList-error{
	margin-bottom: 5px;
	color: #9e2415;
}

/* modifier
-----------------------------------------------------------------*/
.m-formList-required .m-formList-ttl{
	background: url(/newopen/shared/images/ico_required.png) no-repeat 100% 8px;
}

.m-form-confirm .m-formList{
	padding: 15px 0;
}

@media screen and (max-width: 768px){
	.m-formList{
		display: block;
		width: auto;
		padding: 25px 0;
		font-size: 11px;
	}
	.m-formList-ttl{
		display: inline-block;
		width: auto;
		margin-bottom: 15px;
		padding-top: 0;
		padding-bottom: 0;
		padding-right: 45px;
		font-size: 12px;
		vertical-align: middle;
	}
	.m-formList-cont{
		display: block;
	}
	.m-formList-required .m-formList-ttl{
		background: url(/newopen/shared/images/ico_required_sp.png) no-repeat 100% 50%;
		background-size: 35px 15px;
	}
	.m-formList-cont{
		padding-left: 0;
	}
	/* modifier
	-----------------------------------------------------------------*/
	.m-form-confirm .m-formList-ttl{
		display: block;
		padding: 0;
	}
}

/*--------------------------------------------------------------------------
	 m-formSubmit コンバージョンボタンエリア
---------------------------------------------------------------------------*/
.m-formSubmit{
	margin-top: 35px;
}

.m-formSubmit-agree{
	text-align: center;
}

.m-formSubmit-agree .c-check{
	font-size: 16px;
}

.m-formSubmit-btns{
	margin-top: 30px;
	text-align: center;
	letter-spacing: -.4em;
}

.m-formSubmit-btn{
	display: inline-block;
	letter-spacing: 0;
}

.m-formSubmit-btn + .m-formSubmit-btn{
	margin-left: 23px;
}

.m-formSubmit-btn .c-btn{
	width: 360px;
	height: 95px;
	font-size: 16px;
	line-height: 93px;
	letter-spacing: .2em;
}

@media screen and (max-width: 768px){
	.m-formSubmit{
		margin-top: 30px;
	}
	.m-formSubmit-agree .c-check{
		font-size: 12px;
	}
	.m-formSubmit-btns{
		margin-top: 30px;
	}
	.m-formSubmit-btn{
		display: block;
	}
	.m-formSubmit-btn + .m-formSubmit-btn{
		margin-top: 10px;
		margin-left: 0;
	}
	.m-formSubmit-btn .c-btn{
		width: 190px;
		height: 48px;
		font-size: 10px;
		line-height: 46px;
	}
}

/*--------------------------------------------------------------------------
	 m-pageHeader 下層タイトルエリア
---------------------------------------------------------------------------*/
.m-pageHeader{
	display: table;
	width: 100%;
	height: 360px;
	padding-top: 100px;
	background-repeat: no-repeat;
	background-position: 50% 50%;
	background-size: cover;
}

.m-pageHeader-inner{
	display: table-cell;
	vertical-align: middle;
	text-align: center;
}

.m-pageHeader-ttl{
	display: inline-block;
}

.m-pageHeader-txt{
	display: block;
	margin-top: 15px;
	color: #fff;
	text-align: center;
}

@media screen and (max-width: 768px){
	.m-pageHeader{
		height: 190px;
		padding-top: 60px;
		background-size: cover;
	}
	.m-pageHeader-txt{
		font-size: 12px;
	}
}

/*--------------------------------------------------------------------------
	 m-pager ページャー
---------------------------------------------------------------------------*/
.m-pager{
	text-align: center;
	letter-spacing: -.4em;
}

.m-pager-lists{
	display: inline-block;
	margin: 0 6px;
	letter-spacing: 0;
	vertical-align: top;
}

.m-pager-lists li{
	display: block;
	float: left;
	width: 47px;
	height: 45px;
	border: 1px solid #dcdde3;
	background: #fff;
	font-size: 14px;
	text-align: center;
	line-height: 45px;
	-webkit-transition: all .2s ease-out;
	        transition: all .2s ease-out;
}

.m-pager-lists li + li{
	margin-left: 6px;
}

.m-pager-lists a{
	display: block;
	color: #000;
	text-decoration: none;
	-webkit-transition: color .2s ease-out;
	        transition: color .2s ease-out;
}

.m-pager-prev,
.m-pager-next{
	display: inline-block;
	position: relative;
	width: 47px;
	height: 45px;
	border: 1px solid #dcdde3;
	background-color: #fff;
	background-position: 50% 50%;
	background-repeat: no-repeat;
	color: #000;
	text-align: center;
	text-decoration: none;
	line-height: 45px;
	-webkit-transition: all .2s ease-out;
	        transition: all .2s ease-out;
}

.m-pager-prev{
	background-image: url(/shared/images/ico_arrow_pager_prev.png);
}

.m-pager-next{
	background-image: url(/shared/images/ico_arrow_pager_next.png);
}

.m-pager-prev a,
.m-pager-next a{
	display: block;
	width: 100%;
	height: 100%;
}

.m-pager-prev span,
.m-pager-next span{
	display: block;
	overflow: hidden;
	text-indent: 100%;
	white-space: nowrap;
}

/* modifier
-----------------------------------------------------------------*/
.m-pager-prev.is-disabled{
	background-image: url(/shared/images/ico_arrow_pager_prev_disabled.png);
}

.m-pager-next.is-disabled{
	background-image: url(/shared/images/ico_arrow_pager_next_disabled.png);
}

.m-pager-lists .is-current{
	border-color: #000;
	background-color: #000;
}

.m-pager-lists .is-current a{
	color: #fff;
}

/* hover
-----------------------------------------------------------------*/
.modePC .m-pager-lists li:hover{
	border-color: #000;
	background: #000;
}

.modePC .m-pager-lists li:hover a{
	color: #fff;
}

.modePC .m-pager-prev:hover,
.modePC .m-pager-next:hover{
	border-color: #000;
	background-color: #000;
}

.modePC .m-pager-prev:hover{
	background-image: url(/shared/images/ico_arrow_pager_prev_on.png);
}

.modePC .m-pager-next:hover{
	background-image: url(/shared/images/ico_arrow_pager_next_on.png);
}

.modePC .m-pager-prev.is-disabled:hover,
.modePC .m-pager-next.is-disabled:hover{
	border: 1px solid #dcdde3;
	background-color: #fff;
}

.modePC .m-pager-prev.is-disabled:hover{
	background-image: url(/shared/images/ico_arrow_pager_prev_disabled.png);
}

.modePC .m-pager-next.is-disabled:hover{
	background-image: url(/shared/images/ico_arrow_pager_next_disabled.png);
}

@media screen and (max-width: 768px){
	.m-pager-lists{
		margin: 0 6px;
	}
	.m-pager-lists li{
		width: 37px;
		height: 35px;
		font-size: 12px;
		line-height: 35px;
	}
	.m-pager-lists li + li{
		margin-left: 6px;
	}
	.m-pager-prev,
	.m-pager-next{
		width: 37px;
		height: 35px;
		line-height: 35px;
		background-size: 6px 9px;
	}
	.m-pager-prev{
		background-image: url(/shared/images/ico_arrow_pager_prev_sp.png);
	}
	.m-pager-next{
		background-image: url(/shared/images/ico_arrow_pager_next_sp.png);
	}
	/* modifier
	-----------------------------------------------------------------*/
	.m-pager-prev.is-disabled{
		background-image: url(/shared/images/ico_arrow_pager_prev_disabled_sp.png);
	}
	.m-pager-next.is-disabled{
		background-image: url(/shared/images/ico_arrow_pager_next_disabled_sp.png);
	}
}

/*--------------------------------------------------------------------------
	m-footLink ページ下部のコンバージョンセクション
---------------------------------------------------------------------------*/
.m-footLink{
	position: relative;
	padding: 105px 0 85px;
	background-color: #0f0f0f;
	background-repeat: no-repeat;
	background-position: 50% 50%;
	background-size: cover;
	text-align: center;
}

.m-footLink-bg{
	position: absolute;
	top: 0;
	left: 0;
	z-index: 0;
	opacity: 0;
	width: 100%;
	height: 100%;
	background: rgba(0, 0, 0, .3);
	-webkit-transition: all 1s ease;
	        transition: all 1s ease;
}

.m-footLink-cont{
	position: relative;
	z-index: 1;
}

.m-footLink-ttl:before{
	background-color: #9e2416;
}

.m-footLink-txt{
	margin-bottom: 50px;
	color: #fff;
	line-height: 2.8;
	letter-spacing: .12em;
}

.m-footLink-btns{
	letter-spacing: -.4em;
}

.m-footLink-btn{
	display: inline-block;
	letter-spacing: 0;
}

.m-footLink-btn + .m-footLink-btn{
	margin-left: 20px;
}

.m-footLink-btn .c-btn{
	display: inline-block;
	width: 380px;
	height: 95px;
	line-height: 93px;
	font-size: 15px;
}

/* modifier
-----------------------------------------------------------------*/
.m-footLink.is-hover .m-footLink-bg{
	opacity: 1;
}

/* wedding */
.m-footLink-wedding {
	background-image: url(/shared/images/bg_wedding.jpg);
}

@media screen and (max-width: 768px){
	.m-footLink{
		padding: 40px 10px;
	}
	.m-footLink-bg{
		display: none;
	}
	.m-footLink-txt{
		margin-bottom: 30px;
		font-size: 12px;
		line-height: 2.5;
	}
	.m-footLink-btns{
		letter-spacing: normal;
	}
	.m-footLink-btn{
		display: block;
		letter-spacing: normal;
	}
	.m-footLink-btn + .m-footLink-btn{
		margin-top: 15px;
		margin-left: 0;
	}
	.m-footLink-btn .c-btn{
		width: 190px;
		height: 48px;
		line-height: 46px;
		font-size: 10px;
		letter-spacing: .2em;
	}

	.m-footLink-wedding {
		background-image: url(/shared/images/bg_wedding_sp.jpg);
	}
}

/*--------------------------------------------------------------------------

	 utility

---------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------
	 u-disp- レスポンシブによる要素の表示・非表示
---------------------------------------------------------------------------*/
.u-disp-sp{
	display: none;
}

.u-disp-spImg{
	display: none;
}

@media screen and (max-width: 768px){
	.u-disp-pc{
		display: none;
	}
	.u-disp-sp{
		display: block;
	}
	.u-disp-pcImg{
		display: none;
	}
	.u-disp-spImg{
		display: inline;
	}
}

/*--------------------------------------------------------------------------
	 u-cf クリアフィックス
---------------------------------------------------------------------------*/
.u-cf:before,
.u-cf:after{
	content: '';
	display: table;
}

.u-cf:after{
	clear: both;
}

.u-cf{
	zoom: 1;
}

/*--------------------------------------------------------------------------

	 layout

---------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------
	 l-container コンテナ
---------------------------------------------------------------------------*/
.l-container{
	width: 1100px;
	margin: auto;
}

@media screen and (max-width: 768px){
	.l-container{
		width: auto;
	}
}
