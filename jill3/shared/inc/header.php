<?php if(isset($_GET['level']) && $_GET['level'] == 'top'){ ?>
<header id="Header" class="gHeader">
<?php }else{ ?>
<div id="Header" class="gHeader">
<?php } ?>

<div class="gHeader-inner u-cf">

<?php if(isset($_GET['level']) && $_GET['level'] == 'top'){ ?>
<h1 class="gHeaderLogo"><a href="/"><img src="/shared/images/logo.svg" alt="Jillion co.ltd" width="158" height="61"></a></h1>
<?php }else{ ?>
<p class="gHeaderLogo"><a href="/"><img src="/shared/images/logo.svg" alt="Jillion co.ltd" width="158" height="61"></a></p>
<?php } ?>

<p class="gMenuBtn u-disp-sp"><a href="#">
	<span class="gMenuBtn-lines">
		<span></span>
		<span></span>
		<span></span>
	</span>
<!-- .gHeaderBtnSP // --></a></p>

<div class="gNav u-cf">

<div class="gNav-list gNav-company"><a href="/company/">
<div class="gNav-listInner">
<span class="gNav-listTxt">会社概要</span>
<span class="gNav-listTxtEn">COMPANY</span>
<!-- .gNav-listInner // --></div>
<!-- .gNav-list // --></a></div>

<div class="gNav-list gNav-shop"><a href="/shoplist/">
<div class="gNav-listInner">
<span class="gNav-listTxt">店舗</span>
<span class="gNav-listTxtEn">SHOP</span>
<!-- .gNav-listInner // --></div>
</a>

<div class="gShopNav">
<div class="gShopNav-inner">

<div class="gShopNav-lists u-cf">

<div class="gShopNav-list gShopNav-shop gShopNav-jill"><a href="/shop/">
<div class="gShopNav-listInner">
<span>大衆ビストロジル</span>
<!-- .gShopNav-listInner // --></div>
<!-- .gShopNav-list // --></a></div>

<div class="gShopNav-list gShopNav-shop gShopNav-nijill"><a href="/shop/nijill.html">
<div class="gShopNav-listInner">
<span>大衆ビストロジル 煮ジル</span>
<!-- .gShopNav-listInner // --></div>
<!-- .gShopNav-list // --></a></div>

<!-- <div class="gShopNav-list gShopNav-shop gShopNav-jb"><a href="/shop/jb.html">
<div class="gShopNav-listInner">
<span>JB</span>
</div>
</a></div> -->

<div class="gShopNav-list gShopNav-shop gShopNav-shinatora"><a href="/shop/shinatora.html">
<div class="gShopNav-listInner">
<span>酒場 シナトラ</span>
<!-- .gShopNav-listInner // --></div>
<!-- .gShopNav-list // --></a></div>

<div class="gShopNav-list gShopNav-shop gShopNav-gonzo"><a href="/shop/gonzo.html">
<div class="gShopNav-listInner">
<span>Osteria e Bal GONZO</span>
<!-- .gShopNav-listInner // --></div>
<!-- .gShopNav-list // --></a></div>

<div class="gShopNav-list gShopNav-shop gShopNav-rikuu"><a href="http://www.rikuu.jp/" target="_blank">
<div class="gShopNav-listInner">
<span>東京和食りくう</span>
<!-- .gShopNav-listInner // --></div>
<!-- .gShopNav-list // --></a></div>

<div class="gShopNav-list gShopNav-shopList"><a href="/shoplist/">
<div class="gShopNav-listInner">
<span>ショップリスト</span>
<!-- .gShopNav-listInner // --></div>
<!-- .gShopNav-list // --></a></div>

<!-- .gShopNav-lists // --></div>

<!-- .gShopNav-inner // --></div>
<!-- .gShopNav // --></div>

<!-- .gNavList // --></div>

<!-- <div class="gNav-list gNav-company"><a href="/wedding/">
<div class="gNav-listInner">
<span class="gNav-listTxt">ウエディング</span>
<span class="gNav-listTxtEn">WEDDING</span>
</div>
</a></div> -->

<div class="gNav-list gNav-recruit"><a href="/recruit/">
<div class="gNav-listInner">
<span class="gNav-listTxt">採用情報</span>
<span class="gNav-listTxtEn">RECRUIT</span>
<!-- .gNav__listInner // --></div>
<!-- .gNav__list // --></a></div>

<div class="gNav-list gNav-contact"><a href="/contact/">
<div class="gNav-listInner">
<span class="gNav-listTxt">お問い合わせ</span>
<span class="gNav-listTxtEn">CONTACT</span>
<!-- .gNav-listInner // --></div>
<!-- .gNav-list // --></a></div>

<!-- .gNav // --></div>

<!-- .gHeader-inner // --></div>

<?php if(isset($_GET['level']) && $_GET['level'] == 'top'){ ?>
<!-- .gHeader // --></header>
<?php }else{ ?>
<!-- .gHeader // --></div>
<?php } ?>
