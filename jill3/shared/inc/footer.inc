<footer id="Footer" class="gFooter">
	<p id="ScrollTop" class="c-scrollTop"><a href="#Page" class="js-scroll"><img src="/shared/images/btn_scrolltop.png" alt="TOPへ" width="36" height="50"></a></p>
	<div class="gFooter-inner">

		<p class="gFooterLogo"><a href="/"><img src="/shared/images/logo_footer.svg" alt="Jillion co.ltd" width="220" height="86"></a></p>

		<div class="gFooterNav">

			<ul class="gFooterNav-lists">
				<li><a href="/company/">COMPANY</a></li>
				<!-- <li><a href="/wedding/">WEDDING</a></li> -->
				<li><a href="/shoplist/">SHOP</a></li>
				<li><a href="/recruit/">RECRUIT</a></li>
				<li><a href="/contact/">CONTACT</a></li>
			<!-- .gFooterNav--></ul>

		<!-- .gFooterNav // --></div>

		<p class="gFooterCopyright">
			<img src="/shared/images/txt_copyright.png" alt="&copy; JILLION 2016. All rights reserved." width="239" height="10" class="u-disp-pcImg">
			<img src="/shared/images/txt_copyright_sp.png" alt="" width="179" height="8" class="u-disp-spImg">
		</p>

	<!-- .gFooter-inner // --></div>
<!-- #Footer // --></footer>
