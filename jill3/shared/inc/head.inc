<meta property="og:type" content="website">
<meta property="og:url" content="http://www.bistro-jill.com/">
<meta property="og:site_name" content="株式会社ジリオン オフィシャルサイト">
<meta property="og:description" content="大衆ビストロジル/大衆ビストロ煮ジルを展開している株式会社ジリオンのオフィシャルサイトです。">
<meta property="og:image" content="http://www.bistro-jill.com/shared/images/share.jpg">

<link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/favicon.ico">

<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1">
<meta name="format-detection" content="telephone=no,address=no,email=no">
<meta name="theme-color" content="#000000">

<link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/favicon.ico">
<link rel="stylesheet" href="/shared/css/font.css">
<link rel="stylesheet" href="/shared/css/global.css">
<link rel="stylesheet" href="/shared/css/common.css">

<script src="/shared/js/lib/modernizr.js"></script>
