/*--------------------------------------------------------------------------
	本サイト内のソースコードの編集・無断転載・転用を禁止します。
	Copyright(c) Vogaro.inc ALL Rights Reserved.
	http://www.vogaro.co.jp/
--------------------------------------------------------------------------*/
/**
 * @branch.js v0.1.3 - 2013.11.11
 * ├isOS
 * ├isDevice
 * └isBrowser
 */
;(function($){
	$.extend({
    isOS     : function(t, v){ return isOS(t, v)},
    isDevice : function(t){ return isDevice(t)},
    isBrowser: function(t, v, i){ return isBrowser(t, v, i)}
	});

	var ua = navigator.userAgent.toLowerCase();

	/**
	 * @isOS ver 0.4 -2013.07.10
	 */
	var isOS = function(t, v){
		var os = t.toLowerCase();
		// Windows
		if(ua.indexOf('windows') != -1){
			if(ua.indexOf('phone') != -1 || ua.indexOf('mobile') != -1){
				return os == 'windowsphone' || os == 'winphone';
			} else {
				return os == 'windows' || os == 'win';
			}
		// Mac
		} else if(ua.indexOf('mac os') != -1){
			if(ua.indexOf('mobile') != -1){
				if(os == 'ios'){
					if(v)	return ua.indexOf('os ' + String(v).replace('.', '_')) != -1;
					return true;
				}
			} else {
				if(os == 'mac'){
					if(v) return ua.indexOf('mac os x') != -1;
					return true;
				}
			}
		// Android
		} else if(ua.indexOf('android') != -1 && t == 'android'){
			if(v) return ua.indexOf('android ' + v) != -1;
			return true;
		}
		return false;
	};
	/**
	 * @isDevice ver 0.3 -2013.05.27
	 */
	var isDevice = function(t){
		var device = t.toLowerCase();
		if(device == 'pc'){
			return $.isOS('win') || $.isOS('mac');
		} else if (device == 'sd' || device == 'smartdevice'){
			return $.isOS('ios') || $.isOS('android');
		} else if (device == 'sp' || device == 'smartphone'){
			return ua.indexOf('iphone') != -1 || $.isOS('android') && ua.indexOf('mobile') != -1;
		} else if (device == 'tb' || device == 'tablet'){
			return ua.indexOf('ipad') != -1 || $.isOS('android') && ua.indexOf('mobile') == -1;
		} else if (device == 'touchscreen'){
			return 'ontouchstart' in window;
		} else if (device == 'mspointer'){
			return window.navigator.msPointerEnabled > -1;
		} else if(device == 'iphone'){
			return ua.indexOf('iphone') != -1;
		} else if(device == 'ipad'){
			return ua.indexOf('ipad') != -1;
		} else if(device == 'ipod'){
			return ua.indexOf('ipod') != -1;
		} else if(device == 'android'){
			return $.isOS('android') && ua.indexOf('mobile') != -1;
		} else if(device == 'androidtablet'){
			return $.isOS('android') && ua.indexOf('mobile') == -1;
		}else if(device == 'retina'){
			return window.devicePixelRatio == 2;
		}
		return false;
	};
	/**
	 * @isBrowser ver 0.4 -2013.07.10
	 * ※ ieのバージョン更新されたらieVerを更新すること!!
	 */
	var isBrowser = function(t, v, i){
		var browser = t.toLowerCase();

		var init = function(){
			if(i && (ua.indexOf('msie') != -1 || ua.indexOf('trident/7') != -1)){
				return hasIncBrowser();
			} else {
				return hasBrowser(t, v);
			}
		};

		// hasIncBrowser: for ie
		var hasIncBrowser = function(){
			if(i != 'prev' && i != 'later'){
				return undefined; // オプション誤入力チェック
			}

			var ieVer = {min: 6, max: 12},
					flag = i == 'prev',
					end = flag ? v - ieVer.min: ieVer.max - v,
					inc = flag ? -1 : 1,
					counter = v,
					isScope;

			var j = 0, l = end;
			for(; j <= l; j+=1){
				isScope = hasBrowser(t, counter);
				if(isScope)	break;
				counter += inc;
			}
			return isScope;
		};

		// hasBrowser
		var hasBrowser = function(t, v){
			if(ua.indexOf('msie') != -1 && browser == 'ie'){
			// ie
				if(v) return ua.indexOf('msie ' + v + '.') != -1;
				return true;
			} else if(ua.indexOf('trident/7') != -1 && browser == 'ie'){
			// ie11 later
				if(v) return ua.indexOf('rv:' + v + '.') != -1;
				return true;
			} else if(ua.indexOf('firefox') != -1 && browser == 'firefox'){
				if(v) return ua.indexOf('firefox/' + v + '.') != -1;
				return true;
			} else if(ua.indexOf('chrome') != -1){
				if(browser == 'chrome'){
					if(v) return ua.indexOf('chrome/' + v + '.') != -1;
					return true;
				} else if(browser == 'mobilechrome'){
					return $.isOS('ios') || $.isOS('android');
				}
			} else if(ua.indexOf('safari') != -1 && browser == 'safari' || ua.indexOf('safari') != -1 && browser == 'mobilesafari'){
				// PC safari
				if($.isOS('win') && browser == 'safari' || $.isOS('mac') && browser == 'safari'){
					if(v) return ua.indexOf('version/' + v) != -1;
					return true;
				// Mobile Safari
				} else if($.isOS('ios') && browser == 'mobilesafari'){
					if(v)	return $.isOS('ios', v);
					return true;
				}
			// android browser
			} else if(ua.indexOf('safari') !== -1 && $.isOS('android') && browser == 'android'){
				if(v)	return $.isOS('android', v);
				return true;
			} else if(ua.indexOf('opera') != -1){
				return browser == 'opera';
			}
			return false;
		};

		return init();
	};


	/*! jQuery Easing v1.3 - http://gsgd.co.uk/sandbox/jquery/easing/ | Open source under the BSD License. */
	jQuery.easing['jswing'] = jQuery.easing['swing'];
	jQuery.extend(jQuery.easing,{def:'easeOutQuad',swing:function(x,t,b,c,d){return jQuery.easing[jQuery.easing.def](x,t,b,c,d);},easeInQuad:function(x,t,b,c,d){return c*(t/=d)*t+b;},easeOutQuad:function(x,t,b,c,d){return -c *(t/=d)*(t-2)+b;},easeInOutQuad:function(x,t,b,c,d){if((t/=d/2)<1) return c/2*t*t+b;return -c/2 *((--t)*(t-2) - 1)+b;},easeInCubic:function(x,t,b,c,d){return c*(t/=d)*t*t+b;},easeOutCubic:function(x,t,b,c,d){return c*((t=t/d-1)*t*t+1)+b;},easeInOutCubic:function(x,t,b,c,d){if((t/=d/2)<1) return c/2*t*t*t+b;return c/2*((t-=2)*t*t+2)+b;},easeInQuart:function(x,t,b,c,d){return c*(t/=d)*t*t*t+b;},easeOutQuart:function(x,t,b,c,d){return -c *((t=t/d-1)*t*t*t - 1)+b;},easeInOutQuart:function(x,t,b,c,d){if((t/=d/2)<1) return c/2*t*t*t*t+b;return -c/2 *((t-=2)*t*t*t - 2)+b;},easeInQuint:function(x,t,b,c,d){return c*(t/=d)*t*t*t*t+b;},easeOutQuint:function(x,t,b,c,d){return c*((t=t/d-1)*t*t*t*t+1)+b;},easeInOutQuint:function(x,t,b,c,d){if((t/=d/2)<1) return c/2*t*t*t*t*t+b;return c/2*((t-=2)*t*t*t*t+2)+b;},easeInSine:function(x,t,b,c,d){return -c * Math.cos(t/d *(Math.PI/2))+c+b;},easeOutSine:function(x,t,b,c,d){return c * Math.sin(t/d *(Math.PI/2))+b;},easeInOutSine:function(x,t,b,c,d){return -c/2 *(Math.cos(Math.PI*t/d) - 1)+b;},easeInExpo:function(x,t,b,c,d){return(t==0)?b:c * Math.pow(2,10 *(t/d - 1))+b;},easeOutExpo:function(x,t,b,c,d){return(t==d)?b+c:c *(-Math.pow(2,-10 * t/d)+1)+b;},easeInOutExpo:function(x,t,b,c,d){if(t==0) return b;if(t==d) return b+c;if((t/=d/2)<1) return c/2 * Math.pow(2,10 *(t - 1))+b;return c/2 *(-Math.pow(2,-10 *--t)+2)+b;},easeInCirc:function(x,t,b,c,d){return -c *(Math.sqrt(1 -(t/=d)*t) - 1)+b;},easeOutCirc:function(x,t,b,c,d){return c * Math.sqrt(1 -(t=t/d-1)*t)+b;},easeInOutCirc:function(x,t,b,c,d){if((t/=d/2)<1) return -c/2 *(Math.sqrt(1 - t*t) - 1)+b;return c/2 *(Math.sqrt(1 -(t-=2)*t)+1)+b;},easeInElastic:function(x,t,b,c,d){var s=1.70158;var p=0;var a=c;if(t==0) return b;if((t/=d)==1) return b+c;if(!p) p=d*.3;if(a<Math.abs(c)){a=c;var s=p/4;}else var s=p/(2*Math.PI) * Math.asin(c/a);return -(a*Math.pow(2,10*(t-=1)) * Math.sin((t*d-s)*(2*Math.PI)/p ))+b;},easeOutElastic:function(x,t,b,c,d){var s=1.70158;var p=0;var a=c;if(t==0) return b;if((t/=d)==1) return b+c;if(!p) p=d*.3;if(a<Math.abs(c)){a=c;var s=p/4;}else var s=p/(2*Math.PI) * Math.asin(c/a);return a*Math.pow(2,-10*t) * Math.sin((t*d-s)*(2*Math.PI)/p )+c+b;},easeInOutElastic:function(x,t,b,c,d){var s=1.70158;var p=0;var a=c;if(t==0) return b;if((t/=d/2)==2) return b+c;if(!p) p=d*(.3*1.5);if(a<Math.abs(c)){a=c;var s=p/4;}else var s=p/(2*Math.PI) * Math.asin(c/a);if(t<1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin((t*d-s)*(2*Math.PI)/p ))+b;return a*Math.pow(2,-10*(t-=1)) * Math.sin((t*d-s)*(2*Math.PI)/p )*.5+c+b;},easeInBack:function(x,t,b,c,d,s){if(s==undefined) s=1.70158;return c*(t/=d)*t*((s+1)*t - s)+b;},easeOutBack:function(x,t,b,c,d,s){if(s==undefined) s=1.70158;return c*((t=t/d-1)*t*((s+1)*t+s)+1)+b;},easeInOutBack:function(x,t,b,c,d,s){if(s==undefined) s=1.70158;if((t/=d/2)<1) return c/2*(t*t*(((s*=(1.525))+1)*t - s))+b;return c/2*((t-=2)*t*(((s*=(1.525))+1)*t+s)+2)+b;},easeInBounce:function(x,t,b,c,d){return c - jQuery.easing.easeOutBounce(x,d-t,0,c,d)+b;},easeOutBounce:function(x,t,b,c,d){if((t/=d)<(1/2.75)){return c*(7.5625*t*t)+b;}else if(t<(2/2.75)){return c*(7.5625*(t-=(1.5/2.75))*t+.75)+b;}else if(t<(2.5/2.75)){return c*(7.5625*(t-=(2.25/2.75))*t+.9375)+b;}else{return c*(7.5625*(t-=(2.625/2.75))*t+.984375)+b;}},easeInOutBounce:function(x,t,b,c,d){if(t<d/2) return jQuery.easing.easeInBounce(x,t*2,0,c,d) * .5+b;return jQuery.easing.easeOutBounce(x,t*2-d,0,c,d) * .5+c*.5+b;},customInOut:function(x,t,b,c,d){var s=1.70158;if((t/=d/2)<1) return c/2*(t*t*(((s*=(1.525))+1)*t - s))+b;return c/2*((t-=2)*t*(((s*=(1.525))+1)*t+s)+2)+b;}});

	/*! Copyright (c) 2011 Brandon Aaron (http://brandonaaron.net)
	 * Licensed under the MIT License (LICENSE.txt).
	 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
	 * Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
	 * Thanks to: Seamus Leahy for adding deltaX and deltaY
	 * Version: 3.0.6
	 * Requires: 1.2.2+
	 */
	(function($){var types=["DOMMouseScroll","mousewheel"];if($.event.fixHooks){for(var i=types.length;i;){$.event.fixHooks[types[--i]]=$.event.mouseHooks}}$.event.special.mousewheel={setup:function(){if(this.addEventListener){for(var i=types.length;i;){this.addEventListener(types[--i],handler,false)}}else{this.onmousewheel=handler}},teardown:function(){if(this.removeEventListener){for(var i=types.length;i;){this.removeEventListener(types[--i],handler,false)}}else{this.onmousewheel=null}}};$.fn.extend({mousewheel:function(fn){return fn?this.bind("mousewheel",fn):this.trigger("mousewheel")},unmousewheel:function(fn){return this.unbind("mousewheel",fn)}});function handler(event){var orgEvent=event||window.event,args=[].slice.call(arguments,1),delta=0,returnValue=true,deltaX=0,deltaY=0;event=$.event.fix(orgEvent);event.type="mousewheel";if(orgEvent.wheelDelta){delta=orgEvent.wheelDelta/120}if(orgEvent.detail){delta=-orgEvent.detail/3}deltaY=delta;if(orgEvent.axis!==undefined&&orgEvent.axis===orgEvent.HORIZONTAL_AXIS){deltaY=0;deltaX=-1*delta}if(orgEvent.wheelDeltaY!==undefined){deltaY=orgEvent.wheelDeltaY/120}if(orgEvent.wheelDeltaX!==undefined){deltaX=-1*orgEvent.wheelDeltaX/120}args.unshift(event,delta,deltaX,deltaY);return($.event.dispatch||$.event.handle).apply(this,args)}})(jQuery);

	/*!
	 * jQuery Cookie Plugin v1.4.1
	 * https://github.com/carhartl/jquery-cookie
	 *
	 * Copyright 2006, 2014 Klaus Hartl
	 * Released under the MIT license
	 */
	(function (factory) {
		if (typeof define === 'function' && define.amd) {
			// AMD (Register as an anonymous module)
			define(['jquery'], factory);
		} else if (typeof exports === 'object') {
			// Node/CommonJS
			module.exports = factory(require('jquery'));
		} else {
			// Browser globals
			factory(jQuery);
		}
	}(function ($) {

		var pluses = /\+/g;

		function encode(s) {
			return config.raw ? s : encodeURIComponent(s);
		}

		function decode(s) {
			return config.raw ? s : decodeURIComponent(s);
		}

		function stringifyCookieValue(value) {
			return encode(config.json ? JSON.stringify(value) : String(value));
		}

		function parseCookieValue(s) {
			if (s.indexOf('"') === 0) {
				// This is a quoted cookie as according to RFC2068, unescape...
				s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
			}

			try {
				// Replace server-side written pluses with spaces.
				// If we can't decode the cookie, ignore it, it's unusable.
				// If we can't parse the cookie, ignore it, it's unusable.
				s = decodeURIComponent(s.replace(pluses, ' '));
				return config.json ? JSON.parse(s) : s;
			} catch(e) {}
		}

		function read(s, converter) {
			var value = config.raw ? s : parseCookieValue(s);
			return $.isFunction(converter) ? converter(value) : value;
		}

		var config = $.cookie = function (key, value, options) {

			// Write

			if (arguments.length > 1 && !$.isFunction(value)) {
				options = $.extend({}, config.defaults, options);

				if (typeof options.expires === 'number') {
					var days = options.expires, t = options.expires = new Date();
					t.setMilliseconds(t.getMilliseconds() + days * 864e+5);
				}

				return (document.cookie = [
					encode(key), '=', stringifyCookieValue(value),
					options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
					options.path    ? '; path=' + options.path : '',
					options.domain  ? '; domain=' + options.domain : '',
					options.secure  ? '; secure' : ''
				].join(''));
			}

			// Read

			var result = key ? undefined : {},
				// To prevent the for loop in the first place assign an empty array
				// in case there are no cookies at all. Also prevents odd result when
				// calling $.cookie().
				cookies = document.cookie ? document.cookie.split('; ') : [],
				i = 0,
				l = cookies.length;

			for (; i < l; i++) {
				var parts = cookies[i].split('='),
					name = decode(parts.shift()),
					cookie = parts.join('=');

				if (key === name) {
					// If second argument (value) is a function it's a converter...
					result = read(cookie, value);
					break;
				}

				// Prevent storing a cookie that we couldn't decode.
				if (!key && (cookie = read(cookie)) !== undefined) {
					result[name] = cookie;
				}
			}

			return result;
		};

		config.defaults = {};

		$.removeCookie = function (key, options) {
			// Must not alter options, thus extending a fresh object...
			$.cookie(key, '', $.extend({}, options, { expires: -1 }));
			return !$.cookie(key);
		};

	}));



	/*-----------------------------------------------------
	 * $$debug
	-------------------------------------------------------*/
	function $$(str,mode,style){
		var Param = {
			key : 'debug'
		}
		var Func = {
			init : function(){
				if(!$('#'+Param.key).length){
					Func.control();
				}else{
					Param.$debug = $('#'+Param.key);
					Func.echo();
				}
			},
			control : function(){
				var flag,area,_x,_y;
				$('body').append('<div id="'+Param.key+'Bar" style="position: fixed; _position: absolute; bottom: 10px; /top: 10px; /bottom: auto; left: 10px; filter: alpha(opacity=70); opacity:0.7; z-index: 9999;"><div style="cursor: all-scroll; background-color: #333; padding-top: 10px;"><textarea id="'+Param.key+'" style="width: 200px; height: 100px; background-color: #000; color: #fff; font-size: 11px; padding: 5px; border: 0;"></textarea></div></div>');
				Param.$debug = $('#'+Param.key);
				Param.$bar = $('#'+Param.key+'Bar');

				Param.$debug.mousedown(function(){ area = 'fix'; }).mouseup(function(){ area = null; });
				Param.$bar.mousedown(function(e){
					flag = 'move';
					_x = Param.$bar.offset().left-e.pageX;
					_y = Param.$bar.offset().top-e.pageY;
				}).mouseup(function(){ flag = null; });

				$('html').mousemove(function(e){
					if(area) return false;
					if(!flag) return false;
					Param.$bar.css({left:e.clientX+_x,top:e.clientY+_y});
				});

				return Func.echo();
			},
			echo : function(){
				if(typeof(mode) == 'object') style = mode;
				if(style) Param.$debug.css(style)
				if(str == undefined){ Param.$debug.removeAttr('value'); return false;}
				try {if(typeof(str) == 'object') str = JSON.stringify(str,null,'\t');} catch (err) {}
				switch (mode) {
					case 1 :
						Param.$debug.val(Param.$debug.val()+str+'\n');
					break;
					case 2 :
						Param.$debug.val(str + '\n' + Param.$debug.val());
					break;
					default :
						Param.$debug.val(str + '\n');
					break;
				}
			}
		}
		Func.init();
	}

}(jQuery));
