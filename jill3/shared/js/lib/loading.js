;var Jill = Jill || {};

(function(){
	'use strict';

	function Loader(manifest, progressFunc, completeFunc){
		this.lq = new createjs.LoadQueue();
		this.maxConnections = 6;
		this.manifest = manifest;
		this.progressFunc = progressFunc;
		this.completeFunc = completeFunc;
		this.load();
	}
	var p = Loader.prototype;
	p.load = function(){
		var lq = this.lq;
		lq.setMaxConnections(this.maxConnections);
		lq.on('progress', this.progressFunc, this);
		lq.on('complete', this.completeFunc, this);
		lq.loadManifest(this.manifest);
	};

	Jill.Loader = Loader;
}());
