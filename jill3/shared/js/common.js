;// Global Object
var Common = Common || {};

/*--------------------------------------------------------------------------
	 requestAnimationFrame
---------------------------------------------------------------------------*/
// requestAnimationFrame
var requestAnimationFrame = (function(){
	var timeLast = 0;
	return window.requestAnimationFrame		||
		window.webkitRequestAnimationFrame	||
		window.mozRequestAnimationFrame		||
		window.oRequestAnimationFrame		||
		window.msRequestAnimationFrame ||
		function(callback) {
			var timeCurrent = (new Date()).getTime(),
				timeDelta;

			timeDelta = Math.max(0, 16 - (timeCurrent - timeLast));
			timeLast = timeCurrent + timeDelta;

			return setTimeout(function() { callback(timeCurrent + timeDelta); }, timeDelta);
		};
}());
var cancelAnimationFrame = (function(){
	return window.cancelAnimationFrame ||
		window.mozcancelAnimationFrame ||
		window.webkitcancelAnimationFrame ||
		window.mscancelAnimationFrame;
}());

/*--------------------------------------------------------------------------
	Polyfill bindイベントのポリフィル処理
---------------------------------------------------------------------------*/
if (!Function.prototype.bind) {
	Function.prototype.bind = function (oThis) {
		if (typeof this !== "function") {
			// closest thing possible to the ECMAScript 5
			// internal IsCallable function
			throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
		}

		var aArgs = Array.prototype.slice.call(arguments, 1),
			fToBind = this,
			fNOP = function () {},
			fBound = function () {
				return fToBind.apply(this instanceof fNOP && oThis
					? this
					: oThis,
					aArgs.concat(Array.prototype.slice.call(arguments)));
			};

		fNOP.prototype = this.prototype;
		fBound.prototype = new fNOP();

		return fBound;
	};
}

// クラス継承用関数
function inherits(child, parent) {
	if ( Object.setPrototypeOf ) {
		Object.setPrototypeOf(child.prototype, parent.prototype);
	} else {
		var F = function F () {};
		F.prototype = parent.prototype;
		child.prototype = new F();
		child.prototype.constructor = child;
	}
}

(function($){

	var $win = $(window);
	var $html = $('html');
	var $body = $('body');
	var $htmlBody = $('html,body');
	var $doc = $(document);

	Common.setIE = (function(){
		if($.isBrowser('ie')){
			$html.addClass('ie');
		}
	}()); //setIE

	/*--------------------------------------------------------------------------
		 controller
	---------------------------------------------------------------------------*/
	Common.controller = (function () {
		var p = Controller.prototype;

		/**
		 * メディエーター
		 * インスタンス間でのイベントの登録・実行処理を担う
		 */
		function Controller() {
			this._listeners = {};
		}

		/**
		 * イベント登録処理
		 * @param {String} type イベント名
		 * @param {Object} fn 登録関数
		 */
		p.on = function (type, fn) {
			if ( !this._listeners[type] ) {
				this._listeners[type] = [];
			}

			this._listeners[type].push(fn);
		};

		/**
		 * イベント発火処理
		 * @param {type} type イベント名
		 * @param args 引数
		 */
		p.trigger = function (type, args) {
			var i;
			var len = (this._listeners[type]) ? this._listeners[type].length : 0;

			for ( i = 0; i < len; i++ ) {
				this._listeners[type][i](args);
			}
		};

		return new Controller();
	}());

	/*--------------------------------------------------------------------------
		 PageView
	---------------------------------------------------------------------------*/
	Common.pageView = (function () {
		var p = PageView.prototype;
		var s = PageView;

		/**
		 * モード html要素に付与されるクラス
		 */
		s.mode = {
			pc: 'modePC',
			sp: 'modeSD'
		};

		/**
		 * サイト共通で使用されるユーティリティ関数群
		 */
		function PageView() {
			this._init();
		}

		/**
		 * 初期処理
		 */
		p._init = function () {
			this._$window = $(window);
			this._$document = $(document);
			this._$html = $('html');
			this._$head = $('head');
			this._currentMode = '';

			this._props = {};
			this._props.winH = this._$window.innerHeight();

			this._handleEvents();
			this._checkMode();
		};

		/**
		 * イベント登録
		 */
		p._handleEvents = function () {
			var _this = this;
			var timer;
			var interval = 100;

			// リサイズ
			this._$window.on('resize', function () {
				clearTimeout(timer);
				timer = setTimeout(function () {
					_this._props.winH = _this._$window.innerHeight();
					_this._checkMode();
					Common.controller.trigger('resize');
				}, interval);
			});

			// ロード
			this._$window.on('load', function(){
				Common.controller.trigger('load');
			});

			// スクロール
			this._$window.on('scroll', function(){
				Common.controller.trigger('scroll');
			});
		};

		/**
		 * PC/SPのレスポンシブ監視及びhtml要素へのクラスの出しわけ処理
		 */
		p._checkMode = function () {
			var mode = this._$head.css('font-family').replace(/"/gi, '');

			if ( this._currentMode !== mode ) {
				Common.isPC = (mode === 'pc') ? true : false;
				Common.isSP = (mode === 'sp') ? true : false;

				if(this._currentMode){
					this._$html.removeClass(s.mode[this._currentMode]);
				}
				this._$html.addClass(s.mode[mode]);

				Common.controller.trigger('modeChange', mode);

				this._currentMode = mode;
			}
		};

		return PageView;
	}());

	/*--------------------------------------------------------------------------
		 FlatHeight
	---------------------------------------------------------------------------*/
	Common.FlatHeight = (function () {
		var p = FlatHeight.prototype;

		/**
		 * 高さを揃える関数
		 * @param {String} elem 要素名
		 * @param {Number} split 分割数
		 */
		function FlatHeight($elem, split) {
			this._init($elem, split);
		}

		/**
		 * 初期処理
		 */
		p._init = function ($elem, split) {
			this._$elem = $elem;
			this._split = split;

			this._onResize();
			Common.controller.on('modeChange', this._onResize.bind(this));
		};

		/**
		 * 分割数に応じた要素の高さ取得
		 */
		p._splitElem = function () {
			var i, j, pointer, $splitGroup, heightAry, max;
			var len = Math.ceil(this._$elem.length / this._split);


			for ( i = 0, pointer = 0; i < len; i++ ) {
				$splitGroup = [];
				heightAry = [];

				for ( j = 0; j < this._split; j++, pointer++ ) {
					$splitGroup[j] = this._$elem.eq(pointer);
					heightAry[j] = $splitGroup[j].outerHeight();
				}

				max = Math.max.apply(null, heightAry);

				this._flatten($splitGroup, max);
			}
		};

		/**
		 * 高さを揃える処理
		 */
		p._flatten = function ($splitGroup, max) {
			var i, len = $splitGroup.length;

			for ( i = 0; i < len; i++ ) {
				$splitGroup[i].outerHeight(max);
			}

		};

		/**
		 * 初期化
		 */
		p.reset = function () {
			this._$elem.css('height', 'auto');
		};

		/**
		 * リサイズ時に実行
		 */
		p._onResize = function () {
			this.reset();

			if($html.hasClass('modePC')){
				this._splitElem();
			}
		};

		return FlatHeight;
	}());


	/*--------------------------------------------------------------------------
		 fullScreen
	---------------------------------------------------------------------------*/
	Common.fullScreen = (function(){
		var p = FullScreen.prototype;

		/**
		 * フルスクリーン処理
		 * @param {jQuery} $elem
		 */
		function FullScreen($elem){
			this._$elem = $elem;
			this._minHeightPC = 650;
			this._minHeightSP = 320;
			this._winW = 0;

			this._init();
		}

		/**
		 * 初期設定
		 */
		p._init = function(){
			if($.isDevice('tablet')){
				this._onResizeTablet();
				$win.on('orientationchange', this._onResizeTablet.bind(this));
			} else {
				this._onResize();
				Common.controller.on('resize', this._onResize.bind(this));
			}
		};

		/**
		 * 高さ設定
		 */
		p._setHeight = function(){
			var _this = this;

			setTimeout(function(){
				var winH = window.innerHeight ? window.innerHeight : $(window).height();
				var minHeight = ($html.hasClass('modePC')) ? _this._minHeightPC : _this._minHeightSP;
				var height = (minHeight < winH) ? winH : minHeight;

				_this._winW = $win.width();
				_this._$elem.css({ height: height });
			},300);
		};

		/**
		 * リサイズ時に実行
		 */
		p._onResize = function(){
			var winW = parseInt($win.width(), 10);

			if(this._winW !== winW){
				this._setHeight();
			}
		};

		/**
		 * タブレットの場合実行
		 */
		p._onResizeTablet = function(){
			this._setHeight();
		};

		return FullScreen;
	}());

	/*--------------------------------------------------------------------------
		 scroll
	---------------------------------------------------------------------------*/
	Common.scroll = (function(){
		var p = Scroll.prototype;

		/**
		 * ページ内スクロール
		 * .js-scrollクラスをa要素に付与、hrefにidを指定して使用
		 * @param {jQuery} $trigger
		 */
		function Scroll($trigger){
			this._$trigger = $trigger;
			this._$headerH = 0;
			this._$target = $(this._$trigger.attr('href'));
			this._diff = 0;
			this._offset = 0;

			this._init();
		}

		/**
		 * 初期設定
		 */
		p._init = function(){
			this._onEvent();
		};

		/**
		 * スクロール位置の調整
		 * data-diff="100"のようにトリガーのa要素にdata属性を付与して使用
		 */
		p._setDiff = function(){
			this._$headerH = ($html.hasClass('modePC')) ? 70 : 60;
			if(this._$target.attr('id') == 'page'){
				this._diff = 0;
			} else if(this._$trigger.attr('data-diff')){
				this._diff = this._$trigger.attr('data-diff') - this._$headerH;
			} else {
				this._diff = - this._$headerH;
			}
		};

		/**
		 * オフセット値の取得
		 */
		p._setPos = function(){
			this._offset = this._$target.offset().top;
		};

		/**
		 * イベント登録
		 */
		p._onEvent = function(){
			var _this = this;
			this._$trigger.on('click', function(){
				_this._move();
				return false;
			});
		};

		/**
		 * 移動処理
		 */
		p._move = function(){
			var duration = 1000,
				easing = 'easeOutQuint';
			this._setPos();
			this._setDiff();

			$htmlBody.animate({
				scrollTop: this._offset + this._diff
			}, {
				duration: duration,
				easing: easing
			});
		};

		return Scroll;
	}());

	/*--------------------------------------------------------------------------
		 checkSubmit
	---------------------------------------------------------------------------*/
	Common.checkSubmit = (function(){
		var p = CheckSubmit.prototype;

		/**
		 * チェックボックスをチェックすることでボタンが押下できるようにする処理
		 * フォームで使用
		 * @param {jQuery} $elem .js-checkSubmit
		 */
		function CheckSubmit($elem){
			this._$elem = $elem;
			this._$trigger = this._$elem.find('.js-checkSubmit-trigger');
			this._$target = this._$elem.find('.js-checkSubmit-target');

			this._init();
		}

		/**
		 * 初期設定
		 */
		p._init = function(){
			this._onChange();
			this._onEvent();
		};

		/**
		 * イベント登録
		 */
		p._onEvent = function(){
			var _this = this;
			this._$trigger.on('change', function(){
				_this._onChange();
				return false;
			});
		};

		/**
		 * チェンジイベントの際に実行
		 */
		p._onChange = function(){
			if(this._$trigger.is(':checked')){
				this._enable();
			} else {
				this._disable();
			}
		};

		/**
		 * ボタン有効化処理
		 */
		p._enable = function(){
			this._$target.attr('disabled', false);
		};

		/**
		 * ボタン無効化処理
		 */
		p._disable = function(){
			this._$target.attr('disabled', true);
		};

		return CheckSubmit;
	}());

	/*--------------------------------------------------------------------------
		 globalMenuSP
	---------------------------------------------------------------------------*/
	Common.globalMenuSP = (function(){
		var p = GlobalMenuSP.prototype;

		/**
		 * SP時のメニューの開閉処理
		 */
		function GlobalMenuSP(){
			this._$header = $('#Header');
			this._$trigger = this._$header.find('.gMenuBtn');
			this._$target = this._$header.find('.gNav');
			this._activeClass = 'is-open';

			this._init();
		}

		/**
		 * 初期設定
		 */
		p._init = function(){
			this._onResize();
			Common.controller.on('modeChange', this._onResize.bind(this));
		};

		/**
		 * イベント登録
		 */
		p._onEvent = function(){
			var _this = this;
			this._$trigger.on('click.globalMenuSP', function(){
				_this._onClick();
				return false;
			});
		};

		/**
		 * イベント解除
		 */
		p._offEvent = function(){
			this._$trigger.off('click.globalMenuSP');
		};

		/**
		 * クリック時の処理
		 */
		p._onClick = function(){
			if(!this._$header.hasClass(this._activeClass)){
				this._show();
			} else {
				this._hide();
			}
		};

		/**
		 * リサイズ時の処理
		 */
		p._onResize = function(){
			if(!$html.hasClass('modePC')){
				this._offEvent();
				this._onEvent();
			} else {
				this._offEvent();
			}
			this._reset();
		};

		/**
		 * 表示処理
		 */
		p._show = function(){
			this._$header.addClass(this._activeClass);
			$body.addClass(this._activeClass);

			TweenMax.fromTo(this._$target, 0.3, {opacity: 0, y: -10}, {opacity: 1, y: 0, display: 'block'});
		};

		/**
		 * 非表示処理
		 */
		p._hide = function(){
			this._$header.removeClass(this._activeClass);
			$body.removeClass(this._activeClass);

			TweenMax.fromTo(this._$target, 0.3, {opacity: 1}, {opacity: 0, y: -10, display: 'none', onComplete: function(){
				Common.controller.trigger('globalMenuClose');
			}});
		};

		/**
		 * 初期化
		 */
		p._reset = function(){
			this._$header.removeClass(this._activeClass);
			$body.removeClass(this._activeClass);
			this._$target.attr('style', '');
		};

		return GlobalMenuSP;
	}());

	/*--------------------------------------------------------------------------
		 shopNavHover
	---------------------------------------------------------------------------*/
	Common.shopNavHover = (function(){
		var p = ShopNavHover.prototype;

		/**
		 * PC時の「SHOP」ナビにホバー時の処理
		 */
		function ShopNavHover(){
			this._$parent = $('#Header .gNav-shop');
			this._$children = $('#Header .gShopNav');
			this._activeClass = 'is-hover';
			this._flgParent = false;
			this._flgChildren = false;

			this._init();
		}

		/**
		 * 初期設定
		 */
		p._init = function(){
			this._onResize();
			Common.controller.on('modeChange', this._onResize.bind(this));
		};

		/**
		 * リサイズ時の処理
		 */
		p._onResize = function(){
			if($html.hasClass('modePC')){
				this._offEvent();
				this._onEvent();
			} else {
				this._offEvent();
			}
			this._reset();
		};

		/**
		 * イベント登録
		 */
		p._onEvent = function(){
			this._$parent.on('mouseenter.shopNavHover', this._onParentMouseOn.bind(this));
			this._$parent.on('mouseleave.shopNavHover', this._onParentMouseOff.bind(this));
			this._$children.on('mouseenter.shopNavHover', this._onChildrenMouseOn.bind(this));
			this._$children.on('mouseleave.shopNavHover', this._onChildrenMouseOff.bind(this));
		};

		/**
		 * イベント解除
		 */
		p._offEvent = function(){
			this._$parent.off('mouseenter.shopNavHover');
			this._$parent.off('mouseleave.shopNavHover');
			this._$children.off('mouseenter.shopNavHover');
			this._$children.off('mouseleave.shopNavHover');
		};

		/**
		 * 親要素（.gNav-shop）にマウスオンした際の処理
		 */
		p._onParentMouseOn = function(){
			this._flgParent = true;
			this._toggleClass();
		};

		/**
		 * 親要素（.gNav-shop）からマウスオフした際の処理
		 */
		p._onParentMouseOff = function(){
			this._flgParent = false;
			this._toggleClass();
		};

		/**
		 * 子要素（.gShopNav）にマウスオンした際の処理
		 */
		p._onChildrenMouseOn = function(){
			this._flgChildren = true;
			this._toggleClass();
		};

		/**
		 * 子要素（.gShopNav）からマウスオフした際の処理
		 */
		p._onChildrenMouseOff = function(){
			this._flgChildren = false;
			this._toggleClass();
		};

		/**
		 * クラスのトグル処理
		 */
		p._toggleClass = function(){
			if(this._flgParent && this._flgChildren){
				this._$parent.addClass(this._activeClass);
			} else {
				this._$parent.removeClass(this._activeClass);
			}
		};

		/**
		 * 初期化
		 */
		p._reset = function(){
			this._$parent.removeClass(this._activeClass);
		};

		return ShopNavHover;
	}());

	/*--------------------------------------------------------------------------
		 accordion
	---------------------------------------------------------------------------*/
	Common.accordion = (function(){
		var p = Accordion.prototype;

		/**
		 * アコーディオン
		 * @param {jQuery} $elem .js-accordion
		 */
		function Accordion($elem){
			this._$elem = $elem;
			this._$trigger = this._$elem.find('.js-accordion-trigger');
			this._$target = this._$elem.find('.js-accordion-target');
			this._activeClass = 'is-open';
			this._targetH = 0;
			this._isAnimated = false;

			this._init();
		}

		/**
		 * 初期設定
		 */
		p._init = function(){
			this._onEvent();
			Common.controller.on('modeChange', this._onResize.bind(this));

			// デフォルトオープンの設定
			if(this._$elem.hasClass(this._activeClass)){
				this._show();
			} else {
				this._hide();
			}
		};

		/**
		 * 高さ取得
		 */
		p._setHeight = function(){
			this._targetH = this._$target.outerHeight();
		};

		/**
		 * イベント登録
		 */
		p._onEvent = function(){
			var _this = this;
			this._$trigger.on('click.accordion', function(){
				_this._onClick();
				return false;
			});
		};

		/**
		 * イベント解除
		 */
		p._offEvent = function(){
			this._$trigger.off('click.accordion');
		};

		/**
		 * リサイズ時の処理
		 */
		p._onResize = function(){
			this._reset();
		};

		/**
		 * クリック時の処理
		 */
		p._onClick = function(){

			if(!this._isAnimated){
				if(!this._$elem.hasClass(this._activeClass)){
					this._showAnimation();
				} else {
					this._hideAnimation();
				}
			}
		};

		/**
		 * 表示処理 アニメーション無し
		 */
		p._show = function(){
			this._$elem.addClass(this._activeClass);
			this._$target.show();
		};

		/**
		 * 非表示処理 アニメーション無し
		 */
		p._hide = function(){
			this._$elem.removeClass(this._activeClass);
			this._$target.hide();
		};

		/**
		 * 表示処理 アニメーションあり
		 */
		p._showAnimation = function(){
			var _this = this;

			this._isAnimated = true;
			this._$elem.addClass(this._activeClass);

			// 一旦表示させて高さを取得
			this._$target.css({ display: 'block' });
			this._setHeight();
			this._$target.css({ height: 0 });
			this._$target.animate({
				height: this._targetH
			},{
				complete: function(){
					_this._isAnimated = false;
				}
			});
		};

		/**
		 * 非表示処理 アニメーションあり
		 */
		p._hideAnimation = function(){
			var _this = this;

			this._isAnimated = true;
			this._$elem.removeClass(this._activeClass);

			this._$target.animate({
				height: 0
			}, {
				complete: function(){
					_this._$target.css({
						display: 'none',
						height: 'auto'
					});
					_this._isAnimated = false;
				}
			});
		};

		/**
		 * 初期化
		 */
		p._reset = function(){
			this._$elem.removeClass(this._activeClass);
			this._$target.attr('style', '');
		};

		return Accordion;
	}());

	/*--------------------------------------------------------------------------
		 globalNavAccordion
	---------------------------------------------------------------------------*/
	Common.globalNavAccordion = (function(){
		var p = GlobalNavAccordion.prototype;

		/**
		 * グローバルナビ内のアコーディオン（SP）
		 */
		function GlobalNavAccordion(){
			this._$elem = $('.gNav-shop');
			this._$trigger = $('.gNav-shop > a');
			this._$target = this._$elem.find('.gShopNav');
			this._activeClass = 'is-open';
			this._targetH = 0;
			this._isAnimated = false;

			this._init();
		}

		/**
		 * 初期設定
		 */
		p._init = function(){
			this._onResize();
			Common.controller.on('modeChange', this._onResize.bind(this));
			Common.controller.on('globalMenuClose', this._hide.bind(this));
		};

		/**
		 * 高さ取得
		 */
		p._setHeight = function(){
			this._targetH = this._$target.outerHeight();
		};

		/**
		 * イベント登録
		 */
		p._onEvent = function(){
			var _this = this;
			this._$trigger.on('click.accordion', function(){
				_this._onClick();
				return false;
			});
		};

		/**
		 * イベント解除
		 */
		p._offEvent = function(){
			this._$trigger.off('click.accordion');
		};

		/**
		 * リサイズ時の処理
		 */
		p._onResize = function(){
			if($html.hasClass('modePC')){
				this._offEvent();
				this._reset();
			} else {
				this._onEvent();
			}
		};

		/**
		 * クリック時の処理
		 */
		p._onClick = function(){

			if(!this._isAnimated){
				if(!this._$elem.hasClass(this._activeClass)){
					this._showAnimation();
				} else {
					this._hideAnimation();
				}
			}
		};

		/**
		 * 表示処理 アニメーションなし
		 */
		p._show = function(){
			this._$elem.addClass(this._activeClass);
			this._$target.show();
		};

		/**
		 * 非表示処理 アニメーションあり
		 */
		p._hide = function(){
			this._$elem.removeClass(this._activeClass);
			this._$target.hide();
		};

		/**
		 * 表示処理 アニメーションあり
		 */
		p._showAnimation = function(){
			var _this = this;

			this._isAnimated = true;
			this._$elem.addClass(this._activeClass);

			// // 一旦表示させて高さを取得
			this._$target.css({ display: 'block' });
			this._setHeight();
			this._$target.css({ height: 0 });
			this._$target.animate({
				height: this._targetH
			},{
				complete: function(){
					_this._isAnimated = false;
				}
			});
		};

		/**
		 * 非表示処理 アニメーションなし
		 */
		p._hideAnimation = function(){
			var _this = this;

			this._isAnimated = true;
			this._$elem.removeClass(this._activeClass);

			this._$target.animate({
				height: 0
			}, {
				complete: function(){
					_this._$target.css({
						display: 'none',
						height: 'auto'
					});
					_this._isAnimated = false;
				}
			});
		};

		/**
		 * 初期化
		 */
		p._reset = function(){
			this._$elem.removeClass(this._activeClass);
			this._$target.attr('style', '');
		};

		return GlobalNavAccordion;
	}());

	/*--------------------------------------------------------------------------
		 scrollHeader
	---------------------------------------------------------------------------*/
	Common.scrollHeader = (function(){
		var p = ScrollHeader.prototype;

		/**
		 * スクロールした際のグローバルヘッダーの処理（PC）
		 */
		function ScrollHeader(){
			this._$header = $('#Header');
			this._activeClass = 'is-scroll';
			this._scrollVal = 0;

			this._init();
		}

		/**
		 * 初期設定
		 */
		p._init = function(){
			this._onResize();
			Common.controller.on('modeChange', this._onResize.bind(this));
		};

		/**
		 * イベント登録
		 */
		p._onEvent = function(){
			$win.on('scroll.scrollHeader', this._onScroll.bind(this));
		};

		/**
		 * イベント解除
		 */
		p._offEvent = function(){
			$win.off('scroll.scrollHeader');
		};

		/**
		 * リサイズ時の処理
		 */
		p._onResize = function(){
			if($html.hasClass('modePC')){
				this._offEvent();
				this._onEvent();
			} else {
				this._offEvent();
			}
			this._reset();
		};

		/**
		 * スクロール時の処理
		 */
		p._onScroll = function(){
			this._scrollVal = $win.scrollTop();

			if(this._scrollVal > 0){
				this._$header.addClass(this._activeClass);
			} else {
				this._$header.removeClass(this._activeClass);
			}
		};

		/**
		 * 初期化
		 */
		p._reset = function(){
			this._$header.removeClass(this._activeClass);
		};

		return ScrollHeader;
	}());

	/*--------------------------------------------------------------------------
		 loading
	---------------------------------------------------------------------------*/
	Common.loading = (function(){
		var p = Loading.prototype;

		/**
		 * ローディングアニメーション
		 * /shared/js/lib/loading.jsのLoaderクラスが実体
		 * @param {json}   manifest
		 * @param {Function} callback
		 */
		function Loading(manifest, callback){
			this._manifest = manifest;
			this._callback = (callback) ? callback : null;
			this._$loader = $('#Loader');
			this._$white = this._$loader.find('.c-loading-bgWhite');
			this._$black = this._$loader.find('.c-loading-bgBlack');
			this._$txt = this._$loader.find('.c-loading-txt');
			this._headerH = ($html.hasClass('modePC')) ? 100 : 60;

			this._init();
		}

		/**
		 * 初期設定
		 */
		p._init = function(){
			$win.on('mousewheel.noScroll', function(){
				return false;
			});

			new Jill.Loader(this._manifest, this._onProgress.bind(this), this._onComplete.bind(this));
		};

		/**
		 * ローディング中の処理
		 * @param  {Object} e イベントオブジェクト
		 */
		p._onProgress = function(e){
			var rate = e.progress;
			this._progressAnimation(rate);
		};

		/**
		 * ローディング完了時の処理
		 */
		p._onComplete = function(){
			$win.off('mousewheel.noScroll');
			this._completeAnimation();
		};

		/**
		 * ローディング完了後のアニメーション
		 */
		p._completeAnimation = function(){
			var _this = this;

			setTimeout(function(){
				Common.controller.trigger('endLoaded');
			},800);
			TweenMax.to(this._$black, 0.8, { height: this._headerH, delay: 0.6, ease: Expo.easeOut });
			TweenMax.to(this._$txt, 0.8, { opacity: 0, delay: 0.5, ease: Expo.easeOut });
			TweenMax.to(this._$white, 0.8, { height: this._headerH, delay: 0.6, ease: Expo.easeOut, onComplete: function(){
				TweenMax.to(_this._$loader, 0.2, { opacity: 0, ease: Expo.easeOut, onComplete: function(){
					if(_this._callback) _this._callback();
					_this._$loader.css({ display: 'none' });
				}});
			}});
		};

		/**
		 * ローディング中のアニメーション
		 * @param  {Number} rate 0-1の数値（ローディング進捗率）
		 */
		p._progressAnimation = function(rate){
			var percent = Math.floor(rate * 100);
			TweenMax.to(this._$black, 0.6, { scaleY: rate, ease: Expo.easeOut });
			if($html.hasClass('no-backgroundblendmode') && percent > 50){
				this._$loader.addClass('is-nomixblendLoadLatter');
			}
		};

		return Loading;
	}());

	/*--------------------------------------------------------------------------
		 pageHeaderAnimation
	---------------------------------------------------------------------------*/
	Common.pageHeaderAnimation = (function(){
		var p = PageHeaderAnimation.prototype;

		/**
		 * 下層ページのKV領域のタイトルアニメーション
		 * @param {jQuery} $wrap .m-pageHeader
		 */
		function PageHeaderAnimation($wrap){
			this._$wrap = $wrap;
			this._$target = this._$wrap.find('.m-pageHeader-ttl');

			this._init();
		}

		/**
		 * 初期設定
		 */
		p._init = function(){
			// 初期設定
			TweenMax.set(this._$target, { opacity: 0.1 });

			// ローディングが終わり次第アニメーション
			Common.controller.on('endLoaded', this._tween.bind(this));
		};

		/**
		 * アニメーション設定
		 */
		p._tween = function(){
			TweenMax.to(this._$target, .8, { opacity: 1, ease: Expo.ease });
		};

		return PageHeaderAnimation;
	}());

	/*--------------------------------------------------------------------------
		 btnHover
	---------------------------------------------------------------------------*/
	Common.btnHover = (function(){
		var p = BtnHover.prototype;

		/**
		 * ボタンホバー時に演出したい時に使用
		 * @param {jQuery} $elem .js-btnHover スコープ
		 */
		function BtnHover($elem){
			this._$elem = $elem;
			this._$btn = this._$elem.find('.js-btnHover-trigger');
			this._activeClass = 'is-hover';

			this._init();
		}

		/**
		 * 初期設定
		 */
		p._init = function(){
			this._onResize();
			Common.controller.on('modeChange', this._onResize.bind(this));
		};

		/**
		 * リサイズ時の処理
		 */
		p._onResize = function(){
			if($html.hasClass('modePC')){
				this._offEvent();
				this._onEvent();
			} else {
				this._offEvent();
			}
			this._reset();
		};

		/**
		 * イベント登録
		 */
		p._onEvent = function(){
			this._$btn.on('mouseenter.hover', this._onMouseOn.bind(this));
			this._$btn.on('mouseleave.hover', this._onMouseOff.bind(this));
		};

		/**
		 * イベント解除
		 */
		p._offEvent = function(){
			this._$btn.off('mouseenter.hover');
			this._$btn.off('mouseleave.hover');
		};

		/**
		 * ボタンにマウスオン時の処理
		 */
		p._onMouseOn = function(){
			this._$elem.addClass(this._activeClass);
		};

		/**
		 * ボタンからマウスオフ時の処理
		 */
		p._onMouseOff = function(){
			this._$elem.removeClass(this._activeClass);
		};

		/**
		 * 初期化
		 */
		p._reset = function(){
			this._$elem.removeClass(this._activeClass);
		};

		return BtnHover;
	}());

	/*--------------------------------------------------------------------------
		 sendURL
	---------------------------------------------------------------------------*/
	Common.sendURL = (function(){
		var p = SendURL.prototype;

		/**
		 * セレクトボックスでページ遷移させる処理
		 * @param {jQuery} $elem .js-sendURL
		 */
		function SendURL($elem){
			this._$elem = $elem;

			this._init();
		}

		/**
		 * 初期設定
		 */
		p._init = function(){
			this._onEvent();
		};

		/**
		 * イベント登録
		 */
		p._onEvent = function(){
			this._$elem.on('change', this._onChange.bind(this));
		};

		/**
		 * チェンジイベント時の処理
		 */
		p._onChange = function(){
			var val = this._$elem.val();
			window.location = val;
		};

		return SendURL;
	}());

	/*----------------------------------------------------------------------
		parallax
	----------------------------------------------------------------------*/
	Common.parallax = (function(){
		var p = Parallax.prototype;

		/**
		 * パララックスアニメーション（PC）
		 * @param {jQuery} $target .js-parallax-target
		 */
		function Parallax($target){
			this._$target = $target;
			this._baseOffsetTop = 0;
			this._scrollTop = 0;
			this._parallaxRatio = 1.1;
			this._targetH = this._$target.outerHeight();

			Common.controller.on('load', this._init.bind(this));
		}

		/**
		 * 初期設定
		 */
		p._init = function(){
			this._onResize();
			Common.controller.on('modeChange', this._onResize.bind(this));
		};

		/**
		 * スクロール時の処理
		 */
		p._onScroll = function(){
			this._scrollTop = $win.scrollTop();
			this._update();
		};

		/**
		 * リサイズ時の処理
		 */
		p._onResize = function(){
			if($html.hasClass('modePC')){
				this._baseOffsetTop = this._$target.offset().top;
				$win.on('scroll.parallax', this._onScroll.bind(this));
			} else {
				$win.off('scroll.parallax');
			}
			this._reset();
		};

		/**
		 * パララックス対象要素の位置をアップデート
		 */
		p._update = function(){
			var moveVal = Math.round((this._baseOffsetTop - this._scrollTop) * -(1 - this._parallaxRatio));
			moveVal = moveVal / 2;
			TweenMax.to(this._$target, .8, { y: moveVal });
		};

		/**
		 * 初期化
		 */
		p._reset = function(){
			this._$target.attr('style', '');
		};

		return Parallax;
	}());

/*----------------------------------------------------------------------
	DOM READY
----------------------------------------------------------------------*/
	jQuery(function($){
		//タブレットSP表示
		if($.isDevice('tablet')) {
			$("meta[name='viewport']").attr('content','width=768');
		}

		// 表示に関する制御
		// ※デバイスモードの定義のため最初に呼び出す
		new Common.pageView();

		// 下層ヘッダーアニメーション
		if(!$html.hasClass('lt-ie9')){
			new Common.pageHeaderAnimation($('.m-pageHeader'));
		}

		// ページ内スクロール
		$('.js-scroll').each(function(){
			new Common.scroll($(this));
		});

		// チェックボックスが選択されてサブミットできる状態にする
		$('.js-checkSubmit').each(function(){
			new Common.checkSubmit($(this));
		});

		// グロナビのアコーディオン
		new Common.globalNavAccordion();

		// SP時のグロナビ制御
		new Common.globalMenuSP();

		// PC時のグロナビ（SHOP）ホバー字の制御
		new Common.shopNavHover();

		// スクロール時のヘッダー表示制御
		new Common.scrollHeader();

		// セレクトボックス選択時にvalue値を元にページ遷移する
		$('.js-sendURL').each(function(){
			new Common.sendURL($(this));
		});

	});

}(jQuery));
