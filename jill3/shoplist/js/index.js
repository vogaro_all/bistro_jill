;// Global Object
var Page = Page || {};

(function($){

	var $win = $(window);
	var $html = $('html');
	var $body = $('body');
	var $htmlBody = $('html,body');
	var $doc = $(document);

	var manifestPC = [
		{ src: 'images/img_kv.jpg' },
		{ src: 'images/ttl_page.png' },
		{ src: 'images/img_jill01.jpg' },
		{ src: 'images/img_jill02.jpg' },
		{ src: 'images/img_nijill01.jpg' },
		{ src: 'images/img_nijill02.jpg' },
		{ src: 'images/img_nijill03.jpg' },
		{ src: 'images/img_nijill04.jpg' },
		{ src: 'images/img_jb01.jpg' },
		{ src: 'images/img_jb02.jpg' },
		{ src: 'images/img_shinatora01.jpg' },
		{ src: 'images/img_shinatora02.jpg' },
		{ src: 'images/img_gonzo01.jpg' },
		{ src: 'images/ico_arrow_tab.png' }
	];

	var manifestSP = [
		{ src: 'images/img_kv_sp.jpg' },
		{ src: 'images/ttl_page_sp.png' },
		{ src: 'images/img_jill01_sp.jpg' },
		{ src: 'images/img_jill02_sp.jpg' },
		{ src: 'images/img_nijill01_sp.jpg' },
		{ src: 'images/img_nijill02_sp.jpg' },
		{ src: 'images/img_nijill03_sp.jpg' },
		{ src: 'images/img_nijill04_sp.jpg' },
		{ src: 'images/img_jb01_sp.jpg' },
		{ src: 'images/img_jb02_sp.jpg' },
		{ src: 'images/img_shinatora01_sp.jpg' },
		{ src: 'images/img_shinatora02_sp.jpg' },
		{ src: 'images/img_gonzo01_sp.jpg' },
		{ src: 'images/ico_arrow_tab_sp.png' }
	];

/*----------------------------------------------------------------------
	DOM READY
----------------------------------------------------------------------*/
	jQuery(function($){

		// PC
		if($html.hasClass('modePC')){
			if(!$html.hasClass('lt-ie9')){
				new Common.loading(manifestPC);
			}
		} else {
			// SP
			new Common.loading(manifestSP);
		}

	});

}(jQuery));
